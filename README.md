# Spaceball

## Auteurs
Thomas Robert de Massy ROBT24057409

Cyril Ibrahim IBRC25059301

Vincent Lavoie LAVV18089202

Stephane Olssen OLSS14037300

## Description
Spaceball est un jeu dans l'esprit du ballon chasseur où les participants sont des vaisseaux spatiaux et le terrain de jeu est une grande pièce futuriste. Comme au ballon chasseur, il y a deux équipes qui s'affrontent, un bleu et un rouge, qui jouent sur un terrain séparé en deux moitiés qui consiste aussi de deux prisons, un sur chaque côté du terrain qui surplombent le terrain de l'autre équipe.

## Exécution du jeu
Le jeu peut être exécuté de différentes façons :

1. version web [http://playspaceball.com](http://playspaceball.com)

2. directement à partir de Unity en ouvran le dossier principal comme projet.

3. Au besoin un éxecutable Windows, OS X ou Ubuntu peut être rendu disponible

##A prendre en note

La souris est très largement conseillé par soucis de jouabilité. De plus l'affichage des courbes de Bézier se fait avec
la touche 'P'. Et finalement l'idéal pour pouvoir jouer sans trop d'installation est d'utiliser la version web 
à cause de la taille importante des executables. 

## Règles
### Comment gagner
Une partie de Spaceball est divisé en plusieurs manches(rounds) et l’équipe qui remporte le plus de manches gagne
la partie. Chaque manche est chronométrée et l’équipe qui renvoie tous ses adversaires en prison ou qui a le plus de points à
la fin d'une manche la remporte.

### Pointage
Pour faire des points, un joueur doit frapper un adversaire avec une balle ou un laser, en plus de faire un point ceci renvoie l'adversaire en prison.


### Utilisation des balles
Les balles se font attraper simplement en faisant face à la balle lorsque celle-ci est proche, on peux même les attraper en plein vol.
Pour lancer une balle avec plus de puissance il suffit de garder la touche de tir enfoncer plus longtemps, la jauge à droite indique la puissance du tire. Lorsqu'on a atteint la puissance maximum la balle sera lancée automatiquement.
#### Target Lock
Pour plus de contrôle en tirant les balles, il y a une "target lock" qui permet de viser un adversaire plus facilement. Pour s'en servir, il s'agit de
l'actionner lorsqu'on a un adversaire dans le mire rouge en bas à droite de l’écran, ceci nous donne quelques secondes pour tirer et être assuré de frapper l'adversaire.

### Utilisation du laser
Le laser est accessible seulement lorsque le joueur attrape le "power-up" du laser qui est une balle dorée placer dans sa prison au début de la partie. Une fois que le power-up est pris il sera plus disponible avant 45 secondes, mais le joueur pourrais s'en servir pour seulement 20 secondes. Lorsque le joueur peut se servir du laser, il verra le texte "Laser" en haut à droite de l’écran.En plus, le laser ne peut être actionné que pour un maximum de 5 secondes à la fois, elle se recharge automatiquement lorsqu'on ne s'en sert pas. La jauge à droite sert d'indicateur de la charge qui reste pour le laser. À noter que lorsqu'il reste moins de 2 secondes de charge on ne peut actionner le laser.

### Prisons
Un joueur se retrouve en prison s'il se fait frapper par une balle ou franchit le milieu du terrain. Un joueur prisonnier peut sortir de prison en frappant un adversaire avec un laser ou une balle. Une barrière est placée entre la prison et le terrain de jeu qui bloque les joueurs, mais laisse passer les balles et les lasers.

### Barrière Centrale
Il existe une barrière centrale qui divise le terrain en deux, si un joueur dépasse cette frontière alors il se retrouve en prison, mais aucun point n'est accordé à l'autre équipe. Avant de se retrouver en prison la barrière s'illumine rouge pour avertir le joueur qu'il à atteint les limites de sa partie du terrain.

## Contrôles
Malgré le fait que le jeu peut être joué uniquement avec le clavier c'est déconseillé puisque la souris offre un meilleur contrôle du vaisseau.

Touches **W**, **S**, **A** et **D**  : Déplacement

**Souris ou flèches**     				: Rotation (sauf si spécifié autrement dans les options)

**SHIFT**                 				: Activation du Target Lock

**SPACE** ou **Clique gauche**		         	: Lancer une balle et engager le Target Lock (maintenir enfoncé pour plus de puissance)

**ALT** (gauche) ou **Clique droit**            	: Tirer le laser

**F**                     				: Mode plein écran

**C**                     				: Mode First Player

**M**                     				: Activer/Désactiver la musique de fond
	
**Q**                                                   : Changer de niveau de qualité graphique

**R**                     				: Afficher le framerate (debug)

**P**                     				: Afficher les courbes de Bezier des déplacements des vaisseaux (debug)

**Z**                     				: Activer le mode Gravity Zero (cheat)

**I**                     				: Mode invincible (cheat)


## Analyse du fonctionnement du code
Il est possible de visualiser les courbes de Bezier des chemins que suivent les vaisseaux lors de leurs déplacements en appuyant sur la touche **P** pendant une partie. La touche **R** affiche le taux de rafraichissement (framerate) par secondes. Pour éviter de se retrouver trop souvent en prison il est possible d'utiliser le "cheat" invincible en appuyant sur la touche **I** et d'activer/désactiver le mode Zero Gravity avec la touche **Z**.

## Technologies utilisées
Blender

C#

Unity3d

Gimp

Photoshop

3DS Max

Git

## Resources utilisées

**Modèle 3D du vaisseau** : [https://www.assetstore.unity3d.com/en/?gclid=CJ6e08uLuckCFcYRHwodosQL9g#!/content/13866](https://www.assetstore.unity3d.com/en/?gclid=CJ6e08uLuckCFcYRHwodosQL9g#!/content/13866)

**Fond d'écran**: [http://www.sharecg.com/v/68443/browse/1/3D-and-2D-Art/Beyond-the-Farthest-Star](http://www.sharecg.com/v/68443/browse/1/3D-and-2D-Art/Beyond-the-Farthest-Star) par Kathryn Miao

**Bibliothèque BezierPath.cs** : [http://devmag.org.za/2011/04/05/bzier-curves-a-tutorial/](http://devmag.org.za/2011/04/05/bzier-curves-a-tutorial/)

**Clips audio** : [http://www.freesound.org/](http://www.freesound.org/) (Détail de chaque son plus loin)

**Musiques** : [http://incompetech.com/music/royalty-free/most/recent.php](http://incompetech.com/music/royalty-free/most/recent.php]
			   [https://www.assetstore.unity3d.com/en/#!/content/7398]

	"The Lift" Kevin MacLeod
	"Danger Storm" Kevin MacLeod
	"Zero Gravity" Jon Hillman Media, LLC

## Notes sur le clonage de repositoire git
Le repositoire est rendu plus de 300MB ce qui rend le clonage assez long.

## Emplacement des ressources
/bin/win - Les exécutables 32 et 64 bits pour Windows

/bin/osx - Les exécutables 32 et 64 bits pour OSX

/bin/linux - Les exécutables 32 et 64 bits pour Linux(Ubuntu)

/prentation - Les diapositives utilisées lors de notre presentation

/Assets/images - Les images

/Assets/Audio/Audio Files - Les fichiers de son

/Assets/Scripts - Les fichiers de code source en C#

/Assets/Models - Les modèles 3d du jeu

/Devel - Fichiers utilisés lors de la conception des images et modèles 3D



##Références audio détaillé: 

	laser sound: https://www.freesound.org/people/mazk1985/sounds/187405/
	bloc collision: https://www.freesound.org/people/Benboncan/sounds/77074/ modifié
	zero gravity effect: https://www.freesound.org/people/Isaac200000/sounds/185773/ coupé
	deplacement vaisseau 1:https://www.freesound.org/people/Tomlija/sounds/101278/
	deplacement vaisseau 2: https://www.freesound.org/people/lewis100011/sounds/241262/
	deplacement vaisseau 3: https://www.freesound.org/people/DJ%20Chronos/sounds/66536/
	barrière: https://www.freesound.org/people/AlienXXX/sounds/329881/
	generation de voix avec : fromtexttospeech.com   et modification avec Audacity 
	envoi de la balle: https://www.freesound.org/people/lewis100011/sounds/241827/ coupé
	charge la ball: https://www.freesound.org/people/BMacZero/sounds/94119/
	perd la ball: https://www.freesound.org/people/LloydEvans09/sounds/321807/
	win and loose message: https://www.assetstore.unity3d.com/en/#!/content/4369
	countdown timer: http://freesound.org/people/Robinhood76/sounds/9787  
	zero gravity on: http://freesound.org/people/guitarguy1985/sounds/57808/
	zero gravity off: http://freesound.org/people/LG/sounds/25003/
	get bonus: : http://freesound.org/people/GameAudio/sounds/220184/
	loose bonus: http://freesound.org/people/GameAudio/sounds/220174/
	laser empty: http://freesound.org/people/GameAudio/sounds/220169/
	click target lock: http://freesound.org/people/KorgMS2000B/sounds/54405/
	



## Licence GPL v3
