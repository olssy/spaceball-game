﻿using UnityEngine;
using System.Collections;

public class ElectroFrontier : MonoBehaviour {

	public  float scaleSpeed     = 9.0f;
	public  float scaleSize      = 1.0f;
	public  float rotationSpeed1 = 10.0f;
	public  float rotationSpeed2 = 100.0f;

	private float maxIntensity   = 0.2f;
	private bool  isTrespassing  = false;

	private static GameController gameController;


	void Start () {

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		
		if ( gameControllerObject != null ) {
			
			gameController = gameControllerObject.GetComponent <GameController>();
			
		}
		
		if ( gameController == null ) {
			
			Debug.Log ("Cannot find 'GameController' script");
			
		}
	
	}

	void Update () {
        if (gameController.gameStarted) {
		    if ( gameController != null & gameController.game.getConfigs().getAnimFrontier() ) {

			    transform.Rotate( new Vector3( 0.0f, 0.0f, rotationSpeed1 + Random.Range( 0, 100 ) ),  Time.deltaTime * rotationSpeed2 );

			    Vector3 v3Scale = new Vector3( transform.localScale.x, transform.localScale.y, Random.value  * scaleSize );

			    transform.localScale = Vector3.Lerp( transform.localScale, v3Scale, Time.deltaTime * scaleSpeed );

		    }

		    //Material mat = GetComponentInChildren<SkinnedMeshRenderer>().material;

		    //Color newColor = mat.color;

		    //newColor.a += 255;// Mathf.Clamp( Random.Range( -255, 255 ), 0, 255 );

		    //mat.color = newColor;

		    Color color = GetComponent<Renderer>().material.color;
		    //color.a = 10;//Mathf.Clamp( Random.Range( -0.1f, 0.1f ), 0.0f, 1.0f );
		
		    Color newColor = color;

    //		if ( Random.value > 0.8f ) {
    //
    //			newColor.r = Random.Range( 0.0f, 1.0f );
    //			newColor.g = Random.Range( 0.0f, 1.0f );
    //			newColor.b = Random.Range( 0.0f, 1.0f );
    //			
    //			GetComponent<Renderer>().material.color = newColor;// Color.Lerp( color, newColor, Time.deltaTime * 30.0f );
    //
    //		}



	        if ( isTrespassing ) {

			    newColor.a =Random.Range( 0.0f, 0.6f );//Random.Range( 0.0f, 0.02f );
			
			
			    GetComponent<Renderer>().material.color = Color.Lerp( color, newColor, Time.deltaTime * 30.0f );

				// Play the sound
				gameController.game.playSound( AudioController.AUDIO_TOUCH_FRONTIER );


		    } else if ( Random.value > 0.5f ) {

			    newColor.a = Random.Range( 0.0f, 0.05f ); // Sur mon desktop 0.1f est mieux que 0.05f

			    GetComponent<Renderer>().material.color =  Color.Lerp( color, newColor, Time.deltaTime * 30.0f );

		    } else if ( Random.value > 0.9f & !isTrespassing ) {
			
			    newColor.a = 0.0f;//Random.Range( 0.0f, 0.02f );


			    GetComponent<Renderer>().material.color =  newColor;//Color.Lerp( color, newColor, Time.deltaTime * 30.0f );
			
		    } //else if ( Random.value > 0.98f ) {
    //			
    //			newColor.a = 0.02f;//Random.Range( 0.0f, 0.02f );
    //			
    //			
    //			GetComponent<Renderer>().material.color = Color.Lerp( color, newColor, Time.deltaTime * 30.0f );
    //			
    //		}

		    isTrespassing = false;
        }
    }

	void OnTriggerEnter ( Collider other ) {

		isTrespassing = true;
		
	}
	
	void OnTriggerStay ( Collider other ) {

		isTrespassing = true;

	}
	
	void OnTriggerExit ( Collider other ) {

		isTrespassing = false;
		
	}

//	public void setTespassing( bool val ) {
//
//		this.isTrespassing = val;
//		
//	}

}
