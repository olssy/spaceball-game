﻿using UnityEngine;
using System.Collections;

public class OpacityAnimator : MonoBehaviour {

	public  float duration     = 4.0f;
	public  float minIntensity = 0.3f;
	public  float maxIntensity = 0.4f;
	private float starttime;


	void Start () {

		starttime = Time.time;

	}
	

	void FixedUpdate () {

		if ( Random.value >= 0.99 )
			starttime = Time.time;

		Color col = transform.GetComponent<Renderer>().material.color;

		float phi = ( Time.time - starttime ) / duration * 2 * Mathf.PI;
		col.a     = minIntensity + ( Mathf.Sin(phi) * ( maxIntensity - minIntensity ) );
	    
		transform.GetComponent<Renderer>().material.color = col;

	}

}
