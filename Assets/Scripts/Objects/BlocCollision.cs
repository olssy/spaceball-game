﻿using UnityEngine;
using System.Collections;

public class BlocCollision : MonoBehaviour {

	private static GameController gameController;



	void Start () {	
		
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		
		if ( gameControllerObject != null ) {
			
			gameController = gameControllerObject.GetComponent <GameController>();
			
		}
		
		if ( gameController == null ) {
			
			Debug.Log ("Cannot find 'GameController' script");
			
		}
		
	}



	void OnCollisionEnter ( Collision col ) {

		if ( isPlaying() )
			return;

		// Is it a ball?
		if ( col.gameObject.layer == 13 ) {

			Ball collisionball = gameController.game.getBallFromGameObject( col.gameObject );

			// Do not make any sound if the ball is slow
			if ( collisionball == null || collisionball.getSpeed() < 1.0f )
				return;

		}

		// Volume of the sound
		float attenuation = 60.0f;
		Rigidbody colrb   = col.collider.transform.GetComponent<Rigidbody>();
		Rigidbody rb      = transform.GetComponent<Rigidbody>();
		float level       = 0.0f;

		if ( colrb != null && rb != null )
			level = Mathf.Max( colrb.velocity.sqrMagnitude / attenuation, rb.velocity.sqrMagnitude / attenuation );
		else
			return;

		// Play sound if in collision with a wall, frontier, ball or bloc
		if ( ( col.gameObject.layer == 8 || col.gameObject.layer == 13 || col.gameObject.layer == 14 || col.gameObject.layer == 23 ) )
			playSound( AudioController.AUDIO_BLOC_COLLISION, level );

	}



	public void playSound( int sound, float level ) {
		
		gameController.game.audio().playSound( sound, gameObject, level, false );
		
	}



	public bool isPlaying() {
		
		return gameController.game.audio().isPlaying( gameObject );
		
	}

}
