﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Blocs {

	private List<GameObject> blocslist;
	private List<BlocState>  blocsStateList;

	struct BlocState {

		public GameObject gameobject;
		public float      drag;
		public float      angularDrag;

	};

	// Constructor
	public Blocs() {

		blocslist = new List<GameObject>();

	}



	public void drawBlocsWall( Vector3 pos, int width, int height, GameObject bloc, Game game ) {

		float widthX = Mathf.Floor (Mathf.Min (Mathf.Abs (game.getFieldSize ().xMin ()), game.getFieldSize ().xMax ()));
		float widthY = Mathf.Floor (game.getFieldSize ().zMax ());
		float widthZ = Mathf.Floor (Mathf.Min (Mathf.Abs (game.getFieldSize ().zMin ()), game.getFieldSize ().zMax ()));
		
		float blocSizeX = bloc.transform.localScale.x;
		float blocSizeY = bloc.transform.localScale.y;
		float blocSizeZ = bloc.transform.localScale.z;
		
		float wallHeight;
		
		float borderX = 0.5f;
		float borderZ = 3.5f;
		
		int heightNum   = 10;
		int maxBlocsnum = 160;
		int blocsNum    = 0;
		
		Debug.Log (widthX);
		Debug.Log (widthY);
		Debug.Log (widthZ);
		Debug.Log (blocSizeY);
		
		for ( int side = 0; side < 2; ++side ) {
			
			float inv = 1.0f;
			
			if( side == 1 )
				inv = -1.0f;
			
			for (float z = borderZ; z < game.getFieldSize().zMax() - borderZ; z += blocSizeZ) {
				
				wallHeight = (float) height * blocSizeY;
				
				if (Random.value < 0.9f)
					continue;
				
				for (float x = game.getFieldSize().xMin() + borderX; x < game.getFieldSize().xMax() - borderX; x += blocSizeX) {
	
					if (Random.value < 0.5f) {
						wallHeight = Random.Range (blocSizeY, (float)heightNum * blocSizeY);
						continue;
					}
					
					for (float y = blocSizeY / 2.0f; y <= wallHeight; y += blocSizeY) {

						GameObject newbloc = GameController.Instantiate (bloc);
						newbloc.transform.position = new Vector3 (x, y, inv * z);

						blocslist.Add( newbloc );

                        blocsNum++;
						
						if (blocsNum > maxBlocsnum)
							break;
					}
					
					if (blocsNum > maxBlocsnum)
						break;					
					
				}
				
				if (blocsNum > maxBlocsnum)
					break;
				
			}
			
			blocsNum = 0;
			
		}
		
	}



	public void drawRandomBlocsWall( int maxBlocsnum, GameObject[] blocs, Game game ) {

		if ( blocs == null || blocs.Length == 0 || blocs[0] == null )
			return;

		float widthX = Mathf.Floor (Mathf.Min (Mathf.Abs (game.getFieldSize ().xMin ()), game.getFieldSize ().xMax ()));
		float widthY = Mathf.Floor (game.getFieldSize ().zMax ());
		float widthZ = Mathf.Floor (Mathf.Min (Mathf.Abs (game.getFieldSize ().zMin ()), game.getFieldSize ().zMax ()));
				
		float wallHeight;

		float blocSizeX = blocs[0].transform.localScale.x;// 1.5f;// + 0.01f;
		float blocSizeY = blocs[0].transform.localScale.y;// + 0.01f;
		float blocSizeZ = blocs[0].transform.localScale.z;// + 0.01f;

		float borderX = 5.0f;
		float borderZ = 5.5f;
		
		int heightNum   = 10;
		int blocsNum    = 0;
		
		for ( int side = 0; side < 2; ++side ) {
			
			float inv = 1.0f;
			
			if( side == 1 )
				inv = -1.0f;
			
			for (float z = borderZ; z <= game.getFieldSize().zMax() - borderZ; z += blocSizeZ  ) {//+ Random.Range( 0.0f, 1.0f ) ) {
				
				wallHeight = Random.Range (blocSizeY, (float)heightNum * blocSizeY);// widthY );
				
				if (Random.value < 0.9f)
					continue;
				
				for (float x = game.getFieldSize().xMin() + borderX; x <= game.getFieldSize().xMax() - borderX; x += blocSizeX) {

					int blocType = (int)Mathf.Ceil (Random.Range (0, blocs.Length));

					if (Random.value < 0.5f) {
						wallHeight = Random.Range (blocSizeY, (float)heightNum * blocSizeY);
						continue;
					}
					
					for (float y = blocSizeY / 2.0f; y <= wallHeight; y += blocSizeY) {					
						//blocType = (int)Mathf.Ceil (Random.Range (0, blocs.Length));

						GameObject bloc = GameController.Instantiate (blocs [blocType]);
						blocslist.Add( bloc );
                        blocs[0] = bloc;
						bloc.transform.position = new Vector3 (x, y, inv * z);//( (float) side - 1.0f )*
                        blocsNum++;
						
						if (blocsNum > maxBlocsnum)
							break;
					}
					
					if (blocsNum > maxBlocsnum)
						break;					
					
				}
				
				if (blocsNum > maxBlocsnum)
					break;
				
			}
			
			blocsNum = 0;
			
		}
		
	}

	

	public void setBlocsKinematic( bool state ) {

		foreach ( GameObject bloc in blocslist ) {
			
			Rigidbody rb       = bloc.GetComponent<Rigidbody>();
			rb.isKinematic     = state;
			rb.velocity        = new Vector3( 0.0f, 0.0f, 0.0f );
			rb.angularVelocity = new Vector3( 0.0f, 0.0f, 0.0f );

		}
		
	}



	public void destroyBlocs() {

		foreach (GameObject bloc in blocslist) {
			
			Object.Destroy(bloc);
			
		}

		blocslist.Clear();
		
	}



	public void zeroGravity( float min, float max ) {

		blocsStateList = new List<BlocState>();

		foreach ( GameObject bloc in blocslist ) {

			Rigidbody rb      = bloc.GetComponent<Rigidbody>();

			BlocState state;
			state.gameobject  = bloc;
			state.drag        = rb.drag;
			state.angularDrag = rb.angularDrag;

			blocsStateList.Add( state );

			float vx  = Random.value <= 0.5 ? Random.Range( min, max ) : Random.Range( -max, -min );
			float vy  = Random.value <= 0.5 ? Random.Range( min, max ) : Random.Range( -max, -min );
			float vz  = Random.value <= 0.5 ? Random.Range( min, max ) : Random.Range( -max, -min );
			float vax = Random.value <= 0.5 ? Random.Range( min, max ) : Random.Range( -max, -min );
			float vay = Random.value <= 0.5 ? Random.Range( min, max ) : Random.Range( -max, -min );
			float vaz = Random.value <= 0.5 ? Random.Range( min, max ) : Random.Range( -max, -min );

			rb.velocity        = new Vector3( vx,  vy,  vz );
			rb.angularVelocity = new Vector3( vax, vay, vaz );
			rb.drag            = 0.0f;
			rb.angularDrag     = 0.0f;

		}
		
	}



	public void normalGravity() {

		if ( blocsStateList == null )
			return;

		foreach ( BlocState bloc in blocsStateList ) {
			
			Rigidbody rb       = bloc.gameobject.GetComponent<Rigidbody>();
			rb.velocity        = new Vector3( 0.0f, 0.0f, 0.0f );
			rb.angularVelocity = new Vector3( 0.0f, 0.0f, 0.0f );
			rb.drag            = bloc.drag;
			rb.angularDrag     = bloc.angularDrag;
			
		}
		
	}

}
