using UnityEngine;
using System.Collections;

public class BallScript : MonoBehaviour {

   public  Ball                  ball;
   private static GameController gameController;
   private float                 minVelocity = 0.5f;



   void Start () {	

     	GameObject gameControllerObject = GameObject.FindWithTag ("GameController");

     	if ( gameControllerObject != null ) {

       		gameController = gameControllerObject.GetComponent <GameController>();

     	}

     	if ( gameController == null ) {

       		Debug.Log ("Cannot find 'GameController' script");

     	}

		ball = gameController.game.getBallFromGameObject( transform.gameObject );

   }
   


   void FixedUpdate () {

		// TODO: Check the value of 0.5!
		if ( ball != null && ( ball.getSpeed() < minVelocity ) && ball.getPosition().y <= 0.5f ) { // !ball.isGrabbed() && ( ball.getThrower() != null )

			ball.stop();
			ball.setThrower( null );
			ball.setRebound( false );

			if ( ball.isPlaying( AudioController.AUDIO_FLYING_BALL ) || ball.getRebound() ) {

				ball.stopPlaying( AudioController.AUDIO_FLYING_BALL );

			}

		}

		// Throwed ball moving speed sound (has a thrower on is not a rebound)
		if ( !ball.isPlaying( AudioController.AUDIO_FLYING_BALL ) && ball.getSpeed() >= 0.5f 
		   && ball.getThrower() != null && !ball.getRebound() ) {

			ball.playSound( AudioController.AUDIO_FLYING_BALL, ball.getSpeed() / 30.0f, true );

		} else if ( ball.isPlaying( AudioController.AUDIO_FLYING_BALL ) ) {

			ball.setSoundVolume( AudioController.AUDIO_FLYING_BALL, ball.getSpeed() / 30.0f );
			ball.setSoundPitch( AudioController.AUDIO_FLYING_BALL, ball.getSpeed() / 30.0f );

		}

		// Stop the mouvement sound on rebounds
		if ( ball.getRebound() )
			ball.stopPlaying( AudioController.AUDIO_FLYING_BALL );			

		// Magnet ball mode?
		if ( !ball.getRebound() && ball.getThrower() == gameController.game.getMainPlayer() 
		  && gameController.game.getConfigs().getMagnetBalls() ) {
			
			int        layerMask = 1 << 12; // Layers spaceships = 12, ballcollider = 18  
			RaycastHit hit;
			bool       rayCastResult = Physics.SphereCast( transform.position, gameController.getConfigs().getMagnetBallsRadius(), ball.getVelocity().normalized, out hit, 1000, layerMask );			      
			
			if ( rayCastResult && hit.collider != null ) {

				if ( ball.getThrower() != null && hit.collider.GetComponent<PlayerObject>() != null 
				  && ball.getThrower() != hit.collider.GetComponent<PlayerObject>().playerObj ) {

					float   speed          = ball.getVelocity ().magnitude;
					Vector3 magnetVelocity = ( hit.collider.transform.position - transform.position ).normalized * speed;

					ball.setVelocity( magnetVelocity );

				}
				
			}
			
		}

   }



   // TODO: MAKE A REFACTORING OF THE FUNCTION, THE BALL GIVE AWAY CODE SHOULD BE A FUNCTION
   void OnCollisionEnter ( Collision col ) {

		if ( gameController == null )
			return;

		Game game = gameController.game;

		// The ball has been rebounded
		ball.setRebound( true );
	 
	    // Is the collision object a player?
		if ( col.gameObject.GetComponent<PlayerObject>() == null ) {

			// Is the collider a Wall or a Frontier object?
			if ( ( col.gameObject.layer == 14 || col.gameObject.layer == 8 ) 
			    && ball.getSpeed() >= 1.0f && !ball.isPlaying( AudioController.AUDIO_BALL_HIT_WALLS ) ) {

				ball.playSound( AudioController.AUDIO_BALL_HIT_WALLS, ball.getSpeed() / 60.0f );

			}

			// Is the collider a Bloc?
			if ( col.gameObject.layer == 23 && ball.getSpeed() >= 1.0f )
				ball.playSound( AudioController.AUDIO_BALL_HIT_BLOCS, ball.getSpeed() / 60.0f );

			return;

		}

		Player player   = col.gameObject.GetComponent<PlayerObject>().playerObj;
		Ball   hitball  = game.getBallFromGameObject( transform.gameObject );

		// TODO: Why to I need this to prevent some null objects error sometimes (rarely)
		//       There's a bug somewhere...
		if ( hitball == null )
			return;

		Player thrower  = hitball.getThrower();
		bool   gotojail = true;

//		if ( player.isMainPlayer() )
//			Debug.Log ( player.hasLaser() );

		// If not a player or having a laser (cannot grab a ball when having the laser)
		if ( player == null || player.hasLaser() ) {
			
			return;
			
		}

		// If there's no thrower or the player is the thrower, just return
		if ( thrower == null || thrower == player )
			return;

		if ( player != null && ( hitball.getSpeed() >= game.getMinBallHitSpeed() ) ) {

			// Invisibility
			if ( game.getConfigs().invisibility && player.isMainPlayer() ) {

				Debug.Log( "The main player was hit, but is invisible!" );

				return;

			}

			// Pass a ball to a teammate
			if ( thrower.getTeamNo() == player.getTeamNo() ) {
				
				game.giveBall( player, ball );
				
				return;
				
			}		

			// The player looses the ball
			if ( player.ownBall() ) { // TODO: TEST FIX BUG GRAB BALL WHEN ALREADY HAS A BALL && thrower.getTeamNo() != player.getTeamNo() ) {

				Ball playerBall = player.getBall();

				player.giveAwayBall();

				// Play the sound
				if ( player.isMainPlayer() )
					player.playSound( AudioController.AUDIO_PLAYER_LOSE_BALL );

				if ( playerBall != null ) {

					Vector3 pos = player.getPosition();

					if ( pos.y < ( game.getFieldSize().yMax() - 3.0f ) )
						pos.y = pos.y + 2.0f;
					else
						pos.y = pos.y - 2.0f;

					pos.z = pos.z - 1.0f;

					playerBall.setPosition( pos );

					// TODO: Ajust the value 20.0f by the velocity of the ball hiting the player
					playerBall.setVelocity( player.getVelocity() / 20.0f );

					playerBall.show();

				}

				gotojail = false;

			}

			if ( !player.isPrisoner() && thrower.getTeamNo() != player.getTeamNo() ) {

				// Add the hit to the hit counter for the player
				player.hit ();

				// Play the sound
				if ( player.isMainPlayer() )
					player.playSound( AudioController.AUDIO_PLAYER_HIT );

				// Increment the thrower's target hit counter
				thrower.targetHit();

			    if ( player.getTeamNo() == 1 )
				    gameController.AjouterPointage( 1, "rouge" );
			    else
				    gameController.AjouterPointage( 1, "bleu" );							    

				// Is the thrower a prisoner
				if ( thrower.isPrisoner() ) {
					
					float zOffset;
		
					if ( thrower.getTeamNo() == 0 )						
						zOffset = -5.0f;						
					else						
						zOffset = 5.0f;			

					// Play the get out of jail sound
					if ( thrower.isMainPlayer() )
						thrower.playSound( AudioController.AUDIO_PLAYER_GETOUT_JAIL );
					else
						thrower.playSound( AudioController.AUDIO_SHIP_GETOUT_JAIL );

					// Get out of the jail
					thrower.explodeGetOutOfJail( new Vector3 ( game.getFieldSize().centerX(), game.getFieldSize().centerY(), game.getFieldSize().centerZ() + zOffset ) );

				}

				// The thrower lost the targetLock if it is the main player
				if ( thrower.isMainPlayer() )
					thrower.getObject().GetComponent<MovePlayer>().lostTargetLock();

				// The player lost the targetLock if it is the main player
				if ( player.isMainPlayer() )
					player.getObject().GetComponent<MovePlayer>().lostTargetLock();

				// Show explosion halo and send the player to jail
				if ( gotojail ) {

					// Explosion sound
					player.playSound( AudioController.AUDIO_TARGET_EXPLODE, 1.0f );

					// Play the jail sound if main player
					if ( player.isMainPlayer() ) {

						player.playSound( AudioController.AUDIO_PLAYER_GOTO_JAIL );

					}

					player.explodeAndSendToJail();

				}

            }

     	}

    }



	void OnCollisionExit (Collision col) {


	}

}
