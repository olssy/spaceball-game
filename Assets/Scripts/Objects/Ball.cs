﻿/* Ball
 * 
 * constructor:
 * 
 *   Ball( int type, Vector3 pos, float hitdamage, int speed )
 * 
 * methods:
 * 
 *   void       reinitBall()
 *   void       setPosition( Vector3 pos )
 *   void       setLocalPosition( Vector3 pos )
 *   Vector3    getPosition()
 *   void       stop()
 *   void       setVelocity( Vector3 velocity )
 *   Vector3    getVelocity()
 *   void       setAngularVelocity( Vector3 velocity )
 *   Vector3    getAngularVelocity()
 *   float      getSpeed()
 *   void       setSpeed( float speed )
 *   void       setAngularSpeed( float speed )
 *   float      getAngularSpeed()
 *   void       show()
 *   void       showGrabbed()
 *   void       hide()
 *   void       hideGrabbed()
 *   short      getFieldSide()
 *   bool       isGrabbed()
 *   void       setGrabber( Player player )
 *   void       unGrab()
 *   Player     getGrabber()
 *   void       setThrower( Player player )
 *   Player     getThrower()
 *   GameObject getBallObj()
 *   GameObject getGrabbedBallObj()
 *   int        getType()
 *   void       setRebound( bool val )
 *   bool       getRebound()
 *   void       configBallQuality()
 *   void       playSound( int sound )
 *   void       playSound( int sound, float level )
 *   void       playSound( int sound, float level, bool loop )
 *   bool       isPlaying()
 *   bool       isPlaying( int sound )
 *   bool       stopPlaying( int sound )
 *   void       setSoundVolume( int sound, float level )
 *   void       setSoundPitch( int sound, float pitch )
 * 
 */



using UnityEngine;
using System.Collections;

public class Ball {

	private int            ballIndex; // TODO: Needed?
	private int            ballType;
	private GameObject     ballObj        = null;
	private GameObject     grabbedBallObj = null;
	private float          damage         = 10.0f;
	private int            maxSpeed;
	private Player         owner          = null;
	private Player         thrower        = null;
	private bool           noRebound      = true;
	private GameController gameController;



	// Constructor
	public Ball( int type, Vector3 pos, float hitdamage, int speed ) {
		
		this.damage   = hitdamage;
		this.maxSpeed = speed;
		this.ballType = type;

		this.gameController = GameObject.FindWithTag ("GameController").GetComponent <GameController>();

		if (gameController == null) {
			
			Debug.LogError( "Cannot find 'GameController' script" );
			
		}

		GameObject newballObj = this.gameController.ballObjects[type];

		if ( newballObj == null )
			return; // Ball not found

		GameObject gameBallObj = GameController.Instantiate( newballObj );

		// Attatch the Ball object to the GameObject of the ball
		gameBallObj.GetComponent<BallScript>().ball = this;

		// Place the ball at its position
		gameBallObj.transform.position = pos;

		this.ballObj = gameBallObj;

		// Create the grabbed ball object
		GameObject newGrabbedBallObj = GameObject.FindWithTag ("GameController").GetComponent <GameController>().ballGrabbedObjects[type];
		
		if ( newGrabbedBallObj == null )
			return; // Ball not found
		
		GameObject gameGrabbedBallObj = GameController.Instantiate( newGrabbedBallObj );

		gameGrabbedBallObj.SetActive( false );

		this.grabbedBallObj = gameGrabbedBallObj;

		// Configure the quality of the lights and shadows of the ball
		configBallQuality();
		
	}
	


	/* reinitBall()
	 * 
	 * Reinitialisation of the ball
	 * 
	 */
	public void reinitBall() {
		
		owner   = null;
		thrower = null;


        

    }



	/* setPosition( Vector3 pos )
	 * 
	 * Returns the position of the ball
	 * 
	 */
	public void setPosition( Vector3 pos ) {
		
		this.ballObj.transform.position             = pos;
		this.grabbedBallObj.transform.localPosition = pos;

	}



	/* setLocalPosition( Vector3 pos )
	 * 
	 * Returns the local position of the ball
	 * 
	 */
	public void setLocalPosition( Vector3 pos ) {
		
		this.ballObj.transform.localPosition = pos;
		
	}




	/* getPosition()
	 * 
	 * Returns the position of the ball
	 * 
	 */
	public Vector3 getPosition() {
		
		return this.ballObj.transform.position;
		
	}



	/* setVelocity( Vector3 velocity )
	 * 
	 * Set the velocity of the ball (a speed + a direction)
	 * 
	 */
	public void setVelocity( Vector3 velocity ) {
		
		this.ballObj.GetComponent<Rigidbody>().velocity = velocity;
		
	}



	/* getVelocity()
	 * 
	 * Returns the velocity of the ball (a speed + a direction)
	 * 
	 */
	public Vector3 getVelocity() {
		
		return this.ballObj.GetComponent<Rigidbody>().velocity;
		
	}



	/* setAngularVelocity( Vector3 velocity )
	 * 
	 * Set the angular velocity of the ball (a speed + a direction)
	 * 
	 */
	public void setAngularVelocity( Vector3 velocity ) {
		
		this.ballObj.GetComponent<Rigidbody>().angularVelocity = velocity;
		
	}
	
	
	
	/* getAngularVelocity()
	 * 
	 * Returns the angular velocity of the ball (a speed + a direction)
	 * 
	 */
	public Vector3 getAngularVelocity() {
		
		return this.ballObj.GetComponent<Rigidbody>().angularVelocity;
		
	}



	/* getSpeed()
	 * 
	 * Returns the speed of the ball (a float without direction)
	 * 
	 */
	public float getSpeed() {
		
		return this.ballObj.GetComponent<Rigidbody>().velocity.magnitude;
		
	}



	/* setSpeed( float speed )
	 * 
	 * Change the speed of the ball (a float without direction)
	 * 
	 */
	public void setSpeed( float speed ) {

		// TODO: To change, I this it could simply be done like this
		// this.ballObj.GetComponent<Rigidbody>().velocity = this.ballObj.GetComponent<Rigidbody>().velocity.normalized * speed

		float multiplier = ( speed * speed ) / this.ballObj.GetComponent<Rigidbody>().velocity.magnitude;

		if ( multiplier == float.NaN || speed == 0.0f )
			multiplier = 0.0f;

		this.ballObj.GetComponent<Rigidbody>().velocity *= multiplier;
		
	}




	/* float getAngularSpeed()
	 * 
	 * Get the angular speed of the ball (a float without direction)
	 * 
	 */
	public float getAngularSpeed() {

		return this.ballObj.GetComponent<Rigidbody>().angularVelocity.magnitude;
		
	}



	/* void stop()
	 * 
	 * Stop the ball mouvement
	 * 
	 */ 
	public void stop() {	

		setSpeed( 0.0f );
		setAngularSpeed( 0.0f );
		Rigidbody rb       = this.ballObj.GetComponent<Rigidbody>();

		// Toggle on and off isKinematic to reset the ball's momentum
		rb.isKinematic     = true;
		rb.isKinematic     = false;
		
	}



	/* setAngularSpeed( float speed )
	 * 
	 * Change the angular speed of the ball (a float without direction)
	 * 
	 */
	public void setAngularSpeed( float speed ) {

		// TODO: To change, I this it could simply be done like this
		// this.ballObj.GetComponent<Rigidbody>().angularVelocity = this.ballObj.GetComponent<Rigidbody>().angularVelocity * speed

		float multiplier = ( speed * speed ) / this.ballObj.GetComponent<Rigidbody>().angularVelocity.magnitude;

		if ( multiplier == float.NaN || speed == 0.0f )
			multiplier = 0.0f;

		this.ballObj.GetComponent<Rigidbody>().angularVelocity *= multiplier;
		
	}



	/* show()
	 * 
	 * Show the "bouncing" ball GameObject
	 * 
	 */
	public void show() {
		
		this.ballObj.SetActive( true );

	}



	/* showGrabbed()
	 * 
	 * Show the "grabbed" ball GameObject
	 * 
	 */
	public void showGrabbed() {

		this.grabbedBallObj.SetActive( true );
		
	}



	/* hide()
	 * 
	 * Hide the "bouncing" ball GameObject
	 * 
	 */	
	public void hide() {
		
		this.ballObj.SetActive( false );
		
	}



	/* hideGrabbed()
	 * 
	 * Hide the "grabbed" ball GameObject
	 * 
	 */	
	public void hideGrabbed() {
		
		this.grabbedBallObj.SetActive( false );
		
	}


	
	/* getFieldSide()
	 * 
	 * Returns the number of the field side's team the ball is in
	 * 
	 */	
	public short getFieldSide() {

		// TODO: Check this fonction!
		if ( getPosition().z > gameController.game.getFieldSize().centerZ() )
			return 1; // The ball is in team 1's side
		else
			return 0; // The ball is in team 0's side
		
	}
	
	

	/* isGrabbed()
	 * 
	 * Returns if the ball is grabbed by a player
	 * 
	 */	
	public bool isGrabbed() {
		
		if ( this.owner != null )
			return true;
		else
			return false;
		
	}
	
	

	/* setGrabber( Player player )
	 * 
	 * Set the player who grabs the ball
	 * 
	 */	
	public void setGrabber( Player player ) {

		this.owner   = player;

		if ( player != null )
			this.thrower = null;
		
	}
	
	

	/* unGrab()
	 * 
	 * No player is grabbing the ball
	 * 
	 */
	public void unGrab() {
		
		this.owner = null;
		
	}
	
	

	/* getGrabber()
	 * 
	 * Returns the Player object of the player who grabs the ball
	 * 
	 */
	public Player getGrabber() {

		return this.owner;
		
	}



	/* setThrower( Player player )
	 * 
	 * Set the player who throws the ball
	 * 
	 */	
	public void setThrower( Player player ) {
		
		this.thrower = player;
		
	}



	/* getThrower()
	 * 
	 * Get the player who throws the ball
	 * 
	 */	
	public Player getThrower() {
		
		return this.thrower;
		
	}



	/* getBallObj()
	 * 
	 * Returns the GameObject associated with the ball
	 * 
	 */
	public GameObject getBallObj() {

		return this.ballObj;
		
	}


	/* getGrabbedBallObj()
	 * 
	 * Returns the GameObject associated with the ball when it is grabbed
	 * 
	 */
	public GameObject getGrabbedBallObj() {
		
		return this.grabbedBallObj;
		
	}



	/* getType()
	 * 
	 * Returns the type of the the ball
	 * 
	 */
	public int getType() {
		
		return this.ballType;
		
	}



	/* void setRebound( bool val )
	 * 
	 * The ball has rebounded or not
	 * 
	 */
	public void setRebound( bool val ) {

		this.noRebound = val;

	}



	/* getRebound()
	 * 
	 * Returns true if the ball has rebounded
	 * 
	 */
	public bool getRebound() {
		
		return this.noRebound;
		
	}



	/* playSound( int sound )
	 * playSound( int sound, float level )
	 * playSound( int sound, float level, bool loop )
	 * 
	 * Play a sound
	 * 
	 */
	public void playSound( int sound ) {
		
		gameController.audio().playSound( sound, ballObj, 1.0f, false );
		
	}

	public void playSound( int sound, float level ) {
		
		gameController.audio().playSound( sound, ballObj, level, false );
		
	}

	public void playSound( int sound, float level, bool loop ) {

		gameController.audio().playSound( sound, ballObj, level, loop );

	}



	/* isPlaying()
	 * isPlaying( int sound )
	 * 
	 * Is playing a sound?
	 * 
	 */	
	public bool isPlaying() {
		
		return gameController.audio().isPlaying( ballObj );
		
	}

	public bool isPlaying( int sound ) {

		return gameController.audio().isPlaying( sound, ballObj );

	}



	/* stopPlaying( int sound )
	 * 
	 * Stop a sound
	 * 
	 */	
	public bool stopPlaying( int sound ) {
		
		return gameController.audio().stopSound( sound, ballObj );
		
	}



	/* setSoundVolume( int sound, float level )
	 * 
	 * Set the level of a sound
	 * 
	 */	
	public void setSoundVolume( int sound, float level ) {

		gameController.audio().setVolume( sound, ballObj, level );

	}



	/* setSoundPitch( int sound, float pitch ) 
	 * 
	 * Set the pitch of a sound
	 * 
	 */	
	public void setSoundPitch( int sound, float pitch ) {
		
		gameController.audio().setPitch( sound, ballObj, pitch );
		
	}



	/* configBallQuality()
	 * 
	 * Configure the quality of the lights and shadows of the ball
	 */ 
	private void configBallQuality() {

		Light[] lights = this.ballObj.GetComponentsInChildren<Light>();
		
		foreach  ( Light l in lights ) {

			if ( !gameController.game.getConfigs().getAnimatedShadows() ) {

			    l.shadows = LightShadows.None;

			} else { // Enable shadows and config quality
				
				if ( gameController.game.getConfigs().getHighQualityShadows() )
					l.shadows = LightShadows.Soft;
				else
					l.shadows = LightShadows.Hard;
				
			}

			if ( !gameController.game.getConfigs().getAnimatedLights() ) {
				
				//if ( l.name.Equals("Shadow Light" ) ) {
					
					l.enabled = false;
					
				//}

			}


		}

		if ( !gameController.game.getConfigs().getTrailRenderer() && this.ballObj != null )
			this.ballObj.GetComponent<TrailRenderer>().enabled = false;
		else

			this.ballObj.GetComponent<TrailRenderer>().enabled = true;
	}

}
