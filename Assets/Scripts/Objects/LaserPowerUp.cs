﻿using UnityEngine;
using System.Collections;

public class LaserPowerUp : MonoBehaviour {

	private static GameController gameController;
	private float timer = 0;
	private float respawnTime = 45;
	private float powerUpTime = 20;
	private Player player;
	private bool hasPowerUp = false;
	private Renderer rend;
	private Light light;

	// Use this for initialization
	void Start () {

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		
		if ( gameControllerObject != null ) {
			
			gameController = gameControllerObject.GetComponent <GameController>();
			
		}
		
		if ( gameController == null ) {
			
			Debug.Log ("Cannot find 'GameController' script");
			
		}

		rend = GetComponent<Renderer>();
		rend.enabled = true;

		light = gameObject.GetComponent<Light>();

	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (0,20*Time.deltaTime,0); // rotation du power up

		if ( timer > respawnTime ) {
			this.resetPowerUp();
		}
		if (timer > 0 && hasPowerUp )
		{
			timer += Time.deltaTime;
		}
		if (timer > powerUpTime && hasPowerUp) {
			disableLaser();
		}
	}

	/*
	 * collision avec le Power Up
	 * Seulement le joueur principal peut rentrer en collision avec le power up.
	 * */
	void OnCollisionEnter ( Collision col ) {
		if (!hasPowerUp) {

			player = col.gameObject.GetComponent<PlayerObject> ().playerObj;

			// Cannot get the laser if owning a ball
			if ( !player.ownBall() && player.isMainPlayer() )
				this.getPowerUp ();

		}

	}

	void getPowerUp() {
		timer = 0.01f;
		player.setLaser (true);
		rend.enabled = false;
		light.enabled = false;
		hasPowerUp = true;

		// Play get laser sound
		gameController.game.playSound( AudioController.AUDIO_GET_LASER );

	}

	public void resetPowerUp() {
		disableLaser ();
		rend.enabled = true;
		light.enabled = true;
		timer = 0;
		hasPowerUp = false;
		timer = 0;
		respawnTime = 45;
		powerUpTime = 20;
	}

	void disableLaser() {
		if (player != null) {
			player.setLaser (false);

			// Play get laser lost sound
			gameController.game.playSound( AudioController.AUDIO_LASER_LOOSE );

		}
		GameObject.FindWithTag ("Level").GetComponent<Level> ().setLevel (0);
	}

	void hide() {

	}

	void show() {
	
	}

}
