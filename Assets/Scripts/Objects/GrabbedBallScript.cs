using UnityEngine;
using System.Collections;

public class GrabbedBallScript : MonoBehaviour {

   public float rotationSpeed = 200.0f;



   void Start () {	

		transform.rotation = new Quaternion( 0.0f, 0.0f, 0.0f, 0.0f );

   }
   


   void Update () {

      	transform.Rotate( new Vector3( 1.0f, 0.0f, 0.0f ), Time.deltaTime * rotationSpeed );
   
   }

}
