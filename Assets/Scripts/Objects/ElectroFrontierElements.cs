﻿using UnityEngine;
using System.Collections;

public class ElectroFrontierElements : MonoBehaviour {

	void Start () {
	
	}

	void Update () {

		Color color = GetComponent<Renderer>().material.color;

		Color newColor = color;


		if ( Random.value > 0.8f ) {

			newColor.r = Random.Range( 0.3f, 1.0f );

			GetComponent<Renderer>().material.color =  Color.Lerp( color, newColor, Time.deltaTime * 30.0f );

		} else if ( Random.value > 0.5f ) {

			newColor.r = 1.0f;
			
			GetComponent<Renderer>().material.color =  Color.Lerp( color, newColor, Time.deltaTime * 30.0f );

		}

	}

}
