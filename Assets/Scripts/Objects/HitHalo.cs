﻿using UnityEngine;
using System.Collections;

public class HitHalo : MonoBehaviour {

	private float duration = 1.0f;
	private Light halo;
	private float starttime;
	private float minIntensity = 3.0f;
	private float maxIntensity = 8.0f;
	private float size         = 6.0f;

	void Start () {
	
		halo      = GetComponent<Light>();
		starttime = Time.time;
	}

	void FixedUpdate () {
	
		float phi       = ( Time.time - starttime ) / duration * 2 * Mathf.PI;
		float amplitude = Mathf.Sin(phi);
		halo.intensity  = amplitude * maxIntensity + minIntensity;
		halo.range      = amplitude * size;

		if ( phi >= Mathf.PI )
			Destroy( transform.gameObject );

	}

}
