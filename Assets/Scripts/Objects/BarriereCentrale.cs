﻿using UnityEngine;
using System.Collections;

public class BarriereCentrale : MonoBehaviour {

	private static GameController  gameController;
//	private ElectroFrontier frontier;

	// Use this for initialization
	void Start () {
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		
		if ( gameControllerObject != null ) {
			
			gameController = gameControllerObject.GetComponent <GameController>();
			
		}
		
		if ( gameController == null ) {
			
			Debug.Log ("Cannot find 'GameController' script");
			
		}

//		GameObject gameFrontiere = GameObject.FindWithTag ("ElectroFrontier");
//		
//		if ( gameFrontiere != null ) {
//			
//			frontier = gameFrontiere.GetComponent <ElectroFrontier>();
//			
//		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col) {

//		if ( frontier == null )
//			Start();

		if ( col.gameObject.GetComponent<PlayerObject>() == null )
			return;

		Player player = col.gameObject.GetComponent<PlayerObject>().playerObj;

		// Invisibility
		if ( player.getGame().getConfigs().invisibility && player.isMainPlayer() ) {
			
			Debug.Log( "The main player pass the frontier, but is invisible!" );
			
			return;
			
		}

		if ( (this.gameObject.transform.name == "BarriereBleue" && player.getTeamNo() == 0) ||
		     (this.gameObject.transform.name == "BarriereRouge" && player.getTeamNo() == 1) ) {

			// The player looses the ball if he has one
			if ( player.ownBall() )
				player.giveAwayBall();

			// Explosion sound		
			player.playSound( AudioController.AUDIO_FRONTIER_TRESPASS, 1.0f );
			
			// Play the jail sound if main player
			if ( player.isMainPlayer() )
				player.playSound( AudioController.AUDIO_PLAYER_GOTO_JAIL );

			player.explodeAndSendToJail();
            
//			if ( frontier != null )
//				frontier.setTespassing( false );

		}

	}
}
