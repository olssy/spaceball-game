﻿using UnityEngine;
using System.Collections;

public class BallGrabberScript : MonoBehaviour {



	void OnTriggerEnter ( Collider other ) {

		Player player = transform.parent.GetComponent<PlayerObject>().playerObj;

		if ( ( player != null ) && ( player.ownBall() == false ) ) {

			// Get the players Ball object
			Ball ball = player.getGame().getBallFromGameObject( other.gameObject );
						
			// Give the Player the ball
			if ( ball != null ) {

				player.getGame().giveBall( player, ball );

				// Play the sound
				if ( player.isMainPlayer() )
					player.playSound( AudioController.AUDIO_PLAYER_GRAB_BALL );

			}

		} 

	}



	void OnTriggerStay ( Collider other ) {


	}



	void OnTriggerExit ( Collider other ) {

//		Player player = transform.parent.GetComponent<PlayerObject>().playerObj;
//
//		if ( player != null && player.getNeedToGiveAwayBall() )
//			player.giveAwayBall();
		
	}

}
