﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Controller : MonoBehaviour {

	public const int ACTION_NONE      = 0;
	public const int ACTION_WAITING   = 1;
	public const int ACTION_WANDERING = 2;
	public const int ACTION_FINDBALL  = 3;
	public const int ACTION_GETBALL   = 4;
	public const int ACTION_PASS      = 5;
	public const int ACTION_FIRE      = 6;

	private Game   game;
	private Player player;

	private float  currentSpeed;
	private float  lastProcessTimeout;
	private float  processTimeout     = 0.1f;
	private float  maxSpeed           = 50.0f;
	private float  minSpeed           = 20.0f;
	private float  wanderingMinSpeed  = 15.0f; // Min time in secondes for each wandering paths
	private float  wanderingMaxSpeed  = 5.0f;  // Max time in secondes for each wandering paths
	private float  minThrowSpeed      = 10.0f;
	private float  maxThrowSpeed      = 100.0f;
	private float  minActionTime      = 10.0f;
	private float  maxActionTime      = 60.0f;
	private float  actionTime         = 0.0f;
	private float  actionStartTime    = 0.0f;
	private int    action             = ACTION_NONE;
	private Ball   targetBall         = null;

	// For the random movements method
	private float speedX              = 1.0f;
	private float speedY              = 1.0f;
	private float speedZ              = 1.0f;
	private float minForwardSpeed     = 1.0f;
	private float maxForwardSpeed     = 8.0f;
	private float minSmoothSpeed      = 5.0f;
	private float maxSmoothSpeed      = 15.0f;
	private float prisonSlowDown      = 1.0f;
	private float prisonBackSpeed     = 0.6f;
	private bool  newMove             = false;

	// Throwing adjustments
	private float throwBallToMain     = 0.8f;
	private float throwBallToEnnemy   = 0.9f;
	private float throwToRandom       = 0.7f; 
	private float passToTeammate      = 0.9f;

	// Difficulties
	private float difficulty_findBall;
	private float difficulty_wandering;
	private float diff_speedChange;


	private SmoothRandomMouvement smoothmove;

	void Start () {

		game               = getGame();
		player             = getPlayer();
		lastProcessTimeout = Time.fixedTime;
		currentSpeed       = Random.value * ( maxSpeed - minSpeed ) + minSpeed;
		smoothmove         = gameObject.GetComponent<SmoothRandomMouvement>();

		switch ( game.getConfigs().getDifficulty() ) {

			case 1 : // Easy
				
				difficulty_findBall  = 0.6f;
				difficulty_wandering = 0.7f;
				diff_speedChange     = 0.4f;
			
				processTimeout       = 0.1f;
				maxSpeed             = 15.0f;
				minSpeed             = 5.0f;
				wanderingMinSpeed    = 3.0f;
				wanderingMaxSpeed    = 6.0f;
				minSmoothSpeed       = 2.0f;
				maxSmoothSpeed       = 6.0f;
				minThrowSpeed        = 10.0f;
				maxThrowSpeed        = 100.0f;
				minActionTime        = 1.0f;
				maxActionTime        = 8.0f; // this should be at least the speed of wanderingMaxSpeed
			
				throwBallToMain      = 0.5f;
				throwBallToEnnemy    = 0.6f;
				throwToRandom        = 0.5f; 
				passToTeammate       = 1.0f; // Teammate in prison!

				game.getConfigs().setMagnetBalls( true );
			    game.getConfigs().setMagnetBallsRadius( 4.0f );

			    break;

		    case 2 : // Normal
			
				difficulty_findBall  = 0.8f;
				difficulty_wandering = 0.7f;
				diff_speedChange     = 0.4f;
			
				processTimeout       = 0.1f;
				maxSpeed             = 15.0f;
				minSpeed             = 5.0f;
				wanderingMinSpeed    = 3.0f;
				wanderingMaxSpeed    = 7.0f;
				minSmoothSpeed       = 4.0f;
				maxSmoothSpeed       = 6.0f;
				minThrowSpeed        = 20.0f;
				maxThrowSpeed        = 100.0f;
				minActionTime        = 1.0f;
				maxActionTime        = 8.0f; // this should be at least the speed of wanderingMaxSpeed
			
				throwBallToMain      = 0.6f;
				throwBallToEnnemy    = 0.7f;
				throwToRandom        = 0.5f; 
				passToTeammate       = 1.0f; // Teammate in prison!

				game.getConfigs().setMagnetBalls( true );
				game.getConfigs().setMagnetBallsRadius( 1.0f );

		     	break;

		    case 3 : // Hardcore
			
			    difficulty_findBall  = 0.8f;
			    difficulty_wandering = 0.7f;
		    	diff_speedChange     = 0.4f;
			
			    processTimeout       = 0.1f;
			    maxSpeed             = 20.0f;
			    minSpeed             = 10.0f;
			    wanderingMinSpeed    = 4.0f;
			    wanderingMaxSpeed    = 8.0f;
			    minSmoothSpeed       = 3.0f;
			    maxSmoothSpeed       = 7.0f;
			    minThrowSpeed        = 20.0f;
			    maxThrowSpeed        = 110.0f;
		    	minActionTime        = 2.0f;
			    maxActionTime        = 8.0f; // this should be at least the speed of wanderingMaxSpeed

			    throwBallToMain      = 0.7f;
			    throwBallToEnnemy    = 0.7f;
			    throwToRandom        = 0.5f; 
			    passToTeammate       = 1.0f; // Teammate in prison!

				game.getConfigs().setMagnetBalls( false );

			    break;

		    case 4 : // Insane
			
				difficulty_findBall  = 0.9f;
				difficulty_wandering = 0.6f;
				diff_speedChange     = 0.4f;
			
				processTimeout       = 0.1f;
				maxSpeed             = 35.0f;
				minSpeed             = 20.0f;
				wanderingMinSpeed    = 4.0f;
				wanderingMaxSpeed    = 10.0f;
				minSmoothSpeed       = 3.0f;
				maxSmoothSpeed       = 7.0f;
				minThrowSpeed        = 40.0f;
				maxThrowSpeed        = 120.0f;
				minActionTime        = 6.0f;
				maxActionTime        = 10.0f; // this should be at least the speed of wanderingMaxSpeed
			
				throwBallToMain      = 0.7f;
				throwBallToEnnemy    = 0.7f;
				throwToRandom        = 0.5f; 
				passToTeammate       = 1.0f; // Teammate in prison!

				game.getConfigs().setMagnetBalls( false );

	     		break;

		    case 5 : // Suicidal

				difficulty_findBall  = 0.99f;
				difficulty_wandering = 0.3f;
				diff_speedChange     = 0.5f;
			
				processTimeout       = 0.1f;
				maxSpeed             = 35.0f;
				minSpeed             = 20.0f;
				wanderingMinSpeed    = 4.0f;
				wanderingMaxSpeed    = 10.0f;
				minSmoothSpeed       = 5.0f;
				maxSmoothSpeed       = 20.0f;
				minThrowSpeed        = 50.0f;
				maxThrowSpeed        = 120.0f;
				minActionTime        = 6.0f;
				maxActionTime        = 23.0f; // this should be at least the speed of wanderingMaxSpeed
			
				throwBallToMain      = 0.9f;
				throwBallToEnnemy    = 0.9f;
				throwToRandom        = 0.5f; 
				passToTeammate       = 1.0f; // Teammate in prison!

				game.getConfigs().setMagnetBalls( false );	

			    break;

		}

	}
	
	

	void FixedUpdate () {

		if ( player == null || game == null )
			return;


		// Horizontal stabilisation
		if ( transform.localRotation.eulerAngles.z % 360.0f >= 1.0f && transform.localRotation.eulerAngles.z % 360.0f <= 180.0f )
			transform.Rotate( 0.0f, 0.0f, -1.0f);

		if ( transform.localRotation.eulerAngles.z % 360.0f > 180.0f && transform.localRotation.eulerAngles.z % 360.0f <= 359.0f )			
			transform.Rotate( 0.0f, 0.0f, 1.0f );



		// The current action is over?
		if ( Time.fixedTime - actionStartTime > actionTime ) {

			if ( smoothmove != null )
				smoothmove.stop();

			action = ACTION_NONE;

		}

		// TODO: To remove, find another fix for the multi points path bug
		// Must stop the smooth movement if the player is a prisoner
		if ( player.isPrisoner() ) {

			if ( smoothmove != null )
				smoothmove.stop();

		}

		// State machine
		switch ( action ) {

			case ACTION_NONE :

			     //Random.Range( ACTION_WAITING, ACTION_FINDBALL );

			     if ( Random.value < difficulty_findBall )
					action = ACTION_FINDBALL;
		    	// else if ( Random.value < difficulty_wandering || player.isPrisoner() )
				//	action = ACTION_WANDERING;
			     else
				    action = ACTION_WANDERING; // ACTION_WAITING

			     actionStartTime = Time.fixedTime;
			     actionTime      = Random.Range( minActionTime, maxActionTime );

			     break;

		    case ACTION_WAITING :

			     // Toggle on and off isKinematic to reset the player's momentum
				 Rigidbody rb       = transform.GetComponent<Rigidbody>();
				 rb.isKinematic     = true;
			     rb.velocity        = new Vector3 ( 0.0f, 0.0f, 0.0f );
			     rb.angularVelocity = new Vector3 ( 0.0f, 0.0f, 0.0f );
			     rb.isKinematic     = false;

			     break;

		    case ACTION_WANDERING :

				 // Do a random mouvement if wandering
		      	 // wandering();
			     smoothWandering();

			     break;

			case ACTION_FINDBALL :

			     targetBall = findBall();

				 if ( targetBall != null ) {

					 action          = ACTION_GETBALL;
				     actionStartTime = Time.fixedTime;
				     actionTime      = Random.Range( minActionTime, maxActionTime );

				 } else
				     action = ACTION_GETBALL; // Wandering until a ball is found

				 break;

			case ACTION_GETBALL :
							
			     if ( player.ownBall() || !getBall( targetBall ) ) {

					action     = ACTION_NONE;
					targetBall = null;

			     }

			     break;

			case ACTION_PASS :
			
			     break;

			case ACTION_FIRE :
			
			     break;

		}

		
		// Wait for the action timer
		if ( ( ( Time.fixedTime - lastProcessTimeout ) < processTimeout ) )
			return;
		
		lastProcessTimeout = Time.fixedTime;


		// Change the current speed of the player
		if ( Random.value < diff_speedChange )
			currentSpeed = Random.value * ( maxSpeed - minSpeed ) + minSpeed;


		// Stop here if the player has no ball
		if ( !player.ownBall() )
			return;

		bool chooseAction = false;

		while ( !chooseAction ) {

			// The random can be 4, does nothing in this case
			switch ( Mathf.FloorToInt( Random.Range( 0.0f, 3.99f ) ) ) {

				case 0 : // To add difficulties throw to the main player

						 if ( player.getTeamNo() == 1 && Random.value < throwBallToMain ) {
			
						     Player ennemy = game.getMainPlayer();
			
							 if ( ennemy != null && !ennemy.isPrisoner() ) {
				
								 headTo( ennemy.getPosition() );
					
								 throwBall( Random.Range ( minThrowSpeed, maxThrowSpeed ) );
					
								 chooseAction = true;
				
							 }
			
						 }

						 break;

				case 1 : // Throw the ball on an enemmy (nearest or random)

						 if ( Random.value < throwBallToEnnemy ) {

				         	Player ennemy;

							if ( Random.value > throwToRandom )
								ennemy = findRandomEnnemy();
							else 
								ennemy = findNearestEnnemy();

						 	if ( ennemy != null ) {
			
								headTo( ennemy.getPosition() );

								throwBall( Random.Range ( minThrowSpeed, maxThrowSpeed ) );
				
								chooseAction = true;

							}

						 }

						 break;

				case 2 : // Make a pass to a teammate in prison

	 					 if ( Random.value < passToTeammate ) {
			
						 	Player teammate;
			
							teammate = findRandomTeammatePrisoner();
			
							if ( teammate != null ) {
				
								headTo( teammate.getPosition() );
				
								throwBall( Random.Range ( maxThrowSpeed, maxThrowSpeed ) );
				
								chooseAction = true;
				
							}
			
						 }

						 break;							
	
			} // End switch

		} // While not chooseAction

	}


	public void reset() {

		currentSpeed       = 0.0f;
		lastProcessTimeout = 0.0f;
		actionTime         = 0.0f;
		actionStartTime    = 0.0f;
		action             = ACTION_NONE;
		targetBall         = null;
		
	}



	public Player getPlayer() {

		PlayerObject playerObj = transform.GetComponent<PlayerObject>() as PlayerObject;

		if ( playerObj == null )
			return null;

		return playerObj.playerObj;
		
	}



	public Game getGame() {
		
		PlayerObject playerObj = transform.GetComponent<PlayerObject>() as PlayerObject;
		
		if ( playerObj == null )
			return null;
		
		return playerObj.game;
		
	}



	public void headTo( Vector3 position ) {

		Vector3 relativePos = position - player.getObject().transform.position;

		// Return if the heading target is not valid
		if ( relativePos == new Vector3 ( 0.0f, 0.0f, 0.0f ) )
			return;

		Quaternion rotation = Quaternion.LookRotation( relativePos );
		transform.rotation  = rotation;

	}



	public void goTo( Vector3 position, float speed ) {

		headTo( position );
				
		Vector3 velocity = ( position - player.getObject().transform.position ).normalized * speed;
			
		player.setVelocity ( velocity );

	}



	public void headToNearestBall() {

		// TODO:

	}


	public Vector3 getHeading() {
	
		// TODO:
        return new Vector3( 0.0f, 0.0f) ;

	}



	public void throwBall( float speed ) {

		if ( player.ownBall() == true ) {
			
			Ball ball = player.getBall();
			
			// Put the ball in from of the ship by 1.0f
			Vector3 dir = transform.forward;// * 10.0f;
			Vector3 pos = transform.position + dir;
			
			ball.setPosition( pos );
			
			ball.setVelocity ( new Vector3 ( transform.forward.x * speed,
			                                 transform.forward.y * speed,
			                                 transform.forward.z * speed ) );
			ball.hideGrabbed();
			ball.setRebound( false );
			ball.show();
			
			// The player is the thrower of the ball
			ball.setThrower( player );
			
			// Done in BallGrabberScript instead because of the Grabber Collider
			player.giveAwayBall();			
			
		}

	}



	public Ball getNearestBall() {

		Ball   nearestBall     = null;
		Ball[] balls           = game.getBalls();
		float  nearestDistance = Mathf.Infinity;
		float  currentDistance;

		foreach ( Ball ball in balls ) {

			if ( ball == null )
				continue;

			if ( !ball.isGrabbed() && game.getPositionFieldSide( ball.getPosition() ) == player.getTeamNo() 
			  && !game.posInPrison( 0,ball.getPosition() ) && !game.posInPrison( 1, ball.getPosition() ) ) {

				currentDistance = game.getDistance( ball.getPosition(), player.getPosition () );

				if ( currentDistance < nearestDistance ) {

					nearestBall     = ball;
					nearestDistance = currentDistance;

				}

			}

		}

		return nearestBall;
		
	}



	public Ball getRandomBall() {

		Ball[] balls           = game.getBalls();
		Ball[] nearBalls       = new Ball[balls.Length];
		int    nearBallsnum    = -1;
		
		foreach ( Ball ball in balls ) {
			
			if ( ball == null )
				continue;
			
			if ( !ball.isGrabbed() && game.getPositionFieldSide( ball.getPosition() ) == player.getTeamNo() 
		      && !game.posInPrison( 0,ball.getPosition() ) && !game.posInPrison( 1, ball.getPosition() ) ) {
				
				nearBallsnum++;

				nearBalls[nearBallsnum] = ball;

			}
			
		}

		if ( nearBallsnum == -1 )
			return null;

		return nearBalls[Random.Range( 0, nearBallsnum )];
		
	}



	public Ball getNearestPrisonBall( int prisonNo ) {
		
		Ball   nearestBall     = null;
		Ball[] balls           = game.getBalls();
		float  nearestDistance = Mathf.Infinity;
		float  currentDistance;
		
		foreach ( Ball ball in balls ) {
			
			if ( ball == null )
				continue;
			
			if ( !ball.isGrabbed() && game.posInPrison( prisonNo, ball.getPosition() ) ) {
				
				currentDistance = game.getDistance( ball.getPosition(), player.getPosition () );
				
				if ( currentDistance < nearestDistance ) {
					
					nearestBall     = ball;
					nearestDistance = currentDistance;
					
				}
				
			}
			
		}
		
		return nearestBall;
		
	}



	public Player findNearestEnnemy() {

		Player   nearestEnnemy   = null;
		Player[] ennemies;
		float    nearestDistance = Mathf.Infinity;
		float    currentDistance;

		if ( player.getTeamNo() == 0 ) 
			ennemies = game.getTeam(1).getPlayers();
		else
			ennemies = game.getTeam(0).getPlayers();

		foreach ( Player ennemy in ennemies ) {
			
			if ( ennemy == null )
				continue;

			currentDistance = game.getDistance( ennemy.getPosition(), player.getPosition () );
				
			if ( !ennemy.isPrisoner() && currentDistance < nearestDistance ) {
					
				nearestEnnemy   = ennemy;
				nearestDistance = currentDistance;
												
			}
			
		}
		
		return nearestEnnemy;

	}



	public Player findRandomEnnemy() {

		bool     foundEnnemy = false;
		Player   ennemy      = null;
		Player[] ennemies;

		// TODO: TEMP to fix the infinite loop bug
		int      loopcount = 0;
		
		if ( player.getTeamNo() == 0 ) 
			ennemies = game.getTeam(1).getPlayers();
		else
			ennemies = game.getTeam(0).getPlayers();

		// Just in case check if all ennemies are not in prison
		foreach ( Player checkennemy in ennemies ) {

			if ( checkennemy == null )
				continue;

			if ( !checkennemy.isPrisoner() ) {

				foundEnnemy = true;
				break;

			}
			
		}

		if ( foundEnnemy ) {

			ennemy = ennemies[Random.Range( 0, ennemies.Length - 1 )];

			while ( ennemy.isPrisoner() ) {

				if ( !ennemy.isPrisoner() )
					break;

				ennemy = ennemies[Random.Range( 0, ennemies.Length - 1 )];

				// TODO: TEMP to fix the infinite loop bug
				loopcount++;
				if ( loopcount > 20 ) break;

			}

		}
		
		return ennemy;
		
	}


	// TODO:
	public Player findNearestTeamMate() {

        return null;
		
	}	



	public Player findTeamMateWithBall() {

		bool     foundOwner = false;
		Player   teammate   = null;
		Player[] teammates;

		// TODO: TEMP to fix the infinite loop bug
		int      loopcount = 0;
		
		if ( player.getTeamNo() == 0 ) 
			teammates = game.getTeam(0).getPlayers();
		else
			teammates = game.getTeam(1).getPlayers();

		// Just in case check if there's a teammate owning a ball
		foreach ( Player mate in teammates ) {
			
			if ( mate == null )
				continue;
			
			if ( mate.ownBall() && !mate.isPrisoner() ) {
				
				foundOwner = true;
				break;
				
			}
			
		}
		
		if ( foundOwner ) {
			
			teammate = teammates[Random.Range( 0, teammates.Length - 1 )];
			
			while ( !teammate.ownBall() || teammate.isPrisoner() ) {
				
				if ( teammate.ownBall()&& !teammate.isPrisoner() )
					break;							
				
				teammate = teammates[Random.Range( 0, teammates.Length - 1 )];
				
				// TODO: TEMP to fix the infinite loop bug
				loopcount++;
				if ( loopcount > 20 ) break;
				
			}
			
		}
		
		return teammate;
		
	}	



	public Player findRandomTeammatePrisoner() {

		bool     foundPrisoner = false;
		Player   teammate      = null;
		Player[] teammates;

		// TODO: TEMP to fix the infinite loop bug
		int      loopcount = 0;
		
		if ( player.getTeamNo() == 0 ) 
			teammates = game.getTeam(0).getPlayers();
		else
			teammates = game.getTeam(1).getPlayers();
		
		// Just in case check if there's a teammate in prison
		foreach ( Player checkprisoner in teammates ) {
			
			if ( checkprisoner == null )
				continue;
			
			if ( checkprisoner.isPrisoner() ) {
				
				foundPrisoner = true;
				break;
				
			}
			
		}
		
		if ( foundPrisoner ) {
			
			teammate = teammates[Random.Range( 0, teammates.Length - 1 )];
			
			while ( !teammate.isPrisoner() ) {
				
				if ( teammate.isPrisoner() )
					break;							
				
				teammate = teammates[Random.Range( 0, teammates.Length - 1 )];

				// TODO: TEMP to fix the infinite loop bug
				loopcount++;
				if ( loopcount > 20 ) break;

			}
			
		}
		
		return teammate;

	}



	public Player detectTarget( Vector3 direction ) {

		RaycastHit hit;

		// Target are only the spaceships
		int layerMask = 1 << 12; // Layer détecté, spaceships = 12
		
		// Throw raycast at direction            
		Physics.Raycast( transform.position, direction, out hit, 1000, layerMask );           
		
		if ( hit.collider != null ) {
			
			PlayerObject playerinfos = hit.collider.GetComponent<PlayerObject>();

			if ( playerinfos == null )
				return null; // Invalid target

			return playerinfos.playerObj; // Return the player object
			
		}
			
		return null; // No target detected

	}



	// ACTION_WANDERING
	void wandering() {

		if ( action != ACTION_WANDERING )
			return;
		
		Vector3 direction = this.GetComponent<Rigidbody>().velocity;
		Player  player    = this.GetComponent<PlayerObject>().playerObj;
		Game    game      = player.getGame();
		
		// The player is in prison?
		if ( !player.isPrisoner() ) {
			
			if ( Random.value > 0.99f ) {
				
				direction = new Vector3 ( Random.Range( -speedX, speedX ), direction.y, direction.z );
				
			}
			
			if ( Random.value > 0.99f ) {
				
				direction = new Vector3 ( direction.x, Random.Range( -speedY, speedY ), direction.z );
				
			}
			
			if ( Random.value > 0.99f ) {
				
				direction = new Vector3 ( direction.x, direction.y, Random.Range( -speedZ, speedZ ) );
				
			}
			
			direction.Normalize();
			direction *= Random.Range( minForwardSpeed, maxForwardSpeed );
			
			if ( Random.value > 0.005f ) {
				
				this.GetComponent<Rigidbody>().velocity = Vector3.Lerp( this.GetComponent<Rigidbody>().velocity, direction, Time.deltaTime * 142.0f );
				
			} else {
				
				this.GetComponent<Rigidbody>().velocity = new Vector3 ( 0.0f, 0.0f, 0.0f ); 
			}
			
			this.GetComponent<Rigidbody>().angularVelocity = new Vector3 ( 0.0f, 0.0f, 0.0f );
			
			if ( this.GetComponent<Rigidbody>().velocity != new Vector3 ( 0.0f, 0.0f, 0.0f ) )
				transform.rotation = Quaternion.LookRotation ( this.GetComponent<Rigidbody>().velocity.normalized );
			
		} else { // This is a prisonier

			Game.Rect3D field = game.getFieldSize();
			
			Vector3 pos = new Vector3 ( player.getPosition().x, field.centerX(), field.centerZ() );
			
			if ( Random.value > 0.99 ) {
				
				newMove = true;
				
			}
			
			if ( newMove ) {
				
				// Look at the center of the field
				player.headTo( pos );

				direction = new Vector3 ( Random.Range( -speedX, speedX ), 0.0f, 0.0f );

				if ( player.getTeamNo() == 1 )
					direction = new Vector3 ( direction.x, direction.y, Random.Range( prisonBackSpeed, speedZ ) );
				else
					direction = new Vector3 ( direction.x, direction.y, Random.Range( -speedZ , prisonBackSpeed ) );				
				
				direction = new Vector3 ( direction.x, Random.Range( -speedY, speedY ), direction.z );
				
				direction.Normalize();
				direction *= Random.Range( minForwardSpeed * prisonSlowDown, maxForwardSpeed * prisonSlowDown );
				
				this.GetComponent<Rigidbody>().velocity = direction ;

				newMove = false;
				
			}					
			
		}
		
	}



	// ACTION_WANDERING (smooth)
	void smoothWandering() {

		if ( action != ACTION_WANDERING || smoothmove == null )
			return;
		
		Vector3 direction = this.GetComponent<Rigidbody>().velocity;
		Player  player    = this.GetComponent<PlayerObject>().playerObj;
		Game    game      = player.getGame();
		
		// The player is in prison?
		if ( !player.isPrisoner() && smoothmove.isDone() ) {
			
			Vector3       newpos       = new Vector3 ( player.getPosition().x, player.getPosition().y, player.getPosition().z );
			Game.Rect3D   rect         = game.getFieldSize();		
			float         speed        = Random.Range( minSmoothSpeed, maxSmoothSpeed );
			int           num          = Mathf.CeilToInt( Random.Range( 0.0001f, 10.0f ) );
			List<Vector3> newpositions = new List<Vector3>();

			for ( int i = 0; i < num; ++i ) {

				newpos.x = newpos.x + Random.Range( -speed * speedX , speed * speedX );
				newpos.y = newpos.y + Random.Range( -speed * speedY , speed * speedY );
				newpos.z = newpos.z + Random.Range( -speed * speedZ , speed * speedZ );		

				newpos.x = Mathf.Max( newpos.x, rect.xMin() + 4.0f );
				newpos.x = Mathf.Min( newpos.x, rect.xMax() - 4.0f );

				newpos.y = Mathf.Max( newpos.y, rect.yMin() + 4.0f );
				newpos.y = Mathf.Min( newpos.y, rect.yMax() - 4.0f );

				if ( player.getTeamNo() == 0 ) {

					newpos.z = Mathf.Max( newpos.z, rect.zMin() + 7.0f );
					newpos.z = Mathf.Min( newpos.z, -2.0f );

				} else {

					newpos.z = Mathf.Max( newpos.z, 2.0f );
					newpos.z = Mathf.Min( newpos.z, rect.zMax() - 7.0f );

				}

				newpositions.Add( newpos );

			}

			if ( num > 1 )
				smoothmove.moveTo( player, newpositions, Random.Range( wanderingMinSpeed, wanderingMaxSpeed ) );
			else
				smoothmove.moveTo( player, newpos, Random.Range( wanderingMinSpeed, wanderingMaxSpeed ), new Vector3( Random.Range( 0.0f, 1.0f ), Random.Range( 0.0f, 1.0f ), Random.Range( 0.0f, 1.0f ) ) );

		} else if ( player.isPrisoner() ) { // This is a prisonier
			
			Game.Rect3D field = game.getFieldSize();
			
			Vector3 pos = new Vector3 ( player.getPosition().x, field.centerX(), field.centerZ() );
			
			if ( Random.value > 0.99 ) {
				
				newMove = true;
				
			}

			// Check for teammates owning balls
			Player teammateWithBall = findTeamMateWithBall();

			if ( teammateWithBall != null && Random.value > 0.7 ) {

				// Look at the the teammate with ball
				player.headTo( teammateWithBall.getPosition() );

			}
			
			if ( newMove ) {

				direction = new Vector3 ( Random.Range( -speedX, speedX ), 0.0f, 0.0f );

				if ( player.getTeamNo() == 1 )
					direction = new Vector3 ( direction.x, direction.y, Random.Range( prisonBackSpeed, speedZ ) );
				else
					direction = new Vector3 ( direction.x, direction.y, Random.Range( -speedZ, prisonBackSpeed ) );
							
				direction = new Vector3 ( direction.x, Random.Range( -speedY, speedY ), direction.z );

				direction.Normalize();
				direction *= Random.Range( minForwardSpeed * prisonSlowDown, maxForwardSpeed * prisonSlowDown );
				
				this.GetComponent<Rigidbody>().velocity = direction ;

				newMove = false;
				
			}					
			
		}
		
	}




	// ACTION_GETBALL
	bool getBall( Ball target ) {

		// The player is targeting a ball?
		if ( target != null && action == ACTION_GETBALL ) {
			
			Vector3 ballpos = target.getPosition();
			int     team    = player.getTeamNo();
			
			// The ball has been grabbed stop trying to reach it
			if ( target.isGrabbed() )
				return false;
			
			// If the ball is still in the player's field and not in a prison cell if the player is not a prisoner
			// or if it's in his cell if it's a prisoner
			if ( ( !player.isPrisoner() && game.getPositionFieldSide( ballpos ) == team && !game.posInPrison( 0, ballpos ) && !game.posInPrison( 1, ballpos ) ) 
			    || (  player.isPrisoner() && game.posInPrison( team, ballpos ) ) ) {
				
				// This is used to prevent the ship to follow too well the ball
				if ( Random.value < 0.5 )
					goTo( ballpos, Random.value * currentSpeed );

				return true; // target found
				
			}
			
		}

		// The ball is outside of the player's field or prison cell												
		return false;

	}



	// ACTION_FINDBALL
	Ball findBall() {

		Ball foundball = null;

		// Find a ball if the player has no ball
		if ( !player.ownBall() ) {
			
			if ( !player.isPrisoner() && action == ACTION_FINDBALL ) {
				
				if ( Random.value < 7 )
					foundball = getNearestBall(); // Goto the nearest ball
				else
					foundball = getRandomBall();  // Goto a random ball
				
			} else
				foundball = getNearestPrisonBall( player.getTeamNo() );
			
		} // End find a ball

		return foundball;

	}

}
