﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class Laser : MonoBehaviour 
{
	public Material[] laserGunTextures;

	// element GUI du chargeur a laser
	public float barDisplay; // Laser charge
	private Vector2 pos = new Vector2(17,45);
	private Vector2 size = new Vector2(120,15);
	public Texture2D emptyTex;
	public Texture2D fullTex;
	private GUIStyle frontStyle = null;
	private GUIStyle backStyle = null;

	private bool oldView = false;
	private bool oldNeonMode = false;

	private GameObject camera;

	private static GameController gameController;
	private LineRenderer line;
	private int layerMask;

	private Light startLight;
	private Light endLight;
	private GameObject endLightGameObject;

	private bool isFiringLaser = false;
	private bool isChargingLaser = false;
	private bool isWaitingForCharge = false;

	// combien de milli-seconde on peux tirer le laser
	private static float maxTime = 5;
	// nombre de millisecondes restant
	private float timeLeft = maxTime;

	private int deltaTime;

	void Start () 
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");

		if ( gameControllerObject != null ) {
			
			gameController = gameControllerObject.GetComponent <GameController>();
			
		}
		
		if ( gameController == null ) {
			
			Debug.Log ("Cannot find 'GameController' script");
			
		}

		startLight = gameObject.GetComponent<Light>();
		startLight.enabled = false;


		endLightGameObject = new GameObject("The Light");
		endLight = endLightGameObject.AddComponent<Light>();
		endLight.color = Color.yellow;
		endLight.intensity = 8;
		endLight.range = 1;

		endLight.enabled = false;


		line = gameObject.GetComponent<LineRenderer>();
		line.enabled = false;
		int playerLayer = LayerMask.NameToLayer ("OtherSpaceShips");
		int blocLayer = LayerMask.NameToLayer ("Blocs");
		int wallsLayer = LayerMask.NameToLayer ("Walls");
		layerMask = 1 << playerLayer | 1 <<  blocLayer | 1 <<  wallsLayer;

		//new GUIStyle( GUI.skin.box );
		camera = GameObject.FindGameObjectsWithTag("MainCamera")[0];

	}
	void Update () 
	{
		PlayerObject player = this.transform.parent.gameObject.GetComponent<PlayerObject>() as PlayerObject;


		if (Input.GetButton ("Fire2") && timeLeft > 0 && !isWaitingForCharge && player.playerObj.hasLaser ()) {

			player.playerObj.setLaserGunTexture( laserGunTextures[1] ); // Firing gun material

			if ( isFiringLaser == false ) {
				oldView = gameController.game.getFirstPersonView();

				// Save Neon Mode
				oldNeonMode = camera.GetComponent<Bloom>().enabled;
			}

			gameController.game.setFirstPersonView(true);

			// Neon Mode on
			camera.GetComponent<Bloom>().enabled = true;

			// Play the laser sound
			if ( !gameController.game.isPlaying( AudioController.AUDIO_LASER_SHOT ) )
				gameController.game.playSound( AudioController.AUDIO_LASER_SHOT, 1.0f, true );

			//Debug.Log ("firing"+timeLeft);
			timeLeft = timeLeft - Time.deltaTime;

			line.enabled = true;
			//startLight.enabled = true;
			isFiringLaser = true;
			isChargingLaser = false;

			// tourne la texture
			line.material.mainTextureOffset = new Vector2(0, Time.time); 

			Ray ray = new Ray (transform.position - ( 1.01f * transform.forward.normalized ), transform.forward);
			RaycastHit hit;
			
			line.SetPosition (0, new Vector3 ( ray.origin.x, ray.origin.y - 0.04f, ray.origin.z ) );

			bool rayCastResult;

			if ( gameController.getConfigs().getRayCastRadius() == 0.0f )
				rayCastResult = Physics.Raycast (ray, out hit, 100, layerMask);
			else
				rayCastResult = Physics.SphereCast (ray, gameController.getConfigs().getRayCastRadius(), out hit, 100, layerMask);

			if (rayCastResult) {
				endLight.enabled = true;
				endLightGameObject.transform.position = hit.point;

				line.SetPosition (1, hit.point);
				if (hit.rigidbody) {

					if (hit.transform.tag == "Bloc") {
						//Debug.Log (hit.transform.name);
						hit.rigidbody.AddForceAtPosition(transform.forward * 1000000, hit.point);
					}
					else if (hit.transform.tag == "Ennemi") {
						//Debug.Log (hit.transform.name);

						PlayerObject enemy = hit.transform.gameObject.GetComponent<PlayerObject>() as PlayerObject;

						// Explosion sound
						enemy.playerObj.playSound( AudioController.AUDIO_TARGET_EXPLODE, 1.0f );

						enemy.playerObj.explodeAndSendToJail();
						//PlayerObject player = this.transform.parent.gameObject.GetComponent<PlayerObject>() as PlayerObject;
						if ( player.playerObj.isPrisoner() ) {
							player.playerObj.reinitPlayer();

							// Change the segment of the music to a smoother one in Random segment mode if enabled				
							if ( player.playerObj.isMainPlayer() && gameController.getRandomSegmentsMode() )
								gameController.audio().playSegmentsSoundtrack( 0 );

						}
						else {
							gameController.AjouterPointage( 1, "rouge" );
						}


					}

				}
			} else
				line.SetPosition (1, ray.GetPoint (100));

		} else {

			//Debug.Log ("recharging"+timeLeft);
			if ( isFiringLaser == true ) {
				gameController.game.setFirstPersonView(oldView);

				// Restore Old Neon Mode 
				camera.GetComponent<Bloom>().enabled = oldNeonMode;

				// Stop the laser sound				
				if ( gameController.game.isPlaying( AudioController.AUDIO_LASER_SHOT ) ) 
					gameController.game.stopPlaying( AudioController.AUDIO_LASER_SHOT );

			}

			player.playerObj.setLaserGunTexture( laserGunTextures[0] ); // Non firing gun material

			isFiringLaser = false;
			
			isChargingLaser = true;

			if ( timeLeft < maxTime ) {
				timeLeft = timeLeft + Time.deltaTime;
			}
			if ( timeLeft < 2 ) {

				// TODO: PAS SUR SI CE SON EST A LA BONNE PLACE
				// Play get laser empty sound
				if ( !gameController.game.isPlaying( AudioController.AUDIO_LASER_LOW ) )
					gameController.game.playSound( AudioController.AUDIO_LASER_LOW );

				isWaitingForCharge = true;
			}
			else {
				isWaitingForCharge = false;
			}

			line.enabled = false;
			startLight.enabled = false;
			endLight.enabled = false;

		}
		barDisplay = getPercentCharged();
	}

	void OnGUI() {
		PlayerObject player = this.transform.parent.gameObject.GetComponent<PlayerObject>() as PlayerObject;
		//player.playerObj.setLaser (true);
		if (player.playerObj.hasLaser ()) {
		
			//player.playerObj.showLaserGun( true );
			gameController.level().setMode( 1 ); // Level in laser mode
			gameController.level().setLevel ((int)(barDisplay * 16)); // level = 0 à 16

			// TODO: PAS SUR SI CE SON EST A LA BONNE PLACE OU QUE LES VALEURS SONT BONNES
			// Play get laser low sound
			if ( (int) (barDisplay * 16) <= 5 ) {

				if ( !gameController.game.isPlaying( AudioController.AUDIO_LASER_LOW ) )
					gameController.game.playSound( AudioController.AUDIO_LASER_LOW );

			}
	
		} else {

			//player.playerObj.showLaserGun( false );
			gameController.level().setMode( 0 ); // Level in normal mode (ball)

		}

	}
		
	public bool isFiring (){
		return isFiringLaser;
	}

	public bool isCharging () {
		return isChargingLaser;
	}
	public float getPercentCharged () {
		return timeLeft / maxTime;
	}
}