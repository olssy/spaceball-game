﻿using UnityEngine;
using System.Collections;

public class PlayerObject : MonoBehaviour {

	public Player playerObj;   // Reference to the Player object of the player
	public int    team;        // The team number of the player
	public int    playerIndex; // The index number of the player
	public Game   game;

	void FixedUpdate() {

		if ( playerObj != null ) {

			if ( playerObj.getSpeed() > 0.01f ) {

				if ( !playerObj.isMainPlayer() ) {

				    //	playerObj.setSoundVolume( AudioController.AUDIO_SHIP_NOISE, 1.0f );
			        // else
					// Sound of the flying ship
					if ( playerObj != null && !playerObj.isPlaying( AudioController.AUDIO_SHIP_NOISE ) )
						playerObj.playSound( AudioController.AUDIO_SHIP_NOISE, 0.0f, true );

					playerObj.setSoundVolume( AudioController.AUDIO_SHIP_NOISE, playerObj.getSpeed() / 10.0f );
				    //playerObj.setSoundPitch( AudioController.AUDIO_SHIP_NOISE, playerObj.getSpeed() / 30.0f );

				}

			} else {

				if ( !playerObj.isMainPlayer() )
				//	playerObj.setSoundVolume( AudioController.AUDIO_SHIP_NOISE, 0.0f );
				//else
					playerObj.stopPlaying( AudioController.AUDIO_SHIP_NOISE );

			}

		}

		// Check to see if the player is outside of the prison's limit in case of collisions
		if ( playerObj != null && playerObj.isPrisoner() ) {

			Rigidbody rb = transform.GetComponent<Rigidbody>();
			Vector3   newPos;

			newPos = game.fitByInPrison( team, transform.position, 0.01f );

			if ( transform.position != newPos ) {

				// Reset momentum
				//rb.isKinematic = true;

				transform.position = newPos;
				
				//rb.isKinematic = false;

			}


		}

	}

}
