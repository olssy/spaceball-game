﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Move {//: MonoBehaviour {

	public  float         interpolationScale = 0.25f; // 0.25f
	public  float         animTick           = 0.02f;
	private Vector3       velocity;
	private Vector3       rotation;
	private Vector3       angularVelocity;
	private Vector3       startPosition;
	private Vector3       lastPosition; // TODO: may be not needed
	private int           lastFrame;
	private float         lastTick;

	private Game          game;
	private Player        player;
	private GameObject    gameobject;

	private float         duration; // Animation duration in seconds
	private float         starttime      = Mathf.Infinity;
	private float         frameStartTime = Mathf.Infinity;
	private float         frameDuration  = Mathf.Infinity;
	private bool          started        = false;
	private bool          loop           = false;
	private bool          heading        = true;
	private bool          done           = false;
	private bool          linearAnim     = true;
	private BezierPath    path;
	private List<Vector3> points;
	private List<Vector3> curve;

	// Used for debugging
	private LineRenderer  lineRenderer;



	// Constructor
	public Move( GameObject movingObject, float moveDuration, int precision ) {

		PlayerObject playerObject;

		if ( movingObject == null )
			playerObject = movingObject.GetComponent<PlayerObject>();
		else
			playerObject = null;

		// Is the object a player?
		if ( playerObject != null ) {

			this.player = playerObject.playerObj;
			this.game   = player.getGame();

		} else { // Do not contain a PlayerObject

			this.player = null;
			this.game   = null;

		}

		this.gameobject    = movingObject;
		this.startPosition = this.gameobject.transform.position;
		this.lastFrame     = 0;				
		this.duration      = moveDuration;
		this.points        = new List<Vector3>();	
		this.path          = new BezierPath();
		lineRenderer       = movingObject.GetComponent<LineRenderer>();

		// Default is 10 but we want more precision for a smoother path
		this.path.SEGMENTS_PER_CURVE = precision;

		// The first point of the path is the player's actual position
		this.points.Add( movingObject.transform.position );

	}



	// Do the actual animation
	public void animate() {

		float currentTime = Time.fixedTime - this.starttime;

		if ( this.started ) {

			if ( Time.fixedTime - this.lastTick < animTick )
				return;

			lastTick = Time.fixedTime;

			if ( currentTime <= this.duration ) {
							
				float animdone = ( (float) ( curve.Count - 1 ) ) * ( currentTime / this.duration );
				int   frame    = Mathf.FloorToInt ( animdone );

				// Toggle on and off isKinematic to reset the player's momentum
				Rigidbody rb   = this.gameobject.transform.GetComponent<Rigidbody>();
				//rb.isKinematic = true;
				rb.isKinematic = false;

				// Interpolation between 2 frames
				if ( frame < curve.Count - 1 ) {

					Vector3 newpos = Vector3.Lerp( curve[frame], curve[frame + 1], animdone - (float) frame );

					if ( !float.IsNaN( newpos.x ) && !float.IsNaN( newpos.y ) && !float.IsNaN( newpos.z ) ) {
					
						this.gameobject.transform.position = newpos;
						//rb.velocity = ( curve[frame + 1] - newpos ).normalized * 100.0f;

						if ( this.heading ) {

// NOT USEFUL, DO NOT CHANGE ANYTHING
//							// Smooth heading to the next tick position
//							float   nextanimdone = ( (float) ( curve.Count - 1 ) ) * ( ( currentTime + animTick ) / this.duration );
//							Vector3 nextpos      = Vector3.Lerp( curve[frame], curve[frame + 1], nextanimdone - (float) frame );
//
//							headTo( nextpos );

							headTo( curve[frame + 1] );

						}
						//rb.velocity = -this.gameobject.transform.TransformDirection( newpos - this.gameobject.transform.position );
						//rb.velocity.Normalize();
						//rb.velocity  = rb.velocity / Time.smoothDeltaTime;
						//rb.velocity * 10.0f;
						//rb.velocity = this.gameobject.transform.TransformDirection( this.gameobject.transform.position - newpos );
					}

				} else
					this.gameobject.transform.position = curve[curve.Count - 1];

				//rb.isKinematic = false;

			} else { // Loop

				if ( this.loop ) {

					this.gameobject.transform.position = this.startPosition;
					this.starttime = Time.fixedTime;

				} else {


					this.stop();

			    }

			}

		}

	}



	public void addPathPoint( Vector3 point ) {

		this.points.Add( point );

	}



	public void start() {

		this.starttime = Time.fixedTime;
		this.lastTick  = Time.fixedTime - this.animTick;
		this.started   = true;
		this.done      = false;

		// Interpolate the path
		interpolate();

		this.curve         = getCurvePoints();
		this.frameDuration = this.duration / (float) curve.Count;

	}


	public void interpolate() {
		
		path.Interpolate( this.points, this.interpolationScale );
		
		//List<Vector3> curvePoints = this.path.GetDrawingPoints2();
		
		//return curvePoints;
		
	}


	public List<Vector3> getCurvePoints() {

		//path.Interpolate( this.points, this.interpolationScale );
		
		List<Vector3> curvePoints = this.path.GetDrawingPoints1();

		return curvePoints;

	}



	public List<Vector3> getControlPoints() {

		//path.Interpolate( this.points, this.interpolationScale );

		return path.GetControlPoints();

	}



//	public List<float> getSegmentsLengths( float precision ) {
//
//		List<Vector3> thecurve        = getCurvePoints();
//		List<float>   segmentsLengths = new List<float>();
//		float         length          = 0.0f;
//		float         position        = 0.0f;
//		float         pointpos        = 0.0f;
//		int           point           = 0;
//
//		// DEBUG
//		float         total = 0.0f;
//		int           segmentnum = 0;
//
//		for ( float pos = 0.0f; pos <= 1.0f; pos += precision ) {
//
//			position = ( (float) ( thecurve.Count - 1 ) ) * pos;
//
//			// DEBUG
//			//Debug.Log ( "Calculating point : " + point );
//			
//			if ( Mathf.FloorToInt ( position ) != point ) {
//
//				// DEBUG
//				//Debug.Log ( "Segment " + point + " = " + ( pos - pointpos ) );
//
//				total += ( pos - pointpos );
//				segmentnum++;
//
//				segmentsLengths.Add( pos - pointpos );
//
//				pointpos = pos;
//				point    = Mathf.FloorToInt ( position );
//
//			}
//
//			length += precision;
//
//		}
//
//		// Last segment
//		segmentsLengths.Add( 1.0f - pointpos );
//		total += ( 1.0f - pointpos );
//		segmentnum++;
//
//		// DEBUG
//		Debug.Log ( "Total " + segmentnum + " segments, size = " + total );  
//		
//		return segmentsLengths;
//		
//	}


	// THIS FONCTION IS NOT FINISHED AND NOT WORKING CORRECTLY
	public List<float> getSegmentsLengths( float precision ) {
		
		List<Vector3> thecurve        = getCurvePoints();
		List<float>   segmentsLengths = new List<float>();
		float         length          = 0.0f;
		float         position        = 0.0f;
		float         pointpos        = 0.0f;
		int           point           = 0;
		int           currentpoint    = 0;
		float         distance        = 0.0f;

		float         segmentRatio    = 1.0f / ( thecurve.Count - 1 );

//		Debug.Log ( "RATIO : " + segmentRatio );

		// DEBUG
		float         total      = 0.0f;
		int           segmentnum = 0;
		
		for ( float pos = 0.0f; pos <= 1.0f; pos += precision ) {
			
			position = ( (float) ( thecurve.Count - 1 ) ) * pos;
			
			// DEBUG
			//Debug.Log ( "Calculating point : " + point );
			currentpoint = Mathf.FloorToInt ( position );

			if ( currentpoint == ( thecurve.Count - 1 ) )
			    break;

			distance += ( Vector3.Lerp( thecurve[currentpoint], thecurve[currentpoint + 1], pos % segmentRatio ) - thecurve[currentpoint] ).magnitude;

			if ( currentpoint != point ) {

				segmentsLengths.Add( distance );

				distance = 0.0f;
				//pointpos = pos;
				point    = Mathf.FloorToInt ( position );
				
			}
			
			//length += precision;
			
		}
		
		// Last segment
//		segmentsLengths.Add( 1.0f - pointpos );
//		total += ( 1.0f - pointpos );
//		segmentnum++;
		
		// DEBUG
//		Debug.Log ( "Total " + segmentnum + " segments, size = " + total );  
		
		return segmentsLengths;
		
	}




	public void drawCurve() {

		List<Vector3> curve = getCurvePoints();

		lineRenderer.SetVertexCount( curve.Count );
			
		for ( int i = 0; i < curve.Count; ++i ) {

			lineRenderer.SetPosition( i, curve[i] );

		}
				
	}



	public void drawControlPoints() {
		
		List<Vector3> ctlpoint = getControlPoints();

		for ( int i = 0; i < ctlpoint.Count; ++i ) {
			
			GameObject sphere = GameObject.CreatePrimitive( PrimitiveType.Sphere );
			sphere.transform.localScale = new Vector3( 0.05f, 0.05f, 0.05f );
			sphere.transform.position   = ctlpoint[i];
			
		}
		
	}



	public void drawCurvePoints() {
		
		List<Vector3> ctlpoint = getCurvePoints();
		
		for ( int i = 0; i < ctlpoint.Count; ++i ) {
			
			GameObject sphere = GameObject.CreatePrimitive( PrimitiveType.Sphere );
			sphere.transform.localScale = new Vector3( 0.05f, 0.05f, 0.05f );
			sphere.transform.position   = ctlpoint[i];
			
		}
		
	}
	


	public void stop() {
		
		this.starttime = Mathf.Infinity;
		this.started   = false;
		this.done      = true;

		// Destroy the debug path
		if ( this.lineRenderer ) {

			this.lineRenderer.SetVertexCount( 0 );

		}

	}



	public bool isDone() {

		if ( loop )
			return false;


		return this.done;

	}



	public void setLinear( bool state ) {

		this.linearAnim = state;

	}



	public void setHeading( bool state ) {

		this.heading = state;

	}



	public void setLoop( bool state ) {
		
		this.loop = state;
		
	}



	public void setScale( float scale ) {

		this.interpolationScale = scale;

	}



	public void pitch( float angle ) {


	}



	public void roll( float angle ) {

	}



	public void yaw( float angle ) {

	}



	public void headTo( Vector3 position ) {

		Rigidbody rb          = gameobject.transform.GetComponent<Rigidbody>();
		Vector3   relativePos = position - gameobject.transform.position;
		bool      origKine    = rb.isKinematic;
		
		// Return if the heading target is not valid
		if ( relativePos == new Vector3 ( 0.0f, 0.0f, 0.0f ) )
			return;
		
		// Reset momentum
		//rb.isKinematic     = true;
		
		Quaternion rotation = Quaternion.LookRotation( relativePos );
		gameobject.transform.rotation = rotation;
		
		//rb.isKinematic     = origKine;

	}



    public void go( Vector3 pos, float speed ) {

		Rigidbody rb   = this.gameobject.transform.GetComponent<Rigidbody>();
		Vector3   vect = ( pos - this.gameobject.transform.position );

		//vect.Normalize();

		// TODO: should set the speed here

		//vect.

		// Does the object has a Rigidbody?
		if ( rb != null )
			rb.velocity = vect;
		else
			this.gameobject.transform.position = pos;

	}

	public void rotate( Vector3 value ) {
		
	}

}
