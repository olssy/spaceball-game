using UnityEngine;
using System.Collections;



public class MovePlayer : MonoBehaviour {
	
	private  float speed                 = 8.0f;
    private  float backSpeed             = 2.0f;
    private  float rotationSpeed         = 70.0f;
    private  float maxRotationSpeed      = 70.0f;

	// TODO: JE DEVRAIS PEUT-ETRE LE REMETTRE A 30 COMME AVANT, SEMBLE CAUSER DES PROBLEMES
	//       A 40 ca cause des problemes
	private  float idleRotationSpeed     = 30.0f;// was 30.0f
    private  float lockRotationSpeed     = 15.0f;
    private  float scrollSpeed           = 1000.0f;
    
    private  float throwSpeedMax         = 200.0f;
    private  float throwSpeedStep        = 0.5f;
    private  float throwSpeedStart       = 50.0f; //5.0f
    private  float currentthrowSpeed     = 0.0f;
    
    private  bool  targetLockEngaged     = false;
    private  bool  targetLockLost        = false;
    private  bool  targetLockReqEngage   = false;   
    private  float targetLockEngagedTime; 
    private  float targetLockLostTimer;
	private  float targetZoomSpeed       = 5.0f;
    private  float targetLockTimeout     = 10.0f;
    private  float targetNextLockDelay   = 1.0f;
    private  float targetLockEngageDelay = 1.0f;
    private  float targetLockEngageTimer;
    
    private  Quaternion targetLockEngageOriginal;
    
    private  float textBlinkTimeSec      = 0.3f;
    private  float textBlinkTimer        = 0.0f;        
    private  float textAlphaMax          = 0.7f;
    
    private  float idleTimer;
	private  float idleTimeCountdown     = 0.5f;// OLD 0.5f;
	private  float idleMouseTreshold     = 0.3f; // 0.03
	private  float unlockMouseTreshold   = 0.01f;

	private  bool  oldFirstPersonMode;
	private  bool  targetLockEnabled     = false;
	private  bool  showTargetAlways      = true;
	private  bool  lockOnteamMates       = false; // Do we auto lock on teammates (annoying)
	private  float targetLockZoom        = 23.8f;

	private  float mouseSensibility;

    private  Collider targetLockCollider; 
   
    // TODO: A enlever, utilisÃ© pour le hovering
    private  int   framecount            = 0;
    
	private GameController gameController;

	private Game   game;

	// The Player object of the ship
	public  Player player;

	private Level  level;
	private int    oldlevel = -1;

	private int    defWidth;
	private int    defHeight;

	private GameObject playerTargetImg;
	private GameObject playerTargetTeammateImg;
	private GameObject targetImg;
	private GameObject targetTeammateImg;
	private GameObject msgTargetLockEngage;
	private GameObject msgTargetLockLocked;
	private GameObject msgTargetLockPass;
	private GameObject targetCamera;   

	public void Awake()	{

		defWidth  = Screen.width; 
		defHeight = Screen.height; 

	}
	
	void Start () {
		
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		
		if ( gameControllerObject != null ) {
			
			gameController = gameControllerObject.GetComponent <GameController>();
			
		}
		
		if ( gameController == null ) {
			
			Debug.LogError("Cannot find 'GameController' script");
			
		}

		mouseSensibility  = gameController.game.getConfigs().mouseSensibility;
		idleMouseTreshold = idleMouseTreshold * mouseSensibility;
		rotationSpeed     = rotationSpeed     * mouseSensibility ;
		maxRotationSpeed  = maxRotationSpeed  * mouseSensibility;
		idleRotationSpeed = idleRotationSpeed * mouseSensibility;
		lockRotationSpeed = lockRotationSpeed * mouseSensibility;

		if ( gameController.gameStarted ) {

			Cursor.visible    = false;
        	Screen.lockCursor = true;	

		} else {

			Cursor.visible    = true;
			Screen.lockCursor = false;	

		}

		// Get the Player object of the ship
		player = GetComponent<PlayerObject>().playerObj;
		game   = player.getGame();

		playerTargetImg         = GameObject.FindGameObjectsWithTag( "FirstPersonMire" )[0];
		playerTargetTeammateImg = GameObject.FindGameObjectsWithTag( "FirstPersonMireTeammate" )[0];
		targetImg               = GameObject.FindGameObjectsWithTag( "TargetCross" )[0];
		targetTeammateImg       = GameObject.FindGameObjectsWithTag( "TeammateCross" )[0];
		msgTargetLockEngage     = GameObject.FindGameObjectsWithTag( "MsgTargetLockEngage" )[0];
		msgTargetLockLocked     = GameObject.FindGameObjectsWithTag( "MsgTargetLockLocked" )[0];
		msgTargetLockPass       = GameObject.FindGameObjectsWithTag( "MsgTargetLockPass" )[0];
		targetCamera            = GameObject.FindGameObjectsWithTag( "TargetCamera" )[0];   

		setTargetMode( 3 );

		GameObject levelcavas   = GameObject.FindWithTag ("Level");

		if ( levelcavas != null ) {

			level = levelcavas.GetComponent<Level>();
			
		}
		
		if ( level == null ) {
			
			Debug.LogError("Cannot find 'Level' script");

			return;
			
		}

		level.setLevel(0);

    }


    
	void Update() {

		// Toggle fullscreen
		if ( Input.GetKeyDown(KeyCode.F) && Application.isWebPlayer ) {

			if ( !Screen.fullScreen )			
				Screen.SetResolution( Screen.currentResolution.width, Screen.currentResolution.height, true );			
			else
				Screen.SetResolution( defWidth, defHeight, false );

		}

		// Toggle first person view
		if ( Input.GetKeyDown(KeyCode.C) ) {
			
			if ( player.getGame().getFirstPersonView() )			
				player.getGame().setFirstPersonView( false );			
			else
				player.getGame().setFirstPersonView( true );
			
		}

		// Change the graphic quality
		if ( Input.GetKeyDown(KeyCode.Q) ) {

			int quality = gameController.getConfigs().getCurrentQualityLevel();

			quality++;

			if ( quality == 4 )
				quality = 1;

			gameController.getConfigs().setCurrentQualityLevel( quality );

			switch ( quality ) {

				case 1 : gameController.showMessage( "Graphic quality changed to LOW" );
						 break;

				case 2 : gameController.showMessage( "Graphic quality changed to MEDIUM" );
						 break;

				case 3 : gameController.showMessage( "Graphic quality changed to HIGH" );
						 break;

			}
			
		}

		// Toggle the background music on and off
		if ( Input.GetKeyDown(KeyCode.M) ) {

			if ( player.getGame().audio().getSoundtrack() == -1 ) {

				player.getGame().audio().playRandomSoundtrack();			
				gameController.showMessage( "Background music ON" );

			} else {

				player.getGame().audio().stopSoundtrack();
				gameController.showMessage( "Background music OFF" );

			}
			
		}

		// DEBUG - Toggle Frame Rate
		if ( Input.GetKeyDown(KeyCode.R) ) {
			
			if ( player.getGame().getConfigs().getFPSCounter() )			
				player.getGame().getConfigs().showFPSCounter( false );			
			else
				player.getGame().getConfigs().showFPSCounter( true );
			
		}

		// DEBUG - Toggle Bezier paths  
		if ( Input.GetKeyDown(KeyCode.P) ) {
			
			if ( player.getGame().getBezierPaths() ) {

				player.getGame().setBezierPaths( false );			
				gameController.showMessage( "Ships movements Bezier curves paths OFF" );

			} else {

				player.getGame().setBezierPaths( true );
				gameController.showMessage( "Ships movements Bezier curves paths ON" );

			}
			
		}

		// CHEAT - Zero Gravity Mode 
		if ( Input.GetKeyDown(KeyCode.Z) ) {
			
			if ( player.getGame().getZeroGravityMode() ) {

				player.getGame().setZeroGravityMode( false );			
				gameController.showMessage( "Zero Gravity mode OFF" );

			} else {

				player.getGame().setZeroGravityMode( true );
				gameController.showMessage( "Zero Gravity mode ON" );

			}
			
		}

		// CHEAT - Toggle Invisibility 
		if ( Input.GetKeyDown(KeyCode.I) ) {
			
			if ( player.getGame().getConfigs().getInvinsibility() )	{

				GameObject.FindGameObjectsWithTag( "InvisibleText" )[0].GetComponent<GUIText> ().enabled = false;
				player.getGame().getConfigs().setInvinsibility( false );			
				game.playSound( AudioController.AUDIO_INVINSIBILITY_OFF );
				game.playSound( AudioController.AUDIO_INVINSIBILITY_SOUND, 1.0f, true );

				gameController.showMessage( "Inisibility cheat OFF" );

			} else {

				GameObject.FindGameObjectsWithTag( "InvisibleText" )[0].GetComponent<GUIText> ().enabled = true;
				player.getGame().getConfigs().setInvinsibility( true );
				game.playSound( AudioController.AUDIO_INVINSIBILITY_ON );
				game.stopPlaying( AudioController.AUDIO_INVINSIBILITY_SOUND );

				gameController.showMessage( "Inisibility cheat ON" );

			}
			
		}

	}               
	
	void FixedUpdate() {

		// TODO: TEMP to fix the bug, the sprite is auto-disabled somewhere
		//       The target on ennemy is not showing when not in FirstPerson mode
		playerTargetImg.GetComponent<SpriteRenderer> ().enabled = true;
		
		// Reprend le contrÃ´le du curseur
        Cursor.visible    = false;
        Screen.lockCursor = true;        
                    
		// Pour rendre la souris plus precise autour de zero en gardant la polarite
		float dx = Input.GetAxis( "Mouse X" );// * mouseSensibility;
		//dx       = dx * Mathf.Abs( dx );
		float dy = Input.GetAxis( "Mouse Y" );// * mouseSensibility;
		//dy       = dy * Mathf.Abs( dy );

		if ( game.getConfigs().getArrowsMouseMode() ) {

			dx = dx + (float) Input.GetAxis( "Arrows X" );// * mouseSensibility;
			dy = dy + (float) Input.GetAxis( "Arrows Y" );// * mouseSensibility;

		}

		dx = dx * Mathf.Abs( dx );
		dy = dy * Mathf.Abs( dy );

        // Target Lock
        targetLock( dx, dy );

        // Initialisation de la vitesse de tire
		if ( Input.GetButtonDown ("Jump") || Input.GetButtonDown ("Fire1") ) {

			level.setLevel( 0 );

            currentthrowSpeed = throwSpeedStart;

        }
        
        
        // Calcule de la vitesse du tire
		if ( Input.GetButton ("Jump") || Input.GetButton ("Fire1") ) {

			if ( player.ownBall() && currentthrowSpeed < throwSpeedMax + throwSpeedStep ) {
				
				currentthrowSpeed = currentthrowSpeed + throwSpeedStep;

				int newlevel = Mathf.RoundToInt( ( currentthrowSpeed / throwSpeedMax ) * 17 );

				// Play the level sound
				if ( newlevel != oldlevel ) {

					if ( newlevel > 12 ) {

						if ( !game.isPlaying( AudioController.AUDIO_LEVEL_FULL ) )
							game.playSound( AudioController.AUDIO_LEVEL_FULL );

					} else {

						if ( !game.isPlaying( AudioController.AUDIO_LEVEL_CHANGE ) )
							game.playSound( AudioController.AUDIO_LEVEL_CHANGE );

					}

					oldlevel = newlevel;

				}

				level.setLevel( newlevel );

            }

        }


        // Tire le ballon
		if ( Input.GetButtonUp ("Jump") || Input.GetButtonUp ("Fire1") || currentthrowSpeed > throwSpeedMax ) {
			
			if ( player.ownBall() == true ) {

				Ball ball = player.getBall();

				// Put the ball in from of the ship by 1.0f
				Vector3 dir = transform.forward;// * 10.0f;
				Vector3 pos = transform.position + dir;

				// Toggle on and off isKinematic to reset the player's momentum
				Rigidbody rb   = transform.GetComponent<Rigidbody>();
				rb.isKinematic = true;

				ball.setPosition( pos );

				ball.setVelocity ( new Vector3 ( transform.forward.x * currentthrowSpeed,
				                                 transform.forward.y * currentthrowSpeed,
				                                 transform.forward.z * currentthrowSpeed ) );

				ball.hideGrabbed();
				ball.setRebound( false );
				ball.show();

				// The player is the thrower of the ball
				ball.setThrower( player );

				// The player give away the ball
				player.giveAwayBall();

				rb.isKinematic = false;		

				player.playSound( AudioController.AUDIO_PLAYER_BALL_THROW );

			}

			level.setLevel( 0 );
			currentthrowSpeed = 0.0f;
			
		}

		// Sound of the main ship
		if ( Input.GetAxis ("Horizontal") != 0 || Input.GetAxis ("Vertical") != 0
  	     ||( game.getConfigs().getArrowsMouseMode() && ( Input.GetAxis ("Arrows X") != 0 || Input.GetAxis ("Arrows Y") != 0 ) ) ) {

			// Start the sound if not already started
			if ( player != null && !player.isPlaying( AudioController.AUDIO_PLAYER_SHIP_NOISE ) )
				player.playSound( AudioController.AUDIO_PLAYER_SHIP_NOISE, 0.0f, true );

			player.setSoundVolume( AudioController.AUDIO_PLAYER_SHIP_NOISE, 0.2f );

		} else
			player.stopPlaying( AudioController.AUDIO_PLAYER_SHIP_NOISE );

		Vector3 newPosition  = new Vector3();
		bool    displacement = false;

        // DÃ©placement Ã  gauche
		if ( Input.GetAxis ("Horizontal") < 0 || ( !game.getConfigs().getArrowsMouseMode() && Input.GetAxis ("Arrows X") < 0 ) ){
			
			newPosition  = transform.localPosition + transform.TransformDirection( Vector3.left * speed * Time.deltaTime );
			displacement = true;
			idleTimer    = Time.time;

        }

        
        // DÃ©placement Ã  droite
		if ( Input.GetAxis ("Horizontal") > 0 || ( !game.getConfigs().getArrowsMouseMode() && Input.GetAxis ("Arrows X") > 0 ) ) {
			
			newPosition  = transform.localPosition + transform.TransformDirection( Vector3.right * speed * Time.deltaTime );
			displacement = true;
			idleTimer    = Time.time;

        }

        
        // Avance par en avant
		if ( Input.GetAxis ("Vertical") > 0 || ( !game.getConfigs().getArrowsMouseMode() && Input.GetAxis ("Arrows Y") < 0 ) ) {
			
			newPosition  = transform.localPosition + transform.TransformDirection( Vector3.forward * speed * Time.deltaTime ); // up
			displacement = true;
			idleTimer    = Time.time;

        } 

        
        // Recule le vaisseau
		if ( Input.GetAxis ("Vertical") < 0 || ( !game.getConfigs().getArrowsMouseMode() && Input.GetAxis ("Arrows Y") > 0 ) ) {
			
			newPosition  = transform.localPosition + transform.TransformDirection( Vector3.back * backSpeed * Time.deltaTime );
			displacement = true;
			idleTimer    = Time.time;

        }


		if ( displacement ) {

			if ( !player.isPrisoner() ) {

				transform.localPosition = newPosition;

			} else { // The player is a prisoner

				int prisonno = 0;

				if ( player.getTeamNo() == 1 ) {

					prisonno = 1;

				}

				// Toggle on and off isKinematic to reset the player's momentum
				Rigidbody rb   = transform.GetComponent<Rigidbody>();
				rb.isKinematic = true;

				transform.localPosition = newPosition;

				transform.position = game.fitByInPrison( prisonno, transform.position, 0.01f );

				rb.GetComponent<Rigidbody>().velocity = new Vector3( 0.0f, 0.0f, 0.0f );
				
				rb.isKinematic = false;


			}

		}

        
        // Aucunes touches n'est appuyÃ©e
		if ( !Input.GetButton ("Vertical") && !Input.GetButton ("Horizontal")
		|| ( !game.getConfigs().getArrowsMouseMode() && !Input.GetButton ("Arrows X") && !Input.GetButton ("Arrows Y") ) ) {

            // Stabilisation horizontale
            if ( transform.localRotation.eulerAngles.z % 360.0f >= 1.0f && transform.localRotation.eulerAngles.z % 360.0f <= 180.0f ) {
                
                transform.Rotate( 0.0f, 0.0f, -1.0f);
                
            } 
            
            // Stabilisation horizontale
            if ( transform.localRotation.eulerAngles.z % 360.0f > 180.0f && transform.localRotation.eulerAngles.z % 360.0f <= 359.0f ) {
                
                transform.Rotate( 0.0f, 0.0f, 1.0f );
                
            }

            // Effet de Hovering, Ã  modifier. Doit Ãªtre plus alÃ©atoire, ne pas se baser sur le framerate.
            // transform.localPosition = new Vector3(transform.localPosition.x + ( 0.001f * Mathf.Cos ( framecount / 25.0f ) ), transform.localPosition.y + ( 0.001f * Mathf.Sin ( framecount / 30.0f ) ), transform.localPosition.z + ( 0.001f * Mathf.Cos ( framecount / 20.0f ) ) );
            // transform.Rotate( new Vector3( 0.0001f * Mathf.Sin ( framecount / 25.0f ), 0.001f * Mathf.Cos ( framecount / 20.0f ), 0.0001f * Mathf.Sin ( framecount / 40.0f ) ),  Time.deltaTime * rotationSpeed );
            // framecount++;

        }
   
        // Rotation du vaisseau
		transform.Rotate( new Vector3( dy, dx, 0.0f ),  Time.deltaTime * rotationSpeed ); //smoothDeltaTime

    }
    
    

	public void lostTargetLock() {

		targetLockEngaged   = false;
		targetLockReqEngage = false;
		targetLockLost      = false;

	}
 


    private void targetLock( float dx, float dy ) {

		RaycastHit hit            = new RaycastHit();
		bool       rayCastResult;
		bool       skipTargetLock = false;
		Player     target         = null;

		if (   Mathf.Abs( dx ) > idleMouseTreshold 
		    || Mathf.Abs( dy ) > idleMouseTreshold ) {
            //|| Input.GetButtonUp( "Jump" ) || Input.GetButtonUp( "Fire1" ) || !Input.GetButton ("Fire3") ) {

			// Deactivate the TargetLock
			rotationSpeed       = maxRotationSpeed; // the ship gets out of the IDLE mode (more precise rotations)
			targetLockEngaged   = false;
			targetLockReqEngage = false;
			targetLockLost      = false;
			idleTimer           = Time.time;

		}

		if ( Mathf.Abs( dx ) > unlockMouseTreshold || Mathf.Abs( dy ) > unlockMouseTreshold ) {

			targetLockLost = true;

			// TODO: MAY PUT THIS BACK, QUICKER VISUAL RELEASE
			// setTargetMode( 3 );
			
			return;
			
		}


//		if ( game.getConfigs().getArrowsMouseMode() && ( Mathf.Abs( Input.GetAxis( "Arrows X" ) ) > unlockMouseTreshold || Mathf.Abs( Input.GetAxis( "Arrows Y" ) ) > unlockMouseTreshold ) ) {
//			
//			targetLockLost = true;
//			
//			// TODO: MAY PUT THIS BACK, QUICKER VISUAL RELEASE
//			// setTargetMode( 3 );
//			
//			return;
//			
//		}
                                 
		if ( !targetLockEnabled )
			oldFirstPersonMode = game.getFirstPersonView();

        // TargetLock active?
		if ( !Input.GetButton ("Fire3") ) {
          
            // Deactivate TargetLock
			game.setFirstPersonView( oldFirstPersonMode );

			targetLockEnabled   = false;
            targetLockEngaged   = false;
            targetLockReqEngage = false;
            targetLockLost      = false; 
			rotationSpeed       = maxRotationSpeed; // the ship gets out of the IDLE mode (more precise rotations)
			idleTimer           = Time.time;

            msgTargetLockLocked.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f );
            msgTargetLockEngage.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f );      
			msgTargetLockPass.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f ); 
            
            targetCamera.GetComponent<Camera> ().fieldOfView = targetLockZoom; // Zoom to target
            
            skipTargetLock = true;
            int layerMask  = 1 << 12; // Layers spaceships = 12, ballcollider = 18
            
			// Detect a target with a Raycast or RayShere  
			if ( gameController.getConfigs().getRayCastRadius() == 0.0f )
				rayCastResult = Physics.Raycast( transform.position, transform.forward, out hit, 1000, layerMask );
			else
				rayCastResult = Physics.SphereCast( transform.position, gameController.getConfigs().getRayCastRadius(), transform.forward, out hit, 1000, layerMask );			      

            if ( hit.collider != null ) {

				if ( hit.collider.GetComponent<PlayerObject>() != null )
					target = hit.collider.GetComponent<PlayerObject>().playerObj;

				if ( target != null && ( target.getTeamNo() == game.getMainPlayer().getTeamNo() ) )
					setTargetMode( 2 );
				else if ( target != null )
					setTargetMode( 1 );

            } else                
				setTargetMode( 3 );  

			//return;
            
		} else if ( Input.GetButton ("Fire3") && !targetLockEnabled ) {

			targetLockEnabled  = true;
			game.setFirstPersonView( true ); // First person view

		}

        if ( !skipTargetLock ) {
            
            // Lock in direction of target if TargetLock activated
            if ( targetLockEngaged || targetLockReqEngage )
                transform.Rotate( new Vector3( dy, dx, 0.0f ) );                                          
                
            int layerMask = 1 << 12; // Layers spaceships = 12, ballcollider = 18            

			if ( game.getMainPlayer().isPrisoner() ) {

				RaycastHit[] hits;

				// Detect a target with a Raycast or RayShere
				if ( gameController.getConfigs().getRayCastRadius() == 0.0f )
					hits = Physics.RaycastAll( transform.position, transform.forward, 1000, layerMask );
				else
					hits = Physics.SphereCastAll( transform.position, gameController.getConfigs().getRayCastRadius(), transform.forward, 1000, layerMask );			      

				// We do not want to detect teammate in prison
				foreach ( RaycastHit thehit in hits ) {

					if ( ( thehit.collider.GetComponent<PlayerObject>() != null
					  && thehit.collider.GetComponent<PlayerObject>().playerObj.getTeamNo() !=  game.getMainPlayer().getTeamNo() )
					  ||!thehit.collider.GetComponent<PlayerObject>().playerObj.isPrisoner() ) {

						hit = thehit;
						break;

					}

				}

			} else {

				// Detect a target with a Raycast or RayShere
				if ( gameController.getConfigs().getRayCastRadius() == 0.0f )
					rayCastResult = Physics.Raycast( transform.position, transform.forward, out hit, 1000, layerMask );
				else
					rayCastResult = Physics.SphereCast( transform.position, gameController.getConfigs().getRayCastRadius(), transform.forward, out hit, 1000, layerMask );			      

			}

			if ( hit.collider != null ) {
				
				if ( hit.collider.GetComponent<PlayerObject>() != null )
					target = hit.collider.GetComponent<PlayerObject>().playerObj;

			} else
				target = null;

			// Don't activate the TargetLock on teammate in prison
			if ( target != null && target.isPrisoner() && game.getMainPlayer().isPrisoner() ) { //&& target.getTeamNo() == game.getMainPlayer().getTeamNo() )

				//targetLockEnabled   = false;
				targetLockEngaged   = false;
				targetLockReqEngage = false;
				targetLockLost      = false; 
				target              = null;

				return;

			}

            // Delay between TargetLocks
            if ( targetLockLost && Time.time - targetLockLostTimer > targetNextLockDelay )                    
                targetLockLost = false;			            
			            
			// Point to the target in TargetLock mode or if the the Request Engage timeout is not over
            if ( hit.collider == null && !targetLockLost 
                && ( targetLockEngaged || ( targetLockReqEngage && ( Time.time - targetLockEngageTimer < targetLockEngageDelay ) ) ) ) {
            
				// To prevent locks on teammates
				if ( target != null && ( lockOnteamMates || target.getTeamNo() != player.getTeamNo() || targetLockEngaged ) ) {

                	Vector3 relativePos = targetLockCollider.transform.position - transform.position;
                	Quaternion rotation = Quaternion.LookRotation( relativePos );
                	transform.rotation  = rotation;

				}
        
            }
        
            // Show the BLINK "LOCKED" message
            if ( targetLockEngaged ) {         
            
                if ( Time.time - textBlinkTimer > textBlinkTimeSec ) {  

                    if ( msgTargetLockLocked.GetComponent<CanvasRenderer> ().GetAlpha() == 0.0f ) {

                        msgTargetLockLocked.GetComponent<CanvasRenderer> ().SetAlpha( textAlphaMax );                 

					} else                
                        msgTargetLockLocked.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f );                                                       
                
                    textBlinkTimer = Time.time;

                }

            } else            
                msgTargetLockLocked.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f );                                

            // Show the BLINK "ENGAGE" message
			if ( target != null && targetLockReqEngage ) {         
            
				if ( target.getTeamNo() != game.getMainPlayer().getTeamNo() ) {

                	if ( Time.time - textBlinkTimer > textBlinkTimeSec ) {  

                    	if ( msgTargetLockEngage.GetComponent<CanvasRenderer> ().GetAlpha() == 0.0f ) {

							// Play the ENGAGE sound
							if ( !gameController.game.isPlaying( AudioController.AUDIO_TARGET_REQ_ENGAGE ) )								
								gameController.game.playSound( AudioController.AUDIO_TARGET_REQ_ENGAGE );								

                        	msgTargetLockEngage.GetComponent<CanvasRenderer> ().SetAlpha( textAlphaMax );                 

						} else                
                        	msgTargetLockEngage.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f );                                                        
                
                    	textBlinkTimer = Time.time;

                	}

				} else {

					if ( game.getMainPlayer().ownBall() && Time.time - textBlinkTimer > textBlinkTimeSec ) {  
						
						if ( msgTargetLockPass.GetComponent<CanvasRenderer> ().GetAlpha() == 0.0f )							
							msgTargetLockPass.GetComponent<CanvasRenderer> ().SetAlpha( textAlphaMax ); 							
						else							
							msgTargetLockPass.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f );                    							
						
						textBlinkTimer = Time.time;
						
					}

				}

            } else {
            
                msgTargetLockEngage.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f );  
				msgTargetLockPass.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f ); 
            
            }   

            // The player pressed the button to activate the target lock?
			if ( targetLockReqEngage && ( Input.GetButtonDown( "Jump" ) || Input.GetButtonDown( "Fire1" ) ) ) {
            
                targetLockReqEngage   = false;
                targetLockEngaged     = true;
                targetLockEngagedTime = Time.time;

				// Play the TARGETLOCK sound
				if ( !gameController.game.isPlaying( AudioController.AUDIO_TARGET_LOCKED ) )								
					gameController.game.playSound( AudioController.AUDIO_TARGET_LOCKED );
				
			}
                
            // Detected target?
            if ( hit.collider != null && !targetLockLost ) {

				if ( target != null && target.getTeamNo() == game.getMainPlayer().getTeamNo() )					
					setTargetMode( 2 );					
				else if ( target != null )
					setTargetMode( 1 );							

                // Cannot ENGAGE the TargetLock if fire button activated
				if ( !targetLockEngaged && !targetLockReqEngage && !Input.GetButton( "Jump" ) && !Input.GetButton( "Fire1" ) ) {

                    targetLockReqEngage      = true;
                    targetLockEngaged        = false;
                    targetLockLost           = false;                
                    targetLockEngageTimer    = Time.time;	
                    rotationSpeed            = 1.0f;               // More precise rotation
                    targetLockEngageOriginal = transform.rotation; // Save actual position of the ship                
                    targetLockCollider       = hit.collider;       // Save target to follow it

                } else {

                    rotationSpeed = lockRotationSpeed;
                
                    // If timeouts over deactivate ENGAGE and REQUEST ENGAGE modes
                    if ( ( targetLockEngaged   && ( Time.time - targetLockEngagedTime > targetLockTimeout ) ) 
                      || ( targetLockReqEngage && ( Time.time - targetLockEngageTimer > targetLockEngageDelay ) ) ) {                         

						// TODO: Test this is desactivated because it's annoying
                        // if ( targetLockReqEngage )                        
                        //    transform.rotation = targetLockEngageOriginal;                                                   
                    
                        targetLockEngaged     = false;
                        targetLockReqEngage   = false;
                        targetLockLost        = true;
                        targetLockLostTimer   = Time.time;

						// Play the TARGET LOST sound
						if ( !gameController.game.isPlaying( AudioController.AUDIO_TARGET_LOST ) )								
							gameController.game.playSound( AudioController.AUDIO_TARGET_LOST );

                    }

                }

                // Zoom lentement vers le Target     
                targetCamera.GetComponent<Camera> ().fieldOfView = Mathf.Max ( 4.0f, Mathf.Lerp ( targetCamera.GetComponent<Camera> ().fieldOfView, 
                                                                                                  targetCamera.GetComponent<Camera> ().fieldOfView  
                                                                                                - ( hit.distance * 23.8f / 60.0f ), Time.deltaTime * targetZoomSpeed ) );                                                                                      
            
				// Look in direction of the target in ENGAGE mode even if the REQUEST ENGAGE time is not over
                if ( targetLockEngaged || ( targetLockReqEngage && ( Time.time - targetLockEngageTimer < targetLockEngageDelay ) ) ) {
                
					// To prevent locks on teammates
					if ( target != null && ( lockOnteamMates || target.getTeamNo() != player.getTeamNo() || targetLockEngaged ) ) {

						Vector3 relativePos = hit.transform.position - transform.position;
                    	Quaternion rotation = Quaternion.LookRotation( relativePos );
                    	transform.rotation  = rotation;

					}
                
                }

            }  else { // No target found             

                // Deactivate Target Lock
                if ( ( !targetLockEngaged && !targetLockReqEngage ) || targetLockLost ) {
    
                    targetLockEngaged   = false;
                    targetLockReqEngage = false;
                    rotationSpeed       = maxRotationSpeed;

					targetCamera.GetComponent<Camera> ().fieldOfView = targetLockZoom; // Zoom to target

                }

				setTargetMode( 0 );

            }     
            
        } // End skipTargetLock

// IMPORTANT THIS IS THE PART OF THE IDLE TIMING THAT DO NOT WORK

// SHOULD BRING BACK THIS FUNCTION WHEN IN FOUND OUT WHAT IS WRONG
//        // If the ship moves get out of the IDLE mode (more precise rotations)
//		if ( Input.GetButton ( "Horizontal" ) || Input.GetButton ( "Vertical" )
//		    // ATTENTION CETTE LIGNE NETAIT PAS COMMENTE TANTOT ET CETAIT UN && LA LIGNE DAPRES
//		    && ( Input.GetAxis( "Mouse X" ) == 0.0f && Input.GetAxis( "Mouse Y" ) == 0.0f )
//		    || ( game.getConfigs().getArrowsMouseMode() && ( Mathf.Abs( Input.GetAxis( "Arrows X" ) ) == 0.0f || Mathf.Abs( Input.GetAxis( "Arrows Y" ) ) == 0.0f ) ) ) {
//
//            idleTimer = Time.time;
//            
//            
//        }

// THIS TEMPORARELY REPLACES THE COMMENTED LINES ABOVE
		if ( Input.GetButton ( "Horizontal" ) || Input.GetButton ( "Vertical" ) ) {
		
		    idleTimer = Time.time;
		            
		            
		}


//		if (   Mathf.Abs( dx ) > idleMouseTreshold 
//		    || Mathf.Abs( dy ) > idleMouseTreshold ) { 
//		    // ATTENTION CETTE LIGNE ETAIT COMMENTE TANTOT
//		    //|| Input.GetButtonUp( "Jump" ) || Input.GetButtonUp( "Fire1" ) || !Input.GetButton ("Fire3")
//		    //|| ( game.getConfigs().getArrowsMouseMode() && ( Mathf.Abs( Input.GetAxis( "Arrows X" ) ) > unlockMouseTreshold || Mathf.Abs( Input.GetAxis( "Arrows Y" ) ) > unlockMouseTreshold ) ) ) {
//            
//			// Deactivate the TargetLock
//			rotationSpeed       = maxRotationSpeed; // the ship gets out of the IDLE mode (more precise rotations)
//            targetLockEngaged   = false;
//            targetLockReqEngage = false;
//            targetLockLost      = false;
//            idleTimer           = Time.time;
//
//			Debug.Log ("UNLOCK");
//		}

        	// If the ship do not move for a certain time rotations gets more precise
        	if ( Time.time - idleTimer < idleTimeCountdown )
            	rotationSpeed = maxRotationSpeed;
			else            
            	rotationSpeed = idleRotationSpeed;               
 
    }



	public void setTargetMode( byte mode ) {

		Color color;

		switch ( mode ) {

			case 1 : // Ennemy target

				targetImg.GetComponent<CanvasRenderer> ().SetAlpha( 0.9f );
				targetTeammateImg.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f );

				// Play the target on sound
				if ( !gameController.game.isPlaying( AudioController.AUDIO_TARGET_FOUND )
				   && playerTargetImg.GetComponent<SpriteRenderer> ().color.a == 0.0f ) {

					gameController.game.playSound( AudioController.AUDIO_TARGET_FOUND );

				}

				if ( showTargetAlways || targetLockEnabled ) {

					color   = playerTargetImg.GetComponent<SpriteRenderer> ().color;
					color.a = 0.5f;
		    		playerTargetImg.GetComponent<SpriteRenderer> ().color = color;
					color   = playerTargetTeammateImg.GetComponent<SpriteRenderer> ().color;
					color.a = 0.0f;
					playerTargetTeammateImg.GetComponent<SpriteRenderer> ().color = color;

				}

				break;

			case 2 : // Teammate target

				targetImg.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f );
				targetTeammateImg.GetComponent<CanvasRenderer> ().SetAlpha( 0.9f );

				// Play the target on sound
				if ( !gameController.game.isPlaying( AudioController.AUDIO_TARGET_FOUND )
			       && playerTargetTeammateImg.GetComponent<SpriteRenderer> ().color.a == 0.0f ) {
				
					gameController.game.playSound( AudioController.AUDIO_TARGET_FOUND );
				
				}

				if ( lockOnteamMates && ( showTargetAlways || targetLockEnabled ) ) {

					color   = playerTargetImg.GetComponent<SpriteRenderer> ().color;
					color.a = 0.0f;
					playerTargetImg.GetComponent<SpriteRenderer> ().color = color;
					color   = playerTargetTeammateImg.GetComponent<SpriteRenderer> ().color;
					color.a = 0.4f;
					playerTargetTeammateImg.GetComponent<SpriteRenderer> ().color = color;

				}
			    
				break;

			case 3 : // Hide main target
						    
				targetImg.GetComponent<CanvasRenderer> ().SetAlpha( 0.3f );
				targetTeammateImg.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f );
				color   = playerTargetImg.GetComponent<SpriteRenderer> ().color;
				color.a = 0.0f;
				playerTargetImg.GetComponent<SpriteRenderer> ().color = color;
				color   = playerTargetTeammateImg.GetComponent<SpriteRenderer> ().color;
				color.a = 0.0f;
				playerTargetTeammateImg.GetComponent<SpriteRenderer> ().color = color;
			
				break;


			default : // No target

				targetImg.GetComponent<CanvasRenderer> ().SetAlpha( 0.3f );
				targetTeammateImg.GetComponent<CanvasRenderer> ().SetAlpha( 0.0f );

				if ( showTargetAlways || targetLockEnabled ) {

					color   = playerTargetImg.GetComponent<SpriteRenderer> ().color;
					color.a = 0.15f;
					playerTargetImg.GetComponent<SpriteRenderer> ().color = color;
					color   = playerTargetTeammateImg.GetComponent<SpriteRenderer> ().color;
					color.a = 0.0f;
					playerTargetTeammateImg.GetComponent<SpriteRenderer> ().color = color;

				}

				break;

		}

	}

    
}
 