﻿using UnityEngine;
using System.Collections;

public class FullScreen : MonoBehaviour {

    private int defWidth;
    private int defHeight;

    public void Awake()
    {

        defWidth = Screen.width;
        defHeight = Screen.height;

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.F) && Application.isWebPlayer)
        {

            if (!Screen.fullScreen)
                Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
            else
                Screen.SetResolution(defWidth, defHeight, false);

        }

    }

}
