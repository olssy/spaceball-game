﻿using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour {

	public  Sprite[]       levelimagesMode0;	
	public  Sprite[]       levelimagesMode1;	
	private GameController gameController;
	private bool           done       = false;
	private float          initWidth  = 0.0f;
	private float          initHeight = 0.0f;
	private int            mode       = 0; // Normal mode
	
	void FixedUpdate() {

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		
		if ( gameControllerObject != null ) {
			
			gameController = gameControllerObject.GetComponent <GameController>();
			
		}
		
		if ( gameController == null ) {
			
			Debug.Log ("Cannot find 'GameController' script");
		
			return;

		}

		RectTransform rt = this.gameObject.GetComponent<RectTransform> ();

		if ( !gameController.gameStarted && done ) {

			Vector2 curpos     = rt.localPosition;
			curpos.y           = 1000.0f;
			rt.localPosition   = curpos;

		}

		if ( ( done && initWidth == Screen.width && initHeight == Screen.height ) || !gameController.gameStarted )
			return;

		float   scalefactor = ( Screen.height / 600.0f ); // Reference d'une hauteur d'ecran de 600 pixels
		Vector3 pos         = new Vector3();
		Vector3 scale       = new Vector3( 1.0f, 1.0f, 1.0f );
		
		if ( scalefactor > 1 ) 
			scalefactor = 1;
		
		pos.x =     ( Screen.width  / 2 ) - ( 50.0f * scalefactor );
		pos.y = - ( ( Screen.height / 2 ) - ( 155.0f * scalefactor ) );
		
		scale.x = rt.localScale.x * scalefactor; 
		scale.y = rt.localScale.y * scalefactor; 
		
		rt.localPosition = pos;		
		rt.localScale    = scale;

		initWidth  = Screen.width;
		initHeight = Screen.height;
		done       = true;
	
	}
	
	public void setLevel( int level ) {

		switch ( mode ) {

		   // Normal mode (ball)
		   case 0 :	if ( level < 0 || level >= levelimagesMode0.Length )
						return;

			        this.gameObject.GetComponent<UnityEngine.UI.Image>().sprite = levelimagesMode0[level];
		            break;

           // Laser mode
		   case 1 : if ( level < 0 || level >= levelimagesMode1.Length )
						return;

					this.gameObject.GetComponent<UnityEngine.UI.Image>().sprite = levelimagesMode1[level];
			        break;

		}

	}

	public void setMode( int newmode ) {

		this.mode = newmode;

	}

	public int getMode() {

		return this.mode;

	}

}
