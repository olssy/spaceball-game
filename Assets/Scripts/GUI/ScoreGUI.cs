﻿using UnityEngine;
using System.Collections;

public class ScoreGUI : MonoBehaviour
{
    // C#
    public Texture2D controlTexture;
    public Rect rect;
    public int scoreRouge;
    public int scoreBleu;

    public GUIStyle style;


    void OnGUI()
    {

        GUI.Label(this.rect, controlTexture);
        GUI.Label(this.rect, scoreRouge + " - " + scoreBleu,style);


    }

    public void setScoreRouge(int score)
    {
        this.scoreRouge = score;
    }

    public void setScoreBleu(int score)
    {
        this.scoreBleu = score;
    }

}