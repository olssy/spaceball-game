﻿using UnityEngine;
using System.Collections;

public class TargetLock : MonoBehaviour {

	private GameController gameController;
	private bool           done       = false;
	private float          initWidth  = 0.0f;
	private float          initHeight = 0.0f;

	void FixedUpdate() {

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		
		if ( gameControllerObject != null ) {
			
			gameController = gameControllerObject.GetComponent <GameController>();
			
		}
		
		if ( gameController == null ) {
			
			Debug.Log ("Cannot find 'GameController' script");

			return;

		}

		RectTransform rt = this.gameObject.GetComponent<RectTransform> ();
		
		if ( !gameController.gameStarted && done ) {
			
			Vector2 curpos     = rt.localPosition;
			curpos.y           = 1000.0f;
			rt.localPosition   = curpos;
			
		}

		if ( ( done && initWidth == Screen.width && initHeight == Screen.height ) || !gameController.gameStarted )
			return;

		float   scalefactor = ( Screen.height / 600.0f ); // Reference d'une hauteur d'ecran de 600 pixels
		Vector3 pos         = new Vector3();
		Vector3 scale       = new Vector3( 1.0f, 1.0f, 1.0f );

		if ( scalefactor > 1 ) 
			scalefactor = 1;

		pos.x =     ( Screen.width  / 2 ) - ( 200.0f * scalefactor );
		pos.y = - ( ( Screen.height / 2 ) - ( 150.0f * scalefactor ) );

		scale.x = rt.localScale.x * scalefactor; 
		scale.y = rt.localScale.y * scalefactor; 

		rt.localPosition = pos;	    
		rt.localScale    = scale;

		initWidth  = Screen.width;
		initHeight = Screen.height;
		done       = true;

	}

}
