﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum TypeDeDonnee {Difficulte,texte,Qualite };
public enum Difficulte { Easy,Normal,Hardcore,Insane,Suicidale };

public class SliderValue : MonoBehaviour {
    public TypeDeDonnee typeDeDonnee = TypeDeDonnee.texte;
    public Text guitext;
    public Slider slider;



    void Update()
    {

        Difficulte difficulty = Difficulte.Easy;

        if (typeDeDonnee == TypeDeDonnee.texte)
        {
            guitext.text = slider.value.ToString();
        }
        else if (typeDeDonnee == TypeDeDonnee.Difficulte)
        {
            difficulty = (Difficulte)(int)slider.value -1;

            guitext.text = difficulty + "";
        }
        else if (typeDeDonnee == TypeDeDonnee.Qualite)
        {
			if ( (int)slider.value == 1 ) 
				guitext.text = "Low";
			else if ( (int)slider.value == 2 ) 
				guitext.text = "Medium";
		    else if ( (int)slider.value == 3 ) 
				guitext.text = "High";

        }

    }
}

