﻿using UnityEngine;
using System.Collections;

public class ChangeScene : MonoBehaviour {
	
	public void changeScene(string scene)
    {
        Application.LoadLevel(scene);
    }

    public void exitGame()
    {
        Application.Quit();
    }

    public void afficheSousMenu(GameObject sousMenu)
    {
        sousMenu.SetActive(true);
    }

    public void fermerSousMenu(GameObject sousMenu)
    {
        sousMenu.SetActive(false);
    }
}
