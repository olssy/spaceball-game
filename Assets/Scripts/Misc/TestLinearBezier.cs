﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestLinearBezier : MonoBehaviour {
	
	Move mov;
	
	void Start () {
		
		mov = new Move( transform.gameObject, 5.0f, 1000 );

		mov.setScale( 0.25f );
		
//		mov.addPathPoint( new Vector3( 0.0f, 2.0f, 1.0f ) );
//		mov.addPathPoint( new Vector3( 0.0f, 2.0f, 2.0f ) );
//		mov.addPathPoint( new Vector3( 0.0f, 2.0f, 6.0f ) );
		mov.addPathPoint( new Vector3( 0.0f, 2.0f, 1.0f ) );
		mov.addPathPoint( new Vector3( 0.0f, 4.0f, 2.0f ) );
		mov.addPathPoint( new Vector3( 0.0f, 6.0f, 4.0f ) );

		mov.setLoop( true );

		mov.interpolate();

		mov.drawCurve();

		//mov.drawControlPoints();
		mov.drawCurvePoints();

		List<float> lengths = mov.getSegmentsLengths( 0.001f );

		foreach ( float length in lengths ) 
			Debug.Log( "Size : " + length );

		mov.start();
	}
	
	
	// Do the actual animation
	void Update() {
		
		if ( !mov.isDone() ) {
			
			mov.animate();
			
		}
		
	}
	
	
}