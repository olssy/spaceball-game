﻿using UnityEngine;
using System.Collections;

public class Beacon : MonoBehaviour {

	public string name;
	
	void Start () {
	
		Debug.Log( name + " X: " + transform.position.x );
		Debug.Log( name + " Y: " + transform.position.y );
		Debug.Log( name + " Z: " + transform.position.z );

	}

}
