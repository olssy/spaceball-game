﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public Transform       target;
	public Vector3         distance        = new Vector3 ( 0f, 5f, -10f );

	public float           positionDamping = 3.0f;
	public float           rotateDamping   = 2.0f;

	private Transform      thisTransform;
	private bool           canMove         = true;
	private GameController gameController;


//    public bool gameStarted = false;

	void Start () {

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		
		if ( gameControllerObject != null ) {
			
			gameController = gameControllerObject.GetComponent <GameController>();
			
		}
		
		if ( gameController == null ) {
			
			Debug.Log ("Cannot find 'GameController' script");
			
		}

		thisTransform = transform;

	}

//    public void StartGame()
//    {
//        this.gameStarted = true;
//    }

	void LateUpdate () {

		if ( target == null || !gameController.gameStarted )
            return;

        if (canMove) {

	        if ( target.GetComponent<PlayerObject>().game.getFirstPersonView() ) {

                    Vector3 offset = new Vector3(0.0f, 0.0f, 0.1f);

                    thisTransform.position = target.position + target.forward / 10.0f;// + offset;

                    thisTransform.rotation = target.rotation;

                    //thisTransform.rotation = Quaternion.LookRotation ( thisTransform.position + target.forward, target.up );

                    //thisTransform.rotation = Quaternion.LookRotation ( target.position + 2 * offset, target.up );;

			} else {


        		Vector3 wantedPosition = target.position + (target.rotation * distance);
            	Vector3 currentPosition = Vector3.Lerp(thisTransform.position, wantedPosition, positionDamping * Time.deltaTime);

            	thisTransform.position = currentPosition;

                    //			Quaternion wantedRotation;
                    //			Vector3    campos = target.position - thisTransform.position;
                    //
                    //			// Rotate the camera if in first person
                    //			if ( target.GetComponent<PlayerObject>().game.getFirtPersonView() )
                    //				campos.z = -campos.z;
                    //
                    //			wantedRotation = Quaternion.LookRotation ( campos, target.up );

            	Quaternion wantedRotation = Quaternion.LookRotation(target.position - thisTransform.position, target.up);

            	thisTransform.rotation = wantedRotation;

			}

		} else { // La camera est collee sur le mur

    		thisTransform.position = Vector3.Lerp(thisTransform.position, target.position, positionDamping * Time.deltaTime);

    	}
        
	}

	void OnTriggerEnter ( Collider other ) {

		canMove = false;
		
	}
	
	void OnTriggerStay ( Collider other ) {
		
		
	}
	
	void OnTriggerExit ( Collider other ) {

		canMove = true;

	}

}
