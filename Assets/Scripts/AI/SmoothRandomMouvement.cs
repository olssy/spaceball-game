﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SmoothRandomMouvement : MonoBehaviour {

	public  bool showPath = false;
	private Move movement;



	// Do the actual animation
	void FixedUpdate() {
		
		if ( movement != null && !movement.isDone() ) {
			
			movement.animate();
			
		}
		
	}



	public void moveTo( Player player, Vector3 pos, float speed, Vector3 offset ) {

		Vector3 point = new Vector3();
		movement      = new Move( player.getObject(), speed, player.getGame().getConfigs().bezierCurvesPrecision );

		if ( Random.value < 0.5f )
			point.x = ( ( player.getPosition().x + pos.x ) / 2 ) - Random.Range( 0.0f, offset.x );
		else
			point.x = ( ( player.getPosition().x + pos.x ) / 2 ) + Random.Range( 0.0f, offset.x );
			
		if ( Random.value < 0.5f )
			point.y = ( ( player.getPosition().y + pos.y ) / 2 ) - Random.Range( 0.0f, offset.y );
		else
			point.y = ( ( player.getPosition().y + pos.y ) / 2 ) + Random.Range( 0.0f, offset.y );
			
		if ( Random.value < 0.5f )
			point.z = ( ( player.getPosition().z + pos.z ) / 2 ) - Random.Range( 0.0f, offset.z );
		else
			point.z = ( ( player.getPosition().z + pos.z ) / 2 ) + Random.Range( 0.0f, offset.z );
			
		movement.addPathPoint( point );

		// Destination
		movement.addPathPoint( pos );

		movement.interpolate();

		if ( showPath )
			movement.drawCurve();	

		movement.start();

	}



	public void moveTo( Player player, List<Vector3> points, float speed ) {

		movement = new Move( player.getObject(), speed, player.getGame().getConfigs().bezierCurvesPrecision );		

		for ( int i = 0; i < points.Count; ++i ) {

			movement.addPathPoint( points[i] );

		}

		movement.interpolate();
		
		if ( showPath )
			movement.drawCurve();	
		
		movement.start();
		
	}



	public bool isDone() {

		if ( movement == null )
			return true;

		return movement.isDone();

	}



	public void stop() {

		 if ( movement != null )
		 	movement.stop();
		
	}

}

