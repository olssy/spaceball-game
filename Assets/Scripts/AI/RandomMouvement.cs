﻿using UnityEngine;
using System.Collections;

public class RandomMouvement : MonoBehaviour {

	private float speedX          = 1.0f;
	private float speedY          = 1.0f;
	private float speedZ          = 1.0f;
	private float minForwardSpeed = 1.0f;
	private float maxForwardSpeed = 8.0f;
	private float prisonSlowDown  = 0.3f;
	private float prisonBackSpeed = 0.4f;
	private bool  newMove         = false;


	// Use this for initialization
	void Start () {
	
	}



	// Update is called once per frame
	void doRandomMouvement() {

		//return;

		Vector3 direction = this.GetComponent<Rigidbody>().velocity;
		Player  player    = this.GetComponent<PlayerObject>().playerObj;
		Game    game      = player.getGame();

		// The player is in prison?
		if ( !player.isPrisoner() ) {

			if ( Random.value > 0.99f ) {

				direction = new Vector3 ( Random.Range( -speedX, speedX ), direction.y, direction.z );

			}

			if ( Random.value > 0.99f ) {
			
				direction = new Vector3 ( direction.x, Random.Range( -speedY, speedY ), direction.z );

			}

			if ( Random.value > 0.99f ) {
			
				direction = new Vector3 ( direction.x, direction.y, Random.Range( -speedZ, speedZ ) );

			}

			direction.Normalize();
			direction *= Random.Range( minForwardSpeed, maxForwardSpeed );

			if ( Random.value > 0.005f ) {

				this.GetComponent<Rigidbody>().velocity = Vector3.Lerp( this.GetComponent<Rigidbody>().velocity, direction, Time.deltaTime * 142.0f );

			} else {

		    	this.GetComponent<Rigidbody>().velocity = new Vector3 ( 0.0f, 0.0f, 0.0f ); 
	    	}

			this.GetComponent<Rigidbody>().angularVelocity = new Vector3 ( 0.0f, 0.0f, 0.0f );

			if ( this.GetComponent<Rigidbody>().velocity != new Vector3 ( 0.0f, 0.0f, 0.0f ) )
		    	transform.rotation = Quaternion.LookRotation ( this.GetComponent<Rigidbody>().velocity.normalized );

		} else { // This is a prisonier

			Game.Rect3D field = game.getFieldSize();

			Vector3 pos = new Vector3 ( player.getPosition().x, field.centerX(), field.centerZ() );

			if ( Random.value > 0.99 ) {

				newMove = true;

			}

			if ( newMove ) {

				// Look at the center of the field
				player.headTo( pos );

				//if ( Random.value > 0.99f ) {

				direction = new Vector3 ( Random.Range( -speedX, speedX ), 0.0f, 0.0f );

				//}

				//if ( Random.value > 0.99f ) {

				if ( player.getTeamNo() == 1 )
					direction = new Vector3 ( direction.x, direction.y, Random.Range( prisonBackSpeed, speedZ ) );
				else
					direction = new Vector3 ( direction.x, direction.y, Random.Range( -speedZ, prisonBackSpeed ) );


				direction = new Vector3 ( direction.x, Random.Range( -speedY, speedY ), direction.z );
				//}

				direction.Normalize();
				direction *= Random.Range( minForwardSpeed * prisonSlowDown, maxForwardSpeed * prisonSlowDown );

				this.GetComponent<Rigidbody>().velocity = direction ;
//				this.GetComponent<Rigidbody>().velocity = Vector3.Lerp( this.GetComponent<Rigidbody>().velocity, direction, Time.deltaTime * 142.0f );

				newMove = false;

			}					

		}
	
	}
}
