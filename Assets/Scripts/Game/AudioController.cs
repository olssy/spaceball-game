﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioController : MonoBehaviour {

	[Range(0.0f, 1.0f)] public float volumeMaster = 0.8f;
	[Range(0.0f, 1.0f)] public float volumeFX     = 0.8f;
	[Range(0.0f, 1.0f)] public float volumeMusic  = 0.6f;

	public AudioClip[] soundtracks;
	public AudioClip[] segmentsSoundtrack;
	public AudioClip[] clips;

	// Test sound
	public const int AUDIO_TEST                = 0;  // WORKING

	// Objects actions sounds
	public const int AUDIO_BALL_HIT_TARGET     = 1;  // FORGET
	public const int AUDIO_BALL_HIT_WALLS      = 2;  // WORKING
	public const int AUDIO_BALL_HIT_BLOCS      = 3;  // WORKING
	public const int AUDIO_LASER_SHOT          = 4;  // WORKING (must be a loop)
	public const int AUDIO_TARGET_EXPLODE      = 5;  // WORKING
	public const int AUDIO_TOUCH_FRONTIER      = 6;  // WORKING
	public const int AUDIO_FLYING_BALL         = 7;  // WORKING
	public const int AUDIO_PLAYER_SHIP_NOISE   = 38; // WORKING
	public const int AUDIO_BLOC_COLLISION      = 15; // WORKING
	public const int AUDIO_SHIPS_COLLISION     = 16; // FORGET
	public const int AUDIO_SHIP_GETOUT_JAIL    = 17; // IMPLEMENTED
	public const int AUDIO_SHIP_NOISE          = 37; // WORKING
	public const int AUDIO_GET_POWERUP         = 18; // FORGET (we forget it for the moment because there's only the laser powerup and there's a sound for it)
	public const int AUDIO_FRONTIER_TRESPASS   = 51; // WORKING MAIS TU PEUX LE CHANGER CAR J'AI MIS LE MEME MAIS PLUS FORT QUE CELUI QUAND ON TOUCHE LA FRONTIERE

	// Main Player
	public const int AUDIO_PLAYER_GOTO_JAIL    = 8;  // WORKING 
	public const int AUDIO_PLAYER_GETOUT_JAIL  = 9;  // WORKING
	public const int AUDIO_PLAYER_HIT          = 10; // IMPLEMENTED
	public const int AUDIO_PLAYER_LOSE_BALL    = 11; // WORKING
    public const int AUDIO_PLAYER_GRAB_BALL    = 12; // WORKING
    public const int AUDIO_PLAYER_GRAB_HIT     = 13; // FORGET
	public const int AUDIO_PLAYER_BALL_THROW   = 14; // WORKING

	// Laser
	public const int AUDIO_GET_LASER           = 47; // WORKING
    public const int AUDIO_LASER_LOW           = 48; // IMPLEMENTED BUT MAY NOT BE OK
	public const int AUDIO_LASER_EMPTY         = 49; // IMPLEMENTED BUT MAY NOT BE OK (warning when we try to fire the laser but it is empty)
	public const int AUDIO_LASER_LOOSE         = 52; // IMPLEMENTED (when the player looses the laser)

	// Game evenements sounds
	public const int AUDIO_START_GAME          = 19; // WORKING
    public const int AUDIO_START_ROUND         = 20; // WORKING
    public const int AUDIO_END_ROUND           = 21; // WORKING
    public const int AUDIO_GAMEOVER_EQUALITY   = 22; // WORKING
    public const int AUDIO_GAMEOVER_WIN        = 23; // WORKING
    public const int AUDIO_GAMEOVER_LOST       = 24; // WORKING
	public const int AUDIO_ZERO_GRAVITY_ON     = 25; // WORKING 
    public const int AUDIO_ZERO_GRAVITY_SOUND  = 26; // WORKING (must be a loop)
    public const int AUDIO_ZERO_GRAVITY_OFF    = 27; // WORKING 
    public const int AUDIO_LOW_COUNTDOWN_ALERT = 28; // NOT IMPLEMENTED (sound played when the countdown about to expire)
	public const int AUDIO_PRISON_SOUND        = 29; // IMPLEMENTED (must be a loop)
	public const int AUDIO_COUNTDOWN           = 30; // WORKING
    public const int AUDIO_INVINSIBILITY_ON    = 21; // IMPLEMENTED
	public const int AUDIO_INVINSIBILITY_OFF   = 32; // IMPLEMENTED
	public const int AUDIO_INVINSIBILITY_SOUND = 33; // IMPLEMENTED (must be a loop)

	// Ambient sounds
	public const int AUDIO_AMBIENT_BACKGROUND  = 39; // WORKING
    public const int AUDIO_FRONTIER_SOUND      = 40; // WORKING (must be a loop)

    // TargetLock
	public const int AUDIO_TARGET_FOUND        = 42; // FORGET(should be a very low subtile sound, a little soft tick, it's called when a target is detected, appens very often)
	public const int AUDIO_TARGET_LOST         = 43; // IMPLEMENTED (a target that was target locked is lost after 10 secondes)
	public const int AUDIO_TARGET_REQ_ENGAGE   = 44; // WORKING(short alarming repeating sound for the blinking ENGAGE text)
    public const int AUDIO_TARGET_LOCKED       = 45; // WORKING (short alarming repeating lock sound for the blinking LOCKED text)

    // The ball throw level
    public const int AUDIO_LEVEL_CHANGE        = 46; // IMPLEMENTED (sound of throw level change, small short click sound)
	public const int AUDIO_LEVEL_FULL          = 50; // IMPLEMENTED (a sound saying level full before throwing the ball automaticly)

	// Menus sounds
	public const int AUDIO_MENU_SLIDER_MOVE    = 35; // NOT IMPLEMENTED 
	public const int AUDIO_MENU_BUTTON_CLICK   = 36; // NOT IMPLEMENTED

	// Message
	public const int AUDIO_MESSAGE             = 41; // IMPLEMENTED (just a small bip, tong, blip, etc...)

	struct PlayObject {

		public GameObject  obj;
		public int         sound;
		public bool        loop;
		public AudioSource source;

	}

	public  AudioMixer       mixer;

	private List<PlayObject> playingObjects;
	private AudioClip        currentSoundtrack      = null;
	private int              currentSoundtrackIndex = -1;
	private AudioSource      soundtrackAudioSource;
	private bool             musicStop              = false;


	// Initialization
	void Start () {

		playingObjects = new List<PlayObject>();

		//GameObject gameControllerObject = GameObject.FindWithTag("GameController");

	}



	// Manage the playing objects
	void FixedUpdate () {

		setMasterVolume( volumeMaster );
		setFXVolume( volumeFX );
		setMusicVolume( volumeMusic );

		List<PlayObject> toDelete = new List<PlayObject>();
	
		foreach ( PlayObject obj in playingObjects ) {

			// Is the object playing?
			if ( obj.source == null || ( !obj.source.isPlaying && !obj.loop ) ) {

				toDelete.Add( obj );

			}

		}

		// Delete the sound entries if needed
		foreach ( PlayObject obj in toDelete ) {

			// Destroy the AudioSource Component
			if ( obj.source != null )
				Destroy( obj.source );

			playingObjects.Remove( obj );

		}

	}




	/* bool playSound( int sound, Object obj, float level, bool loop )
	 * 
	 * Play a sound
	 * 
	 */
	public void playSound( int sound, GameObject obj, float level, bool loop ) {

		// TODO: DO WE NEED THIS?
		// If the object is already playing a sound, stop the the old one first
		if ( clips[sound] == null || playingObjects.Exists( theObj => ( theObj.obj == obj ) && ( theObj.sound == sound ) ) ) {
			
			return;
			//stopSound( sound, obj ); // Stop the sound
			
		}	

		PlayObject  thePlayObject;
		AudioSource source;

        if ( obj != null )
			source = obj.AddComponent<AudioSource>();
		else { // Add the AudioSource to the AudioController object

			source = gameObject.AddComponent<AudioSource>();
			obj    = gameObject;

		}
		
		if ( source == null )
			return;

		thePlayObject.obj    = obj;
		thePlayObject.sound  = sound;
		thePlayObject.loop   = loop;
		thePlayObject.source = source;
		source.loop          = loop;

		playingObjects.Add( thePlayObject );

		source.clip               = clips[sound];
		source.transform.position = obj.transform.position;
		source.minDistance        = 1.0f;
		source.maxDistance        = 500.0f;
		source.enabled            = true;

		switch ( sound ) {

			case AUDIO_TEST :

				 source.volume                = Mathf.Min( level, 1.0f );
			     source.outputAudioMixerGroup = mixer.FindMatchingGroups( "Master" )[0];
				 source.priority              = 0;    // High
			     source.spatialBlend          = 0.0f; // Full 2D
			
				 break;

			case AUDIO_BALL_HIT_TARGET :

				 source.volume                = Mathf.Min( level, 1.0f );
			  	 source.outputAudioMixerGroup = mixer.FindMatchingGroups( "Balls" )[0];
				 source.priority              = 0;    // High
			     source.spatialBlend          = 0.9f; // Almost full 3D

				 break;

			case AUDIO_BALL_HIT_WALLS :

				 source.volume                = Mathf.Min( level, 1.0f );
				 source.outputAudioMixerGroup = mixer.FindMatchingGroups( "Balls" )[0];
			     source.priority              = 0;    // High
			 	 source.spatialBlend          = 1.0f; // Full 3D

				 break;

			case AUDIO_BALL_HIT_BLOCS :
			
				 source.volume                = Mathf.Min( level, 1.0f );
				 source.outputAudioMixerGroup = mixer.FindMatchingGroups( "Balls" )[0];
				 source.priority              = 0;    // High
			     source.spatialBlend          = 1.4f; // Almost full 3D
			
				 break;

			case AUDIO_LASER_SHOT :
			
				 source.volume                = Mathf.Min( level, 1.0f );
				 source.outputAudioMixerGroup = mixer.FindMatchingGroups( "Spaceships" )[0];
				 source.priority              = 0;    // High
				 source.spatialBlend          = 0.9f; // Almost full 3D
			
				 break;
			
			case AUDIO_TARGET_EXPLODE :
			
				 source.volume                = Mathf.Min( level, 1.0f );
				 source.outputAudioMixerGroup = mixer.FindMatchingGroups( "Spaceships" )[0];
				 source.priority              = 128;  // Medium
				 source.spatialBlend          = 0.8f; // Not full 3D
			
				 break;
            case AUDIO_TOUCH_FRONTIER:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("Frontier")[0];
                source.priority = 0;        // High
                source.spatialBlend = 1.0f; // Full 3D

                break;

			case AUDIO_FRONTIER_TRESPASS:
			
				source.volume                = Mathf.Min(level, 1.0f) * 0.2f;
				source.outputAudioMixerGroup = mixer.FindMatchingGroups("Frontier")[0];
				source.priority              = 0;    // High
				source.spatialBlend          = 0.0f; // Full 2D
			
				break;
				
				case AUDIO_PLAYER_GOTO_JAIL:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("Voices")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_PLAYER_GETOUT_JAIL:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("Voices")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;

            case AUDIO_GAMEOVER_EQUALITY:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("WinAndLoose")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_GAMEOVER_WIN:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("WinAndLoose")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_GAMEOVER_LOST:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("WinAndLoose")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_COUNTDOWN:

                source.volume = Mathf.Min(level, 1.0f) * 0.8f;
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("CountDown")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_START_ROUND:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("Voices")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_END_ROUND:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("Voices")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_ZERO_GRAVITY_ON:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("gravityOnOff")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_ZERO_GRAVITY_OFF:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("gravityOnOff")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_GET_LASER:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("otherEffect")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_LASER_LOOSE:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("otherEffect")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_LASER_EMPTY:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("otherEffect")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;

            case AUDIO_TARGET_LOST:
                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("targetLock")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_TARGET_REQ_ENGAGE:
                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("targetLock")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_TARGET_LOCKED:
                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("targetLock")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;
            case AUDIO_START_GAME:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("Voices")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;

            case AUDIO_PLAYER_BALL_THROW:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("PlayerEffect")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;

            case AUDIO_BLOC_COLLISION :

				source.volume                = Mathf.Min( level, 1.0f );
				source.outputAudioMixerGroup = mixer.FindMatchingGroups( "Blocs" )[0];
				source.priority              = 128;  // Medium
				source.spatialBlend          = 1.0f; // Full 3D

				break;

            case AUDIO_PLAYER_GRAB_BALL:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("PlayerEffect")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;

            case AUDIO_PLAYER_LOSE_BALL:

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("PlayerEffect")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D
                source.dopplerLevel = 1.0f;

                break;

            case AUDIO_FLYING_BALL :
			
				 source.volume                = Mathf.Min( level, 1.0f );
				 source.outputAudioMixerGroup = mixer.FindMatchingGroups( "FlyingBalls" )[0];
				 source.priority              = 0;    // High
				 source.spatialBlend          = 1.0f; // Full 3D
				 source.dopplerLevel          = 2.0f;

				 break;

			case AUDIO_SHIP_NOISE :
			
				source.volume                = Mathf.Min( level, 1.0f );
				source.outputAudioMixerGroup = mixer.FindMatchingGroups( "PlayerEffect" )[0];
				source.priority              = 0;    // High
				source.spatialBlend          = 1.0f; // Full 3D
				source.dopplerLevel          = 1.0f;
			
				break;

			case AUDIO_PLAYER_SHIP_NOISE :
			
				source.volume                = Mathf.Min( level, 1.0f );
				source.outputAudioMixerGroup = mixer.FindMatchingGroups( "Spaceships" )[0];
				source.priority              = 0;    // High
				source.spatialBlend          = 0.0f; // Full 2D
				source.dopplerLevel          = 1.0f;
			
				break;

			case AUDIO_AMBIENT_BACKGROUND :
			
				source.volume                = Mathf.Min( level, 1.0f );
				source.outputAudioMixerGroup = mixer.FindMatchingGroups( "AmbientBackground" )[0];
				source.priority              = 0;    // High
				source.spatialBlend          = 0.0f; // Full 2D
			
				break;

            case AUDIO_ZERO_GRAVITY_SOUND :

                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("AmbientBackground")[0];
                source.priority = 0;        // High
                source.spatialBlend = 0.0f; // Full 2D

                break;

            case AUDIO_FRONTIER_SOUND :
                source.volume = Mathf.Min(level, 1.0f);
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("Frontier")[0];
                source.priority = 0;        // High
                source.spatialBlend = 1.0f; // Full 3D
                source.dopplerLevel = 0.7f;

                break;

			case AUDIO_TARGET_FOUND :
			
				source.volume                = Mathf.Min( level, 1.0f );
				source.outputAudioMixerGroup = mixer.FindMatchingGroups( "PlayerEffect" )[0];
				source.priority              = 0;    // High
				source.spatialBlend          = 0.0f; // Full 2D
			
				break;

        }

		// Volume adjustment
		source.volume = source.volume * volumeFX * volumeMaster;

		// Play the sound
		if ( source.isActiveAndEnabled )
			source.Play();

	}

	public void playSound( int sound, GameObject obj ) {
		
		playSound( sound, obj, 1.0f, false );
		
	}
	
	public void playSound( int sound, GameObject obj, float level ) {
		
		playSound( sound, obj, level, false );
		
	}
	


	/* bool stopSound( int sound, GameObject obj )
	 * 
	 * Stop a playing sound
	 * 
	 */ 
	public bool stopSound( int sound, GameObject obj ) {

		PlayObject theObject;

		// If null it's the AudioController object
		if ( obj == null )
			obj = gameObject;

		if ( !playingObjects.Exists( theObj => ( theObj.obj == obj ) && ( theObj.sound == sound ) ) ) 
			return false;

		theObject = playingObjects.Find( theObj => ( theObj.obj == obj ) && ( theObj.sound == sound ) );

		// Destroy the AudioSource Component
		if ( theObject.source != null ) {

			// Stop the sound
			theObject.source.Stop();

			Destroy( theObject.source );

		}

		playingObjects.Remove( theObject );

		return true;

	}



	/* void stopAll()
	 * 
	 * Stop all sounds
	 * 
	 */ 
	public void stopAll() {
		
		foreach ( PlayObject obj in playingObjects ) {
			
			// Is the object playing?
			if ( obj.source != null ) {
				
				obj.source.Stop ();

				Destroy( obj.source );
				
			}
			
		}

		playingObjects.Clear();
		
	}



	/* void stopAllLoops()
	 * 
	 * Stop all loop sounds
	 * 
	 */ 
	public void stopAllLoops() {

		List<PlayObject> toDelete = new List<PlayObject>();

		foreach ( PlayObject obj in playingObjects ) {
			
			// Is the object playing?
			if ( obj.source != null && obj.loop == true ) {
				
				obj.source.Stop ();
				
				Destroy( obj.source );
				
			}
			
		}

		// Delete de looping sounds from the playing sound list
		foreach ( PlayObject obj in toDelete )			
			playingObjects.Remove( obj );			
		
	}



	/* bool isPlaying( int sound, GameObject obj )
	 * bool isPlaying( GameObject obj )
	 * 
	 * Is a sound already playing
	 * 
	 */ 
	public bool isPlaying( int sound, GameObject obj ) {

		// If null it's the AudioController object
		if ( obj == null )
			obj = gameObject;
		
		if ( playingObjects.Exists( theObj => ( theObj.obj == obj ) && ( theObj.sound == sound ) ) ) 
			return true;
		
		return false;
		
	}

	public bool isPlaying( GameObject obj ) {

		// If null it's the AudioController object
		if ( obj == null )
			obj = gameObject;

		if ( playingObjects.Exists( theObj => ( theObj.obj == obj ) ) ) 
			return true;
		
		return false;
		
	}



	/* void setVolume( int sound, GameObject obj, float level ) 
	 * 
	 * Set the volume of a sound
	 * 
	 */	
	public void setVolume( int sound, GameObject obj, float level ) {

		// If null it's the AudioController object
		if ( obj == null )
			obj = gameObject;

		foreach ( PlayObject curobj in playingObjects ) {

			if ( curobj.obj == obj && curobj.sound == sound ) {
				
				curobj.source.volume = Mathf.Min( level, 1.0f );;
				
			}
			
		}
				
	}
	
	
	
	/* void setPitch( int sound, GameObject obj, float pitch ) 
	 * 
	 * Set the pitch of a sound
	 * 
	 */	
	public void setPitch( int sound, GameObject obj, float pitch ) {

		// If null it's the AudioController object
		if ( obj == null )
			obj = gameObject;

		foreach ( PlayObject curobj in playingObjects ) {

			if ( curobj.obj == obj && curobj.sound == sound ) {
				
				curobj.source.pitch = pitch;
				
			}
			
		}
		
	}



	/* void setMasterVolume( float level )
	 * 
	 */	
	public void setMasterVolume( float level ) {

		mixer.SetFloat( "volumeMaster", ( -100.0f * ( ( 1.0f - level ) ) ) + 20.0f );
		
	}



	/* void setMasterVolume( Slider slider )
	 * 
	 */	
	public void setMasterVolume( Slider slider ) {

		float val = slider.value;
		
		if ( val <= 0.4 )
			val = 0.0f;
		
		mixer.SetFloat( "volumeMaster", ( -100.0f * ( ( 1.0f - val ) ) ) + 20.0f );
		volumeMaster = val;

	}



	/* float getMasterVolume()
	 * 
	 */	
	public float getMasterVolume() {

		float level = 0.0f;
		
		mixer.GetFloat( "volumeMaster", out level );
		
		return -( ( ( level - 20.0f ) / -100.0f ) - 1.0f );
		
	}



	/* void setFXVolume( float level )
	 * 
	 */	
	public void setFXVolume( float level ) {
		
		mixer.SetFloat( "volumeFX", ( -100.0f * ( ( 1.0f - level ) ) ) + 20.0f );
		
	}



	/* void setFXVolume( Slider slider )
	 * 
	 */	
	public void setFXVolume( Slider slider ) {

		float val = slider.value;
		
		if ( val <= 0.4 )
			val = 0.0f;
		
		mixer.SetFloat( "volumeFX", ( -100.0f * ( ( 1.0f - val ) ) ) + 20.0f );
		volumeFX = val;

	}



	/* float getFXVolume()
	 * 
	 */	
	public float getFXVolume() {

		float level = 0.0f;
		
		mixer.GetFloat( "volumeFX", out level );
		
		return -( ( ( level - 20.0f ) / -100.0f ) - 1.0f );
		
	}



	/* void setMusicVolume( float level )
	 * 
	 */	
	public void setMusicVolume( float level ) {
		
		mixer.SetFloat( "volumeMusic", ( -100.0f * ( ( 1.0f - level ) ) ) + 20.0f );
		
	}


	/* void setMusicVolume( Level level )
	 * 
	 */	
	public void setMusicVolume( Slider slider ) {

		float val = slider.value;

		if ( val <= 0.4 )
			val = 0.0f;

		mixer.SetFloat( "volumeMusic", ( -100.0f * ( ( 1.0f - val ) ) ) + 20.0f );
		volumeMusic = val;

	}


	/* float getMusicVolume()
	 * 
	 */	
	public float getMusicVolume() {

		float level = 0.0f;

		mixer.GetFloat( "volumeMusic", out level );

		return -( ( ( level - 20.0f ) / -100.0f ) - 1.0f );
		
	}



	/* void playSoundTrack( int soundtrack )
	 * 
	 */	
	public void playSoundtrack( int soundtrack ) {

		if ( soundtracks == null || soundtrack < 0 || soundtrack >= soundtracks.Length || soundtracks[soundtrack] == null )
			return;

		if ( currentSoundtrack != null )
			stopSoundtrack();

		soundtrackAudioSource = gameObject.AddComponent<AudioSource>();

		soundtrackAudioSource.loop                  = true;		
		soundtrackAudioSource.clip                  = soundtracks[soundtrack];
		soundtrackAudioSource.enabled               = true;
		soundtrackAudioSource.volume                = volumeMusic * volumeMaster;
		soundtrackAudioSource.outputAudioMixerGroup = mixer.FindMatchingGroups( "Music" )[0];
		soundtrackAudioSource.priority              = 0;    // High
		soundtrackAudioSource.spatialBlend          = 0.0f; // Full 2D

		// Start the soundtrack
		soundtrackAudioSource.Play();

		currentSoundtrack      = soundtracks[soundtrack];		
		currentSoundtrackIndex = soundtrack;
		musicStop              = false;

	}



	/* void playRandomSoundtrack()
	 * 
	 */	
	public void playRandomSoundtrack() {
				
		int soundtracknum = getSoundtracksNum();

		if ( soundtracks == null || soundtracks.Length < 1 || soundtracknum == 0 )
			return;

		int soundtrack = Mathf.FloorToInt( Random.Range( 0, soundtracknum ) );

		if ( soundtracks[soundtrack] == null || soundtrack == soundtracknum ) {

			playRandomSoundtrack();
			return;

		}

		playSoundtrack( soundtrack );
		
	}



	/* void playSegmentsSoundtrack()
	 * 
	 */	
	public void playSegmentsSoundtrack( int segment ) {

		int soundtracknum = getSegmentsSoundtracksNum();

		if ( segmentsSoundtrack == null || segmentsSoundtrack.Length < 1 || soundtracknum == 0 )
			return;
		
		if ( !musicStop && ( segment != -1 || soundtrackAudioSource == null || !soundtrackAudioSource.isPlaying ) ) {

			if ( soundtrackAudioSource != null || ( segment != -1 && soundtrackAudioSource != null ) )
				soundtrackAudioSource.Stop();

			int soundtrack = Mathf.FloorToInt( Random.Range( 0, soundtracknum ) );

			// Force the playback of a segement
			if ( segment != -1 )
				soundtrack = segment;

			if ( segment == -1 && ( segmentsSoundtrack[soundtrack] == null || soundtrack == soundtracknum || soundtrack == currentSoundtrackIndex ) ) {

				playSegmentsSoundtrack();
				return;

			}

			// Debug.Log( "Audio track changed to segment no : " + soundtrack );

			soundtrackAudioSource = gameObject.AddComponent<AudioSource>();
			
			soundtrackAudioSource.loop                  = false;		
			soundtrackAudioSource.clip                  = segmentsSoundtrack[soundtrack];
			soundtrackAudioSource.enabled               = true;
			soundtrackAudioSource.volume                = volumeMusic * volumeMaster;
			soundtrackAudioSource.outputAudioMixerGroup = mixer.FindMatchingGroups( "Music" )[0];
			soundtrackAudioSource.priority              = 0;    // High
			soundtrackAudioSource.spatialBlend          = 0.0f; // Full 2D
			
			// Start the soundtrack
			soundtrackAudioSource.Play();
			
			currentSoundtrack      = segmentsSoundtrack[soundtrack];		
			currentSoundtrackIndex = soundtrack;
			musicStop              = false;

		}

	}
	
	public void playSegmentsSoundtrack() {

		playSegmentsSoundtrack( -1 );

	}



	/* void stopSoundTrack()
	 * 
	 */	
	public void stopSoundtrack() {

		if ( currentSoundtrack != null && soundtrackAudioSource != null ) {

			// Stop the soundtrack
			soundtrackAudioSource.Stop();
			
			Destroy( soundtrackAudioSource );

			currentSoundtrack      = null;
			currentSoundtrackIndex = -1;
			musicStop              = true;
			
		}
		
	}
	


	/* int getSoundtrack()
	 * 
	 */	
	public int getSoundtrack() {
			
		return currentSoundtrackIndex;
		
	}



	/* int getSoundtracksNum()
	 * 
	 */	
	public int getSoundtracksNum() {

		int counter = 0;

		foreach ( AudioClip soundtrack in soundtracks ) {

			if ( soundtrack != null ) 
				counter++;

		}

		return counter;
		
	}


	/* int getSegmentsSoundtracksNum()
	 * 
	 */	
	public int getSegmentsSoundtracksNum() {
		
		int counter = 0;
		
		foreach ( AudioClip soundtrack in segmentsSoundtrack ) {
			
			if ( soundtrack != null ) 
				counter++;
			
		}
		
		return counter;
		
	}

}
