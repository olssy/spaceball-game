﻿/* Round
 * 
 * constructor:
 * 
 *   Round( float roundTime, Game game )
 * 
 * methods:
 * 
 *   void  start()
 * 	 void  stop()
 *   void  pause() (NOT IMPLEMENTED YET)
 *   float getTimeout()
 *   bool  isStarted()
 *   bool  isOver()
 *   void  incScore( int no, int points )
 *   int   getScore( int no )
 *   void  resetScores()
 * 
 */



using UnityEngine;
using System.Collections;

public class Round {

	private float timeout   = 60.0f;      // Timeout by default 1 minutes
	private float startTime = -1.0f;      // Time in seconds that the round was started, -1,0 = game not started
	private Game  game;                   // The game object that owns the round
	private int   time      = 0;          // Remainig time in seconds for the current round
	private int[] scores    = new int[2]; // A score for each team for the current round
	private short firstTeam = 0;          // The team that starts this round

	// Constructor
	public Round( float roundTime, Game thegame ) {

		this.game    = thegame;
		this.timeout = roundTime;

	}



	/* start()
	 * 
	 * Start the round
	 * 
	 */
	public void start() {

		this.startTime = Time.fixedTime;

	}



	/* stop()
	 * 
	 * Stop the round
	 * 
	 */
	public void stop() {
		
		// TODO
		this.startTime = -1.0f; // Game not started
		
	}



	/* pause()
	 * 
	 * Pause the round
	 * 
	 */
	public void pause() {
		
		// TODO
		
	}



	/* getTimeout()
	 * 
	 * Returns the time in seconds (float) remaning in the round
	 * 
	 * Returns -1.0f if the round is not started
	 * 
	 */
	public float getTimeout() {

		if ( this.startTime == -1.0f )
			return -1.0f; // Round not started
		else
			return Mathf.Max( ( this.timeout - ( Time.fixedTime - this.startTime ) ), 0.0f );

	}



	/* isStarted()
	 * 
	 * Is the round started?
	 * 
	 */
	public bool isStarted() {
		
		if ( this.startTime == -1.0f )
			return false; // Round not started
		else
			return true;
		
	}



	/* isOver()
	 * 
	 * Returns true if the round is over
	 * 
	 */
	public bool isOver() {

		// The round is over if all players of a team are in jail
		for ( int team = 0; team < 2; ++team ) {

			if ( game.getTeam( team ) != null && game.getTeam( team ).getJailPlayersNum() >= game.getPlayersByTeamNum() )
				return true; // Round over

		}

		if ( ( getTimeout() != 0.0f ) || ( this.startTime == -1.0f ) )
			return false;
		else
			return true;
		
	}



	/* incScore( int team, int points )
	 * 
	 * Increment by points the round score of team no
	 * 
	 */
	public void incScore( int no, int points ) {

		if ( no >= 0 && no < 2 )
			scores[no] += points;

	}



	/* getScore( int team )
	 * 
	 * Returns the score of team no, return -1 if team invalid
	 * 
	 */
	public int getScore( int no ) {
		
		if ( no >= 0 && no < 2 )
			return scores[no];
		else 
			return -1;
		
	}



	/* resetScores()
	 * 
	 * Reset to 0 the scores of all teams
	 * 
	 */
	public void resetScores() {
		
		for ( int i = 0; i < 2; ++i )
			scores[i] = 0;
		
	}

}
