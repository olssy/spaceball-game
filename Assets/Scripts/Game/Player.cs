﻿/* Player
 * 
 * constructor:
 * 
 *   Player( int teamno, int playerno, int gameIndex, Game game )
 * 
 * methods:
 * 
 *   int        getGameIndex()
 *   Game       getGame() 
 *   void       attachObject( GameObject obj )
 *   GameObject getObject()
 *   float      decHealt( float byval )
 *   float      incHealt( float byval )
 *   void       setHealt( float val )
 *   float      getHealt()
 *   bool       giveBall( Ball ball )
 *   void       giveAwayBall()
 *   bool       ownBall()
 *   Ball       getBall()
 *   void       setNeedToGiveAwayBall( bool state )
 *   bool       getNeedToGiveAwayBall()
 *   void       setMainPlayer( bool val )
 *   bool       isMainPlayer()
 *   void       setHumain( bool val )
 *   bool       isHumain()
 *   void       setPrisoner( bool val )
 *   bool       isPrisoner()
 *   void       setHovering( bool val )
 *   bool       isHovering()
 *   void       setDead( bool val )
 *   bool       isDead()
 *   void       hit()
 *   short      getHits()
 *   void       resetHits()
 *   void       targetHit()
 *   short      getTargetHits()
 *   void       resetTargetHits()
 *   Player     getTarget()
 *   Team       getTeam()
 *   int        getTeamNo()
 *   void       releaseTarget()
 *   void       setTarget( Player playerobj )
 *   Player[]   getTargeters()
 *   Vector3    getPosition()
 *   void       setPosition( Vector3 pos )
 *   void       setRotation( Quaternion rot )
 *   void       setLocalPosition( Vector3 pos )
 *   void       setVelocity( Vector3 velocity )
 *   Vector3    getVelocity()
 *   float      getSpeed()
 *   void       setSpeed( float speed )
 *   void       sendToJail()
 *   void       explodeAndSendToJail()
 *   void       explodeGetOutOfJail( Vector3 pos )
 *   bool       hasLaser()
 *   void       setLaser( bool val )
 *   void       playSound( int sound )
 *   void       playSound( int sound, float level )
 *   void       playSound( int sound, float level, bool loop )
 *   bool       isPlaying( int sound ) 
 *   bool       stopPlaying( int sound )
 *   void       setSoundVolume( int sound, float level )
 *   void       setSoundPitch( int sound, float pitch )
 *   void       showLaserGun( bool state )
 *   void       setLaserGunTexture( Material texture ) 
 * 
 */ 


using UnityEngine;
using System.Collections;

public class Player {

	// Public members

	// Private members
	private int        gameplayer;                  // The index of the player in all the game's players
	private int        player;                      // The index of the player in the team's players
	private int        team;                        // The index of the player's team
	private GameObject gameobject;                  // Use to attach a GameObject to the player
    private float      healt              = 100.0f; // Player's healt of the player in %	
	private short      hitsNum            = 0;      // Player's hits (the number of time the player has been hit)
	private short      targetHitsNum      = 0;      // Player's target hits (the number of time the player has hit an ennemy)
	private bool       hasBall            = false;  // Does this player has the ball
	private bool       mainPlayer         = false;  // This player is the player of the user
	private bool       humain             = false;  // Is this player humain or controlled by the game
	private bool       prisoner           = false;  // Is this player in prison
	private bool       hovering           = true;   // Is this player in hovering mode
	private bool       dead               = false;  // Is this player dead
	private bool       laser              = false;  // Has a laser
	private Game       game;                        // The current game object
    private Ball       grabbedBall        = null;   // The Ball object if the player owns it
    private Player     target             = null;   // Current player target, default null = none
    private Vector3    playerSpawn;                 // The initial position of player (Random)

	// Constructor
	public Player( int teamno, int playerno, int gameIndex, Game game ) {

		this.player     = playerno;
		this.team       = teamno;
		this.gameplayer = gameIndex;
		this.game       = game;

		this.healt      = 100.0f; // 100% healt
		this.hitsNum    = 0;      // The player has not been hit so far

    }



	/* getGameIndex()
	 * 
	 * Get the player's index in all game's players array
	 * 
	 */
	public int getGameIndex() {

		return this.gameplayer;

	}



    /* getGame()
     * 
     * Return the object of the current game
     * 
     */
    public Game getGame() {
        
        return this.game;
        
    }



	/* attachObject( GameObject obj )
	 * 
	 * Attach a GameObject to the player
	 * 
	 */
	public void attachObject( GameObject obj ) {

		this.gameobject     = obj;
		PlayerObject player = obj.GetComponent<PlayerObject>() as PlayerObject;
		player.team         = this.team;
		player.playerObj    = this;
		player.playerIndex  = gameplayer;
		player.game         = this.game;

	}



	/* getObject()
	 * 
	 * Get the GameObject attached to the player
	 * 
	 */
	public GameObject getObject() {
		
		return this.gameobject;
		
	}

		

	/* decHealt( short byval )
	 * 
	 * Decrease player healt by byval and returns the new healt
	 * 
	 */
    public float decHealt( float byval ) {
		
        this.healt = (float) Mathf.Min( 0.0f, this.healt - byval );

		return this.healt;

	}
	
	
	
	/* incHealt( short byval )
	 * 
	 * Increase player healt by byval and returns the new healt
	 * 
	 */
    public float incHealt( float byval ) {
		
        this.healt = (float) Mathf.Max( this.healt + byval, 100.0f );

		return this.healt;
		
	}
	
	
	
	/* setHealt( short val )
	 * 
	 * Set player healt to val
	 * 
	 */
    public void setHealt( float val ) {
		
        this.healt = (float) Mathf.Max( Mathf.Min( 0.0f, val ), 100.0f );
		
	}
	
	
	
	/* getHealt()
	 * 
	 * Get player healt
	 * 
	 */
    public float getHealt() {
		
		return this.healt;
		
	}

    /*setPlayerSpawn(Vector3 position)
    *
    *Set the position to respown
    */
    public void setPlayerSpawn(Vector3 position)
    {
        this.playerSpawn = position;
    }


	/* giveBall( Ball ball )
	 * 
	 * Give the player the ball
	 * 
	 */
    public bool giveBall( Ball ball ) {
	
		// If the player has a ball, it must give it away first
		if ( this.hasBall )
			return false;

		this.hasBall     = true;
		this.grabbedBall = ball;

		// The ball is the child of the player object
		this.grabbedBall.getGrabbedBallObj().transform.parent = this.gameobject.transform;

		// Place the ball at the right place on the player object
		// TODO: Should be parametrisable
		this.grabbedBall.setPosition( new Vector3 ( 0.0f, -0.1f, 0.8f ) );

		// The player is the owner of the ball
		this.grabbedBall.setGrabber( this );

		this.grabbedBall.hide();
		this.grabbedBall.showGrabbed();

		Player[] players = game.getPlayers();

		// Check if another player has the ball and give it away if it does
		foreach ( Player player in players ) {

			if ( player != this && player.getBall() == ball )
				player.giveAwayBall();

		}

		return true;

	}
	
	

	/* giveAwayBall()
	 * 
	 * The player doesn't have the ball
	 * 
	 */
	public void giveAwayBall() {

		if ( this.hasBall && this.grabbedBall != null ) {

			this.grabbedBall.hideGrabbed();
			this.grabbedBall.show();
		    this.grabbedBall.setGrabber( null );

		}

		this.hasBall     = false;
		this.grabbedBall = null;

	}



	/* ownBall()
	 * 
	 * Does the player has the ball?
	 * 
	 */
	public bool ownBall() {
		
		return this.hasBall;
		
	}



	/* getBall()
	 * 
	 * Return the Ball object owned by the player
	 * 
	 */
	public Ball getBall() {

		return this.grabbedBall;

	}



	/* void setMainPlayer( bool val )
	 * 
	 * Set this player as the main player or not
	 * 
	 */
	public void setMainPlayer( bool val ) {

		this.mainPlayer = val;
		
	}



	/* isMainPlayer()
	 * 
	 * Is the player the main player
	 * 
	 */
	public bool isMainPlayer() {
		
		return this.mainPlayer;
		
	}



	/* setHumain( bool val )
	 * 
	 * This player is a humain or not
	 * 
	 */
	public void setHumain( bool val ) {
		
		this.humain = val;
		
	}



	/* isHumain()
	 * 
	 * Is the player a humain or not
	 * 
	 */
	public bool isHumain() {
		
		return this.humain;
		
	}



	/* setPrisoner( bool val )
	 * 
	 * This player is in prison or not
	 * 
	 */
	public void setPrisoner( bool val ) {
		
		this.prisoner = val;
		
	}



	/* isPrisoner()
	 * 
	 * Is the player in prison
	 * 
	 */
	public bool isPrisoner() {
		
		return this.prisoner;
		
	}



	/* setHovering()
	 * 
	 * This player is in hovering mode
	 * 
	 */
	public void setHovering( bool val ) {
		
		this.hovering = val;
		
	}



	/* isHovering()
	 * 
	 * Is the player in hovering mode
	 * 
	 */
	public bool isHovering() {
		
		return this.hovering;
		
	}



	/* setDead()
	 * 
	 * This player is dead or not
	 * 
	 */
	public void setDead( bool val ) {
		
		this.dead = val;
		
	}



	/* isDead()
	 * 
	 * Is the player dead
	 * 
	 */
	public bool isDead() {
		
		return this.dead;
		
	}



	/* hits()
	 * 
	 * The player has been hit
	 * 
	 */
	public void hit() {
		
		this.hitsNum++;
		
	}



	/* getHits()
	 * 
	 * Return the number of time the player has been hit
	 * 
	 */
	public short getHits() {
		
		return this.hitsNum;
		
	}



	/* resetHits()
	 * 
	 * Reset the number of time the player has been hit to 0
	 * 
	 */
	public void resetHits() {
		
		this.hitsNum = 0;
		
	}



	/* targetHit()
	 * 
	 * The player has hit a target
	 * 
	 */
	public void targetHit() {
		
		this.targetHitsNum++;
		
	}
	
	
	
	/* getTargetHitsNumHits()
	 * 
	 * Return the number of time the player has hit a target
	 * 
	 */
	public short getTargetHits() {
		
		return this.targetHitsNum;
		
	}
	
	
	
	/* resetTargetHitsNumHits()
	 * 
	 * Reset the number of time the player has hit a target to 0
	 * 
	 */
	public void resetTargetHits() {
		
		this.targetHitsNum = 0;
		
	}



    /* getTarget()
     * 
     * Returns the players target Player object, null if not target
     * 
     */
    public Player getTarget() {

        return this.target;

    }



	/*  getTeam()
	 * 
     *  Returns the players team
     */
     public Team getTeam() {

		return this.game.getTeam( this.team ) ;

	}



	/* getTeamNo()
	 * 
	 * Return the player's team no
	 * 
	 */
	public int getTeamNo() {
		
		return this.team;
		
	}



    /* releaseTarget()
     * 
     * The player do not target anything
     * 
     */
    public void releaseTarget() {
        
        this.target = null;
        
    }



    /* setTarget( Player playerobj )
     * 
     * Set the players target Player object
     * 
     */
    public void setTarget( Player playerobj ) {
        
        this.target = playerobj;
        
    }


   
    /* getTargeters()
     * 
     * Return an array of Player object who target the current player
     * return null if there's no targeters
     * 
     */
    public Player[] getTargeters() {

        Player[] players        = this.game.getPlayers();
        Player[] testTargeters  = new Player[players.Length];
        Player[] targeters;
        int      targetersIndex = 0;

        // Search for targeters
        foreach ( Player player in players ) {

            if ( player != this && player.getTarget() == this ) {

                testTargeters[targetersIndex] = player;
                targetersIndex++;

            }

        }

        // No targeters
        if ( targetersIndex == 0 )
            return null;

        targeters = new Player[targetersIndex];

        // Cleanup the array before returning it
        for ( int i = 0; i < targetersIndex; ++i )
            targeters[i] = testTargeters[i];

        return targeters;

    }
    
    
    
     /* void setRotation( Quaternion rot )
     * 
     *  Change the rotation of the player
     * 
     */
	public void setRotation( Quaternion rot ) {

		this.gameobject.transform.rotation = rot;

	}    



	/* getPosition()
     * 
     * Return the position of the player
     * 
     */
	public Vector3 getPosition() {

		return this.gameobject.transform.position;

	}



	/* setPosition( Vector3 pos )
     * 
     * Change the position of the player
     * 
     */
	public void setPosition( Vector3 pos ) {
		
		this.gameobject.transform.position = pos;
		
	}

	

	/* setLocalPosition( Vector3 pos )
     * 
     * Change the local position of the player
     * 
     */
	public void setLocalPosition( Vector3 pos ) {
		
		this.gameobject.transform.localPosition = pos;
		
	}


    /* respawn()
     * 
     * set the init position of the player
     * 
     */
    public void respawn()
    {
        setPosition(this.playerSpawn);
        if(team ==1)
        this.headTo(getPosition() + new Vector3(0, 0,-5));
        else
        this.headTo(getPosition() + new Vector3(0, 0, 5));
    }

    /* setVelocity( Vector3 velocity )
     * 
     * Change the velocity of the player
     * 
     */
    public void setVelocity( Vector3 velocity ) {
		
		this.gameobject.GetComponent<Rigidbody>().velocity = velocity;
		
	}
	
	

	/* getVelocity()
     * 
     * Returns the velocity of the player
     * 
     */
	public Vector3 getVelocity() {
		
		return this.gameobject.GetComponent<Rigidbody>().velocity;
		
	}
	


	/* getSpeed()
     * 
     * Returns the speed of the player
     * 
     */
	public float getSpeed() {

		return this.gameobject.GetComponent<Rigidbody>().velocity.magnitude;
		
	}



	/* void setSpeed( float speed )
     * 
     * Change the speed of the player
     * 
     */
	public void setSpeed( float speed ) {

		// TODO: To change, I this it could simply be done like this
		// this.gameobject.GetComponent<Rigidbody>().velocity = this.gameobject.GetComponent<Rigidbody>().velocity.normalized * speed
		
		float multiplier = ( speed * speed ) / this.gameobject.GetComponent<Rigidbody>().velocity.magnitude;

		if ( multiplier == float.NaN || speed == 0.0f )
			multiplier = 0.0f;

		this.gameobject.GetComponent<Rigidbody>().velocity *= multiplier;
		
	}
	


	/* sendToJail()
     * 
     * Place the player into it's jail
     * 
     */
	public void sendToJail() {

		Vector3 jailPos = this.getTeam().getJailPosition();

		Rigidbody rb       = gameobject.transform.GetComponent<Rigidbody>();
		bool      origKine = rb.isKinematic;

		// Fit the position in the limits of the jail
		jailPos = game.fitByInPrison( this.getTeamNo(), jailPos, 0.01f );
		
		// Reset momentum
		rb.isKinematic     = true;

		this.setPrisoner( true );
		this.setPosition( jailPos );
		this.setVelocity( new Vector3( 0.0f, 0.0f, 0.0f ) );
		Vector3 origin = new Vector3 (0.0f, 0.0f, 0.0f);
		Quaternion rotation = Quaternion.LookRotation(origin - this.gameobject.transform.position);
		this.setRotation (rotation);

		rb.isKinematic     = origKine;

	}



	/* explodeAndSendToJail()
     * 
     * Show explosion halo and place the player into it's jail
     * 
     */
	public void explodeAndSendToJail() {

		if ( !this.isPrisoner() ) {

			GameObject haloObject = this.getGame().getGameConroller().haloObject;

			if ( haloObject == null )
				return; // haloObject not found

			// To reset momentum
			Rigidbody rb       = gameobject.transform.GetComponent<Rigidbody>();
			bool      origKine = rb.isKinematic;

			// Reset momentum
			rb.isKinematic = true;

			// The Player explosion halo
			GameObject gamehaloObject = GameController.Instantiate( haloObject );

			gamehaloObject.transform.position = this.getPosition();

			// The Jail halo
			GameObject gamehaloObjectJail = GameController.Instantiate( haloObject );
		
			gamehaloObjectJail.transform.position = this.getTeam().getJailPosition();

			// Send the player to jail
			this.sendToJail(); 

			rb.isKinematic = origKine;

			// Change the segment of the music to a darker one in Random segment mode if enabled				
			if ( isMainPlayer() && game.getGameConroller().getRandomSegmentsMode() )
				game.getGameConroller().audio().playSegmentsSoundtrack( 4 );

		}

	}



	/* explodeGetOutOfJail( Vector3 pos )
     * 
     * Show explosion halo and place the player outside of the jail
     * 
     */
	public void explodeGetOutOfJail( Vector3 pos ) {
		
		if ( this.isPrisoner() ) {
			
			GameObject haloObject = this.getGame().getGameConroller().haloObject;
			
			if ( haloObject == null )
				return; // haloObject not found
			
			// The Player explosion halo
			GameObject gamehaloObject = GameController.Instantiate( haloObject );
			
			gamehaloObject.transform.position = pos;
			
			// The Jail halo
			GameObject gamehaloObjectJail = GameController.Instantiate( haloObject );
			
			gamehaloObjectJail.transform.position = this.getPosition();
			
			// Send the player out of jail
			this.setPrisoner( false ); 

			Rigidbody rb       = this.getObject().transform.GetComponent<Rigidbody>();
			bool      origKine = rb.isKinematic;

			// Reset momentum
			rb.isKinematic = true;

			this.setPosition ( pos );
			
			// Look a the center of the game
			// TODO: the headto fonction show be moved to a mouvement class!
			this.headTo( new Vector3( 0.0f, game.getFieldSize().centerY(), game.getFieldSize().centerZ() ) );
			
			rb.isKinematic = origKine;

			// Change the segment of the music to a smoother one in Random segment mode if enabled				
			if ( isMainPlayer() && game.getGameConroller().getRandomSegmentsMode() )
				game.getGameConroller().audio().playSegmentsSoundtrack( 0 );
			
		}
		
	}



    /*reinitPlayer()
    *
    *Reset the Player attributes and position after each round
    *
    */
    public void reinitPlayer()
    {

		if ( ownBall() )
			giveAwayBall();

		healt         = 100.0f; // Player's healt of the player in %	
		hitsNum       = 0;      // Player's hits (the number of time the player has been hit)
		targetHitsNum = 0;      // Player's target hits (the number of time the player has hit an ennemy)
		hasBall       = false;  // Does this player has the ball
		prisoner      = false;  // Is this player in prison
		dead          = false;  // Is this player dead
		grabbedBall   = null;   // The Ball object if the player owns it
		target        = null;

		// Reset the Controller of the player
		Controller controller = gameobject.GetComponent<Controller>() as Controller;

		if ( controller != null )
			controller.reset();

        respawn();

    }



	// TODO: This fonction should be in a mouvement class!
	public void headTo( Vector3 position ) {

		Rigidbody rb          = gameobject.transform.GetComponent<Rigidbody>();
		Vector3   relativePos = position - gameobject.transform.position;
		bool      origKine    = rb.isKinematic;

		// Return if the heading target is not valid
		if ( relativePos == new Vector3 ( 0.0f, 0.0f, 0.0f ) )
			return;

		// Reset momentum
	    rb.isKinematic     = true;

		Quaternion rotation = Quaternion.LookRotation( relativePos );
		gameobject.transform.rotation = rotation;

		rb.isKinematic     = origKine;
		
	}



	/* hasLaser()
    *
    * Does the player has the laser
    *
    */
	public bool hasLaser() {

		return this.laser;

	}



	/* void setLaser( bool val )
    *
    * Give or not the laser to the player
    *
    */
	public void setLaser( bool val ) {

		this.laser = val;
		GameObject.FindGameObjectsWithTag( "InvisibleLaserText" )[0].GetComponent<GUIText> ().enabled = val;

		if ( isMainPlayer() )
			showLaserGun( val );

	}



	/* playSound( int sound )
	 * playSound( int sound, float level )
	 * playSound( int sound, float level, bool loop )
	 * 
	 * Play a sound
	 * 
	 */
	public void playSound( int sound ) {
		
		game.audio().playSound( sound, gameobject, 1.0f, false );
		
	}
	
	public void playSound( int sound, float level ) {
		
		game.audio().playSound( sound, gameobject, level, false );
		
	}
	
	public void playSound( int sound, float level, bool loop ) {
		
		game.audio().playSound( sound, gameobject, level, loop );
		
	}
	
	
	/* isPlaying()
	 * isPlaying( int sound )
	 * 
	 * Is playing a sound?
	 * 
	 */	
	public bool isPlaying() {
		
		return game.audio().isPlaying( gameobject );
		
	}
	
	public bool isPlaying( int sound ) {
		
		return game.audio().isPlaying( sound, gameobject );
		
	}



	/* stopPlaying( int sound )
	 * 
	 * Stop a sound
	 * 
	 */	
	public bool stopPlaying( int sound ) {
		
		return game.audio().stopSound( sound, gameobject );
		
	}



	/* setSoundVolume( int sound, float level )
	 * 
	 * Set the level of a sound
	 * 
	 */	
	public void setSoundVolume( int sound, float level ) {
		
		game.audio().setVolume( sound, gameobject, level );
		
	}
	
	
	
	/* setSoundPitch( int sound, float pitch ) 
	 * 
	 * Set the pitch of a sound
	 * 
	 */	
	public void setSoundPitch( int sound, float pitch ) {
		
		game.audio().setPitch( sound, gameobject, pitch );
		
	}



	/* showLaserGun( bool state )
	 * 
	 * Show the laser gun on the ship
	 * 
	 */	
	public void showLaserGun( bool state ) {

		// TODO: For the moment this fonction only works for the main player
		//       must fix this to support every ships later

		GameObject laserObj = GameObject.FindGameObjectWithTag( "LaserGun" );

		if ( laserObj != null ) {

			laserObj.GetComponent<MeshRenderer>().enabled = state;
			laserObj.GetComponent<Light>().enabled        = state;

		}

	}



	/* setLaserGunTexture( Texture2D texture ) 
	 * 
	 * Change the laser gun texture of a gun
	 * 
	 */	
	public void setLaserGunTexture( Material texture ) {

		// TODO: For the moment this fonction only works for the main player
		//       must fix this to support every ships later
		
		GameObject laserObj = GameObject.FindGameObjectWithTag( "LaserGun" );
		
		if ( laserObj != null && texture != null )
			laserObj.GetComponent<MeshRenderer>().material = texture;
		
	}

}
