﻿/* Team
 * 
 * constructor:
 * 
 *   Team( int no, int playersnum, Game game, Vector3 jail )
 * 
 * methods:
 * 
 *   Player[] getPlayers()
 *   Player   getPlayer( int no )
 *   int      getJailPlayersNum()
 *   Vector3  getJailPosition()
 *   int      getNo()
 * 
 */



using UnityEngine;
using System.Collections;

public class Team {

	private Player[] players;          // The players in the team
	private int      teamno;           // The index number of the team
	private int      playersnum = 2;   // Number of players in this team, 2 by default
	private Vector3  jailSpawn;
    private Vector3  playerSpawn;
	private Game     game;             // The game object that owns the team
	
	// Constructor
	public Team( int no, int playersnum, Game thegame ) {

		this.playersnum = playersnum;
		this.teamno     = no;
		this.game       = thegame;
		//this.jailSpawn  = jail;
        

        this.players = new Player[playersnum];

		// Players creation
		for ( int i = 0; i < playersnum; ++i ) {

			players[i] = new Player( no, i, playersnum * no + i, game );

		}

	}


	/* getPlayers()
	 * 
	 * Returns all players objects in this team
	 * 
	 */
	public Player[] getPlayers() {

		return players;

	}



	/* getPlayer( int no )
	 * 
	 * Returns the player of the team at index no
	 * 
	 */
	public Player getPlayer( int no ) {
		
		return players[no];
		
	}



	/* getJailPlayersNum()
	 * 
	 * Returns the number of the players in the team in jail
	 * 
	 */
	public int getJailPlayersNum() {

		int jailcounter = 0;

		for ( int i = 0; i < this.playersnum; ++i ) {

			if ( players[i].isPrisoner() )
				jailcounter++;

		}

		return jailcounter;

	}



	/*  getJailPosition()
	 * 
	 *  Returns this teams jail position 
	 */
	public Vector3 getJailPosition() {

		Game.Rect3D prison = game.getPrisonSize( teamno );

		return new Vector3( prison.centerX(), prison.centerY(), prison.centerZ() );
		//return this.jailSpawn;

	}



	/* getNo()
	 * 
	 * Return the team no
	 * 
	 */
	public int getNo() {
		
		return this.teamno;
		
	}


    /* reinitPlayers()
	 * 
	 * Reinit all the players after a round
	 * 
	 */
    public void reinitPlayers()
    {

        // Players creation
        for (int i = 0; i < playersnum; ++i)
        {

            players[i].reinitPlayer();

        }

    }
    /*generate Respawn
    *
    *Genere les points de départs de chaque vaisseau
    *
    */
    public void generateRespawn()
    {
        //Creation des respawn 
        for (int i = 0; i < playersnum; ++i)
        {
            players[i].setPlayerSpawn( game.generatePosInTeamSide(this.teamno) );
            //Positionne le vaisseau au respawn
            players[i].respawn();
        }

        
    }

}
