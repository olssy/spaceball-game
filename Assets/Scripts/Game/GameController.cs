﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
	
	public  GameObject      spaceshipPlayer;      // The main player
	public  GameObject      spaceshipTeam1;
	public  GameObject      spaceshipTeam2;
	public  GameObject      audioControllerObj;
	public  GameObject      menu;
	public  int             pointageRouge;
	public  int             pointageBleu;
	public  Game            game;
	public  GameObject[]    ballObjects           = new GameObject[40];
	public  GameObject[]    ballGrabbedObjects    = new GameObject[40];
	public  GameObject[]    blocs                 = new GameObject[500];
	public  GameObject      haloObject;
	public  bool            segmentsRandomSoundtrack = true;

	private AudioController audioController;
	private Blocs           blocsObject;
	private Round           round;
	private int             playerNumByTeam       = 2;
    private float           roundTime             = 100.0f;
	private int             roundNum              = 5;
	private int             difficulty            = 2;
	
	// GRAVITY ZERO CONFIGURATION
	private bool            gravityZeroEnabled    = true;
	private float           gravityZeroTimer      = 0.0f;
	private float           gravityZeroTime       = 10.0f; // In seconds
	private float           gravityZeroDuration   = 3.0f;  // In seconds
	private float           gravityZeroProb       = 0.2f;
	private float           gravityZeroTimerStart = 0.0f;

	private float           thresholdLowFPS       = 30.0f;
	private float           timeoutLowFPS         = 5.0f; // In seconds
	private int             lowFPSMax             = 10;
	private float           lowFPStime            = 0.0f;
	private int             lowFPSCount           = 0;

    //GUI Elements
	public GUIText          roundwinnertext;
    public GUIText          textePointageRouge;
    public GUIText          textePointageBleu;
    public GUIText          texteTimer;
    public GUIText          texteTimer321;
    public GUIText          texteRound;
    public GUIText          texteWinner;
    public GUIText          texteGameOver;
    public GameObject       scoreGeneralGUI;
	public GameObject       FPScounter;
	public GameObject       zeroGravityText;

    public bool             gameStarted = false;


	
    // Use this for initialization
    void Start() {

		GameObject.FindWithTag("Configs").GetComponent<Configs>().setQualityLevel( 2 );

		// Get the Audio Controller object
		this.audioController = audioControllerObj.GetComponent<AudioController>();

		if ( !this.audioController )
			Debug.LogError( "AudioController not found" );

	}



	/* AudioController audio()
	 * 
	 * Returns the AudioController of the game
	 * 
	 */
	public AudioController audio() {
		
		return this.audioController;
		
	}



    public void startGame() {	

        pointageRouge = 0;
        pointageBleu  = 0;
        RafraichirPointage("bleu");
        RafraichirPointage("rouge");

        // Read the number of players from the configuration
        playerNumByTeam = GameObject.FindWithTag("Configs").GetComponent<Configs>().getPlayersByTeam();

        // Read the number of round time from the config 
        this.roundTime  = GameObject.FindWithTag("Configs").GetComponent<Configs>().getRoundTime();

		// Number of rounds
		this.roundNum   = GameObject.FindWithTag("Configs").GetComponent<Configs>().getRoundNum();
		
		// Difficulty
		this.difficulty = GameObject.FindWithTag("Configs").GetComponent<Configs>().getDifficulty();

		// Create a new game
	    game = createNewGame();

		// Blocs reinitalisation
		reinitBlocs();

		// Hide the menu
		menu.SetActive( false );

		this.gameStarted = true;

		// Play the background music
		if ( !segmentsRandomSoundtrack )
			audio().playRandomSoundtrack();

		// audio().playSoundtrack( 0 );

		// Start the ambient background sound
		audio().playSound( AudioController.AUDIO_AMBIENT_BACKGROUND, null, 1.0f, true );

		// Start the ambient electromagnetic sound
		audio().playSound( AudioController.AUDIO_FRONTIER_SOUND, GameObject.FindWithTag("ElectroFrontier"), 1.0f, true );

		// TODO: Start the music here

		// Show the countdown
        StartCoroutine( secondCountDown() );

    }



	// FixedUpdate is called at a fixed time
	void FixedUpdate() {

		if ( segmentsRandomSoundtrack )
			audio().playSegmentsSoundtrack();

		// Check FPS if auto-quality adjust on low FPS is enabled
		if ( game != null && game.getConfigs().getQualityAdjustFPS() )
			checkFPSstate();

        if (this.gameStarted) { 

		    if ( !game.isOver() ) {

			    if ( !round.isOver() ) {

				    if ( round.isStarted() ) {

                        RafraichirTimer(round.getTimeout());

						// Play the prison background sound
						if ( game.getMainPlayer().isPrisoner() ) {

							if ( !game.isPlaying( AudioController.AUDIO_PRISON_SOUND ) )
								game.playSound( AudioController.AUDIO_PRISON_SOUND, 1.0f, true );

						} else {

							if ( game.isPlaying( AudioController.AUDIO_PRISON_SOUND ) )
								game.stopPlaying( AudioController.AUDIO_PRISON_SOUND );

						}

					    // GRAVITY ZERO
					    gravityZeroTimer = Time.fixedTime - gravityZeroTimerStart;

					    if ( gravityZeroEnabled && !game.getZeroGravityMode() && gravityZeroTimer >= gravityZeroTime && Random.value < gravityZeroProb ) {

						    game.setZeroGravityMode( true );  // ON
							//game.playSound( AudioController.AUDIO_ZERO_GRAVITY_ON );
							//game.playSound( AudioController.AUDIO_ZERO_GRAVITY_SOUND, 1.0f, true );
						    gravityZeroTimerStart = Time.fixedTime;

					    }

					    if ( gravityZeroEnabled && game.getZeroGravityMode() && gravityZeroTimer >= gravityZeroDuration && Random.value < gravityZeroProb ) {
						
						    game.setZeroGravityMode( false );  // OFF
							//game.playSound( AudioController.AUDIO_ZERO_GRAVITY_OFF );
							//game.stopPlaying( AudioController.AUDIO_ZERO_GRAVITY_SOUND );
						    gravityZeroTimerStart = Time.fixedTime;

					    }				

					    if ( gravityZeroTimer >= gravityZeroTime && !game.getZeroGravityMode() )
						    gravityZeroTimerStart = Time.fixedTime;

					    if ( gravityZeroTimer >= gravityZeroDuration && game.getZeroGravityMode() )
						    gravityZeroTimerStart = Time.fixedTime;

				    }

			    } else { // The round is over

				    // Stop the current round
				    round.stop();

					// Just in case stop all looping sounds
					audioController.stopAllLoops();

					game.playSound( AudioController.AUDIO_END_ROUND );

                    //Ajout du score général
                    if (round.getScore(0) > round.getScore(1)) {

						roundwinnertext.text = "RED\nWINS";
                        game.incScore(0, 1);

					} else if (round.getScore(0) == round.getScore(1)){

						roundwinnertext.text = "EQUALITY";
                        game.incScore(0, 1);
                        game.incScore(1, 1);

                    } else {

						roundwinnertext.text = "BLUE\nWINS";
                        game.incScore(1, 1);
                    
					}

                    RafraichirScore( game.getScore(0), game.getScore(1) );

                    //On reinitialise les pointages du round à afficher 
                    this.pointageBleu  = 0;
                    this.pointageRouge = 0;

                    RafraichirPointage("rouge");
                    RafraichirPointage("bleu");

				    // Pass to the next round
				    if ( game.nextRound() != null ) {

					    // Pause the game
					    Time.timeScale = 0;

					    // Get the current round
					    round = game.getRound();

					    // Turn of the Zero Gravity Mode
					    game.setZeroGravityMode( false );

                        //TODO: Can show the result of the round before going to the next round

					    // Reset positions, hits, etc...
					    reinitGame();

					    // Blocs
						reinitBlocs();

                        //3.2.1  coroutine
                        StartCoroutine( secondCountDown() );

                        // Unpause the game
                        Time.timeScale = 1;

                    }

			    }

		    } else { // Game over

                //Ajout du score général du dernier round
                if (round.getScore(0) > round.getScore(1))
                    game.incScore(0, 1);
                else if (round.getScore(0) == round.getScore(1))
                {
                    game.incScore(0, 1);
                    game.incScore(1, 1);
                }
                else
                    game.incScore(1, 1);
                RafraichirScore(game.getScore(0), game.getScore(1));

                gameStarted        = false;	
                texteGameOver.text = "GAME OVER";



                if ( game.getScore(0) > game.getScore(1) ) {

					game.playSound( AudioController.AUDIO_GAMEOVER_WIN );
                    RafraichirWinner(0);

				} else if ( game.getScore(0) == game.getScore(1) ) {

					game.playSound( AudioController.AUDIO_GAMEOVER_EQUALITY );
                    RafraichirWinner(-1);

				} else {

					game.playSound( AudioController.AUDIO_GAMEOVER_LOST );
                    RafraichirWinner(1);                

				}

				// IMPORTANT, must stop the game to destroy all the game's GameObjects before creating a new one
				game.stop();

				// Stop the background music
				audio().stopSoundtrack();

				// Just in case stop all looping sounds
				audioController.stopAllLoops();

				// TODO: Can show the result or the menu before going to the next game

				// Show the menu
				StartCoroutine( showMenu() );
			
		    }

        }

    }



	public Game createNewGame() {

		Game newgame;
		
		newgame = new Game( playerNumByTeam, roundNum, roundTime, difficulty );

		// Set the size of the field of the game
		newgame.setFieldSize( -14.5f, 14.5f, -19.0f, 19.0f, 0.0f, 10.36f );

        // Set the sizes of the prison cells (ajusted)
		newgame.setPrisonLimits( 0, -3.6f, 3.6f,  15.5f,  17.2f, 5.4f, 6.0f );
		newgame.setPrisonLimits( 1, -3.6f, 3.6f, -17.2f, -15.5f, 5.4f, 6.0f );

		// Real prison sizes
		newgame.setPrisonSize( 0, -4.2f, 4.2f,  15.0f, 18.0f, 4.85f, 7.15f );
		newgame.setPrisonSize( 1, -4.2f, 4.2f, -18.0f, -15.0f, 4.85f, 7.15f );

		newgame.attachObjects( spaceshipPlayer, spaceshipTeam1, spaceshipTeam2 );
		
		// The main player is a humain
		newgame.getMainPlayer().setHumain( true );	
		
		// Attach the main camera to the main player
		CameraScript cameraScript = GameObject.FindWithTag( "MainCamera" ).GetComponent<CameraScript>();
		cameraScript.target       = newgame.getMainPlayer().getObject().transform;
			 
		// Get the current round
		round = newgame.getRound();

        //Genere des positions de depart pour chaque vaisseau
        newgame.generateRespawnPos();

		if ( gravityZeroEnabled )
			newgame.setZeroGravityMode( false );

		gravityZeroTimerStart = Time.fixedTime;

		return newgame;

	}



    public void reinitGame() {

		if ( game != null ) {

			game.reinitAll();
			game.destroyBalls();

		}

    }



	public void reinitBlocs() {

		if ( game == null || blocs == null || blocs.Length == 0 )
			return;
		
		//Ajout des murs
		if ( blocsObject == null )
			blocsObject = new Blocs();
		else
			blocsObject.destroyBlocs(); //On supprime les murs
		
		//On recréer les murs
		blocsObject.setBlocsKinematic(true);
		blocsObject.drawRandomBlocsWall(480, blocs, game);
		blocsObject.setBlocsKinematic(false);
		
	}



    //CoRoutine pour le 3.2.1 go
    IEnumerator secondCountDown()
    {

        this.RafraichirTimer(roundTime);
        this.RafraichirRound("Round " + (game.getRoundNum() + 1));

        int timeToWait = 3;
        int incrementToRemove = 1;
        while (timeToWait > 0)
        {
            this.RafraichirTimer321(timeToWait);
			game.playSound( AudioController.AUDIO_COUNTDOWN );
            yield return new WaitForSeconds(incrementToRemove);
            timeToWait -= incrementToRemove;

        }

        this.RafraichirTimer321("");
        this.RafraichirRound("");

        //On reeatribut des positions aléatoire au ball et on enleve leurs vitesse
        Game.Rect3D rect = game.getFieldSize();

        for (int i = 0; i < game.getConfigs().getBallsNum(); ++i)
        {

            game.addBall((int)Mathf.Floor(Random.Range(0.0f, 1.99f)), new Vector3(Random.Range(rect.xMin() + 1.0f, rect.xMax() - 1.0f), rect.yMax()-2.0f, Random.Range(rect.zMin() + 5.0f, rect.zMax() - 5.0f)), 1.0f, 1);

        }

		// Erase the round result
		roundwinnertext.text = "";

        if (!game.isStarted())
        {
            // Start the game
            game.start();
			game.playSound( AudioController.AUDIO_START_GAME );

        }
        else
        {
            // Start the current round
            round.start();
			game.playSound( AudioController.AUDIO_START_ROUND );

        }
    }

    public void AjouterPointage (int points, string couleur)
	{
		if(couleur == "rouge"){
			round.incScore( 0, points );
			// game.incScore( 0, points );
			pointageRouge += points;
		} else if(couleur == "bleu"){
			round.incScore( 1, points );
			// game.incScore( 1, points );
			pointageBleu += points;
		}
		RafraichirPointage(couleur);
	}


	
	void RafraichirPointage (string couleur)
	{
		if (couleur=="rouge"){
			textePointageRouge.text = pointageRouge.ToString();
		} 
		else if(couleur=="bleu"){
			textePointageBleu.text = pointageBleu.ToString();
		}
	}



    void RafraichirTimer(float timeout)
    {

        if(timeout >= 10) {
            int time = (int)timeout;
            texteTimer.text = time.ToString();
			texteTimer.color = new Color ( 1, 1, 1, 0.7f );
        }
        else
        {
            int time = (int)timeout;
            texteTimer.text = time.ToString();
			texteTimer.color = new Color ( 1, 0, 0, 0.7f );
        }

    }



    void RafraichirTimer321(int timeout)
    {

        texteTimer321.text = timeout.ToString();

    }



    void RafraichirTimer321(string str)
    {

        texteTimer321.text = str;

    }



    void RafraichirScore(int rouge, int bleu)
    {

        this.scoreGeneralGUI.GetComponent<ScoreGUI>().setScoreRouge(rouge);
        this.scoreGeneralGUI.GetComponent<ScoreGUI>().setScoreBleu(bleu);

    }



    void RafraichirRound(string str)
    {

        texteRound.text = str;

    }



    void RafraichirWinner(int teamGagnant)
    {

        if(teamGagnant == 0)
        {
            texteWinner.text  = "RED TEAM WON THE GAME!";
			texteWinner.color = new Color ( 1, 0, 0, 0.7f );

        }else if(teamGagnant == 1){
            texteWinner.text  = "BLUE TEAM WON THE GAME!";
			texteWinner.color = new Color ( 0, 0, 1, 0.7f );
        }
        else if(teamGagnant == -1){
            texteWinner.text  = "EQUALITY!";
			texteWinner.color = new Color ( 0, 1, 0, 0.7f );
        }

    }



    public Blocs getBlocs() {

		return this.blocsObject;

	}



	public Configs getConfigs() {

		return GameObject.FindWithTag("Configs").GetComponent<Configs>();

	}



	public IEnumerator showMenu() {

		yield return new WaitForSeconds( 10 );

		Cursor.visible     = true;
		Screen.lockCursor  = false;	
		texteGameOver.text = "";
		texteWinner.text   = "";

		// Erase the round result
		roundwinnertext.text = "";
		
        Application.LoadLevel("spaceball");

    }



	private void checkFPSstate() {

		// Check for low framerate
		if ( thresholdLowFPS > game.getConfigs().getCurrentFPS() ) {

			lowFPSCount++;

		}

		// Low framerate detected?
		if ( lowFPSCount > lowFPSMax ) {

			if ( game != null && game.getConfigs().getQualityAdjustFPS() ) {

				// Debug.Log( "Low framerate : " + game.getConfigs().getCurrentFPS() + " FPS" );

				int quality = game.getConfigs().getCurrentQualityLevel();

				// Downgrade the quality
				if ( quality > 1 ) {

					game.getConfigs().setCurrentQualityLevel( quality - 1 );

					// Show a message to the player
					showMessage( "LOW FRAMERATE DETECTED! DOWNGRADING QUALITY" );

				}

				lowFPStime  = Time.fixedTime;
				lowFPSCount = 0;

			}

		}

		// Reset the timeout
		if ( game != null && ( Time.fixedTime - lowFPStime ) > timeoutLowFPS ) {

			lowFPStime  = Time.fixedTime;
			lowFPSCount = 0;

		}

	}



	/* showMessage( string text, Color color, float time ) 
	 * 
	 * Show a message to the player
	 * 
	 */	
	public void showMessage( string text, Color color, int time ) {
		
		GameObject messageObj = GameObject.FindWithTag("Message");
		
		if ( messageObj == null )
			return;
		
		GUIText messageText = messageObj.GetComponent<GUIText>();
		
		if ( messageText == null || game == null )
			return;

		// play a sound
		game.playSound( AudioController.AUDIO_MESSAGE );
		
		messageText.text  = text;
		messageText.color = color;
		
		// Hide the message in time secondes
		StartCoroutine( hideMessage( time ) );
		
	}
	
	public void showMessage( string text, int time ) {
		
		showMessage( text, new Color( 1.0f, 0.0f, 0.0f, 0.6f ), time );
		
	}
	
	public void showMessage( string text ) {
		
		showMessage( text, new Color( 1.0f, 0.0f, 0.0f, 0.6f ), 10 );
		
	}
	
	
	
	/* IEnumerator hideMessage( int time )
	 * 
	 * Hide the message in time secondes
	 * 
	 */	
	private IEnumerator hideMessage( int time ) {
		
		yield return new WaitForSeconds( time );
		
		GameObject messageObj = GameObject.FindWithTag("Message");
		
		if ( messageObj == null )
			yield return false;
		
		GUIText messageText = messageObj.GetComponent<GUIText>();
		
		if ( messageText == null )
			yield return false;
		
		messageText.text = "";
		
	}


	/* Level level()
	 * 
	 * Returns the level object
	 * 
	 */	
	public Level level() {

		if ( GameObject.FindWithTag ("Level") == null )
			return null;

		 
		return GameObject.FindWithTag ("Level").GetComponent<Level>();

	}



	/* getRandomSegmentsMode()
	 * 
	 * Is the music in random segments mode?
	 * 
	 */	
	public bool getRandomSegmentsMode() {

		return segmentsRandomSoundtrack;

	}



	/* setRandomSegmentsMode( bool mode )
	 * 
	 * Is the music in random segments mode?
	 * 
	 */	
	public void setRandomSegmentsMode( bool mode ) {
		
		segmentsRandomSoundtrack = mode;

	}

}
