﻿/* Game
 * 
 * constructors:
 * 
 *    Game( int teamplayernum )
 *    Game( int teamplayernum, int rndNum, float roundTime )
 *    Game( int teamplayernum, int rndNum, float roundTime, short difficultyval )
 * 
 * 
 * methods:
 * 
 *   AudioController audio()
 *   GameController getGameConroller()
 *   Configs        getConfigs()
 *   void           destroyBalls()
 *   Team           getTeam( int no )
 *	 int            getPlayersByTeamNum()
 *   int            getPlayersNum()
 *   void           start()
 *   bool           isStarted()
 *   void           stop()
 *   void           destroyGameObjects()
 *   Round          getRound()
 *   int            getRoundNum()
 *   bool           setRound( int roundnum )
 *   void           startRound()
 *   void           pauseRound() (NOT IMPLEMENTED IN Round)
 *   void           stopRound()
 *   void           attachObjects( GameObject mainplayer, GameObject team1, GameObject team2 )
 *   Round          nextRound()
 *   bool           isOver()
 *   Player[]       getPlayers()
 *   void           setScore( int no, int score )
 *   int            getScore( int no, int score )
 *   void           incScore( int no, int incVal )
 *   void           decScore( int no, int incVal )
 *   void           resetScores()
 *   void           giveBall( Player player, Ball ball )
 *   void           giveBall( int playerno, Ball ball )
 *   void           freeBall()
 *   bool           isBallFree( Ball ball )
 *   Ball           addBall( int type, Vector3 pos, float hitdamage, int speed )
 *   bool           removeBall( Ball ball )
 *   Player[]       whoOwnsBall()
 *   Player         whoOwnsBall( Ball ball )
 *   Ball[]         getBalls()
 *   Ball           getBallFromGameObject( GameObject obj )
 *   void           setMainPlayer( Player player )
 *   Player         getMainPlayer()
 *   void           setPrisonField( int prisonno, bool active )
 *   void           setMagneticField( bool active )
 *   bool           getPrisonField( int prisonno )
 *   bool           getMagneticField()
 *   short          getDifficulty()
 *   void           setDifficulty( short newdifficulty )
 *   void           setMinBallHitSpeed( float val )
 *   float          getMinBallHitSpeed()
 *   Rect3D         getFieldSize()
 *   Rect3D         getPrisonSize( int no )
 *   Rect3D         getPrisonLimits( int no )
 *   void           setFieldSize( float left, float right, float front, float back, float bottom, float top )
 *   void           setPrisonSize( int no, float left, float right, float front, float back, float bottom, float top )
 *   void           setPrisonLimits( int no, float left, float right, float front, float back, float bottom, float top )
 *   bool           posInPrison( int no, Vector3 pos )
 *   Vector3        fitByInPrison( int no, Vector3 pos, float offset )
 *   int            getPositionFieldSide( Vector3 pos ) 
 *   float          getDistance( Vector3 pos1, Vector3 pos2 )
 *   void           generateRespawnPos()
 *   void           reinitAll()
 *   void           setZeroGravityMode( bool state )
 *   bool           getZeroGravityMode()
 *   void           setFirstPersonView( bool state )
 *   bool           getFirstPersonView()
 *   void           setBezierPaths( bool val )
 *   bool           getBezierPaths()
 *   void           playSound( int sound )
 *   void           playSound( int sound, float level )
 *   void           playSound( int sound, float level, bool loop )
 *   bool           isPlaying()
 *   bool           isPlaying( int sound )
 *   bool           stopPlaying( int sound )
 *   void           setSoundVolume( int sound, float level )
 *   void           setSoundPitch( int sound, float pitch )
 * 
 */



using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class Game {

	private GameController  controller;
	private Configs         configs;

	private Team[]          teams;                            // The teams (2 teams)
	private Round[]         rounds;                           // The teams (2 teams)
	private int             playersnum          = 4;          // Total number of players
    private int             difficulty          = 2;          // The difficulty of the game from 0 to 10
    private int[]           scores              = new int[2]; // A score for each team for the game
	private int             currentRound        = 0;          // The index of the current round
	private int             roundsNum           = 0;          // Number of rounds in the game
    private GameObject      spaceshipPlayer;                  // Spaceship of the main player
	private GameObject      spaceshipTeam1;                   // Spaceships of team 0
    private GameObject      spaceshipTeam2;                   // Spaceships of team 1
	private Player          mainPlayer;
    private bool            magneticFieldActive = true;
    private bool[]          prisonFieldActive   = new bool[2];
    private short           maxBallsNum         = 40;         // Max number of balls at the same time
    private short           ballsNum            = 1;          // The number of ball(s) on the field
    private Ball[]          balls;                            // An array of the balls on the field
	private float           minBallHitSpeed     = 8.0f;       // Minimal speed of a ball count as a hit
	private bool            isGameStarted       = false;
	private Rect3D          fieldSize           = new Rect3D();
	private Rect3D[]        prisonSize          = new Rect3D[2];
	private Rect3D[]        prisonLimits        = new Rect3D[2];
	private bool            zeroGravityMode     = false;
	private bool            firtPersonView      = false;
	private bool            showBezierPath      = false;

	public class Rect3D {

		public float left;
		public float right;
		public float front;
		public float back;
		public float bottom;
		public float top;

		public float xMin() { return left; }						
		public float xMax() { return right; }						
		public float yMin() { return bottom; }						
		public float yMax() { return top;	}						
		public float zMin() { return front; }
		public float zMax() { return back; }
		public float centerX() { return ( ( right - left ) / 2 + left ); }
		public float centerY() { return ( ( top - bottom ) / 2 + bottom ); }
		public float centerZ() { return ( ( back - front ) / 2 + front ); }

	}



	// Constructor
	public Game( int teamplayernum, int rndNum, float roundTime ) {

		// GameController lookup
		GameObject gameControllerObject = GameObject.FindWithTag ( "GameController" );
		
		if ( gameControllerObject != null) {
			
			this.controller = gameControllerObject.GetComponent <GameController>();

			if ( this.controller == null) {

				Debug.LogError( "Cannot find 'GameController' script" );
				return;

			}


		}

		// Configs lookup
		GameObject configsObject = GameObject.FindWithTag ( "Configs" );
		
		if ( configsObject != null) {
			
			this.configs = configsObject.GetComponent <Configs>();
			
			if ( this.configs == null) {

				Debug.LogError( "Cannot find 'Configs' script" );
				return;
			
			}
			
		}

		this.playersnum  = teamplayernum * 2;  // Total number of players = 2 times the number of players by team
		this.teams       = new Team[2];
		this.rounds      = new Round[rndNum];  // Create the rounds objects
		this.roundsNum   = rndNum;

		// Teams initialisation
		teams[0] = new Team( 0, teamplayernum, this );
		teams[1] = new Team( 1, teamplayernum, this );

		// Rounds initialisation
		this.currentRound = 0;
		this.roundsNum    = rndNum;
		for ( int i = 0; i < rndNum; ++i ) {

			this.rounds[i] = new Round( roundTime, this );

		}

		// By default main player is player 0
		setMainPlayer( teams[0].getPlayer( 0 ) );

        // Set the defaut states of the magnetic fields
        this.magneticFieldActive  = true;
        this.prisonFieldActive[0] = true; // Prison 0
        this.prisonFieldActive[1] = true; // Prison 1

        // Create the balls array       
        this.balls = new Ball[maxBallsNum];

		// Create the dimension objects of the prison cells
		prisonSize[0]   = new Rect3D();
		prisonSize[1]   = new Rect3D();
		prisonLimits[0] = new Rect3D();
		prisonLimits[1] = new Rect3D();

	}




    // Constructor
    public Game( int teamplayernum, int rndNum, float roundTime, int difficultyval ) : this( teamplayernum, rndNum, roundTime ) {

        this.difficulty = difficultyval;

    }



    // Constructor (1 round of 3 minutes with difficulty 5)
    public Game( int teamplayernum ) : this( teamplayernum, 1, 180.0f, 5 ) {
        
    }



	/* AudioController audio()
	 * 
	 * Returns the AudioController of the game
	 * 
	 */
	public AudioController audio() {

		return this.controller.audio();

	}



	/* getGameConroller()
	 * 
	 * Returns the game controller of the game
	 * 
	 */
	public GameController getGameConroller() {

		return this.controller;
		
	}



	/* getConfigs()
	 * 
	 * Returns the configurations of the game
	 * 
	 */
	public Configs getConfigs() {
		
		return this.configs;
		
	}



	/* destroyBalls()
	 * 
	 * Destroy all balls
	 * 
	 */
	public void destroyBalls() {

		for ( int i = 0; i < this.ballsNum; ++ i ) {

			if ( this.balls[i] != null ) {

				Object.Destroy( this.balls[i].getBallObj() );
				this.balls[i] = null;

			}

		}

        // Create a new balls array       
	    this.balls = new Ball[maxBallsNum];
		
		this.ballsNum = 0;
		
	}



	/* getTeam( int no )
	 * 
	 * get the Team object of the team no
	 * 
	 */
	public Team getTeam( int no ) {
		
		return this.teams[no];
		
	}



	/* int getPlayersByTeamNum()
	 * 
	 * Returns the number of players by team
	 * 
	 */ 
	public int getPlayersByTeamNum() {

		return this.playersnum / 2;

	}



	/* int getPlayersNum()
	 * 
	 * Returns the number of players in the game
	 * 
	 */ 
	public int getPlayersNum() {
		
		return this.playersnum;
		
	}



	/* start()
	 * 
	 * start the game
	 * 
	 */
	public void start() {

		setRound( 0 );


		
		this.rounds[0].start();

		this.isGameStarted = true;

	}



	/* isStarted()
	 * 
	 * is the game started
	 * 
	 */
	public bool isStarted() {

		// Stop the game if it's over
		if ( isOver() )
			stop();

		return this.isGameStarted;
		
	}



	/* stop()
	 * 
	 * stop the game
	 * 
	 */
	public void stop() {

		getRound().stop();

		//setRound( 0 );
		this.currentRound = 0;

		this.isGameStarted = false;

		destroyGameObjects();
		
	}



	/* destroyGameObjects()
	 * 
	 * Destroy all the game's GameObjects
	 * 
	 */
	public void destroyGameObjects() {

		// Destroy the teams objects
		for ( int i = 0; i < 2; ++i ) {

			// Destroy the players objects
			for ( int j = 0; j < ( this.playersnum / 2 ); ++j ) {

				if ( teams[i].getPlayer( j ) != null )
					MonoBehaviour.Destroy( teams[i].getPlayer( j ).getObject() );

			}

			// Destroy the team
			teams[i] = null;

		}

		// Destroy the balls
		for ( int i = 0; i < this.ballsNum; ++i ) {

			if ( balls[i] != null ) {

				MonoBehaviour.Destroy( balls[i].getBallObj() );
				MonoBehaviour.Destroy( balls[i].getGrabbedBallObj() );

			}

		}
		
	}



	/* getRound()
	 * 
	 * return the current round object
	 * 
	 */
	public Round getRound() {

		if ( this.rounds != null && this.rounds.Length != 0 )
			return this.rounds[this.currentRound];
		else
			return null;

	}



	/* getRoundNum()
	 * 
	 * return the current round number (starts at 0)
	 * 
	 */
	public int getRoundNum() {
		
		return this.currentRound;
		
	}



    /* setRound( int roundnum )()
     * 
     * switch to round roundum, returns false in case of error
     * 
     */
    public bool setRound( int roundnum ) {

        if ( roundnum >= 0 && roundnum < roundsNum ) {

			// Stop the current round
			rounds[currentRound].stop();

            this.currentRound = roundnum;

            return true;

        }

        return false; // Error, wrong round number
        
    }



    /* startRound()
     * 
     * start the current round
     * 
     */
    public void startRound() {
        
        this.rounds[this.currentRound].start();
        
    }


    /* pauseRound()
     * 
     * pause the current round
     * 
     */
    public void pauseRound() {
        
        this.rounds[this.currentRound].pause();
        
    }



    /* stop()
     * 
     * stop the current round
     * 
     */
    public void stopRound() {
        
        this.rounds[this.currentRound].stop();
        
    }

	

	/* attachObjects( GameObject mainplayer, GameObject team1, GameObject team2 )
     * 
     * Attach the GameObjects of the players's spaceships (main, team1 and team2) to the game
     * 
     */
	public void attachObjects( GameObject mainplayer, GameObject team1, GameObject team2 ) {

		this.spaceshipPlayer = mainplayer;
		this.spaceshipTeam1  = team1;
		this.spaceshipTeam2  = team2;
		Player[] players     = getPlayers();

		// Create players of team 1
		for ( int i = 0; i < ( this.playersnum / 2 ); ++i ) {

			// Is the player the main player?
			if ( players[i].isMainPlayer() ) {

				GameObject newplayer = GameController.Instantiate( this.spaceshipPlayer );
				players[i].attachObject( newplayer );

			} else { // No, it's a teammate

				GameObject newplayer = GameController.Instantiate( this.spaceshipTeam1 );
				players[i].attachObject( newplayer );

			}
			
		}

		// Create players of team 2
		for ( int i = ( this.playersnum / 2 ); i < this.playersnum; ++i ) {
			
			GameObject newplayer = GameController.Instantiate( this.spaceshipTeam2 );
			players[i].attachObject( newplayer );
			
		}

	}



	/* nextRound(
	 * 
	 * Start the next round of the game
	 * 
	 * Returns the round object if there's a next round, else null
	 * 
	 */
	public Round nextRound() {
		
		if ( this.currentRound < ( this.roundsNum - 1 ) ) {

			// Stop the current round
			this.rounds[this.currentRound].stop();

			// Next round
			this.currentRound += 1;

			return this.rounds[this.currentRound];

		}
		
		return null; // No more round
		
	}



	/* isOver()
	 * 
	 * is the game over
	 * 
	 */
	public bool isOver() {

		if ( ( this.currentRound == ( this.roundsNum - 1 ) ) && getRound().isOver() )
			return true;
		else
			return false;

	}



	/* getPlayers()
	 * 
	 * get all players objects
	 * 
	 */
	public Player[] getPlayers() {
		
		Player[] gamePlayers = new Player[playersnum];
		Player   currentPlayer;

		/*  TODO: Why did I get this error!?

			NullReferenceException: Object reference not set to an instance of an object
			Game.getPlayers () (at Assets/Scripts/Game/Game.cs:625)
			Game.giveBall (Int32 playerno, .Ball ball) (at Assets/Scripts/Game/Game.cs:731)
			Game.giveBall (.Player player, .Ball ball) (at Assets/Scripts/Game/Game.cs:717)
		    BallGrabberScript.OnTriggerEnter (UnityEngine.Collider other) (at Assets/Scripts/Player/BallGrabberScript.cs:20)
		*/

		// Get players for each team
		for ( int i = 0; i < 2; ++i ) {

			for ( int j = 0; j < ( this.playersnum / 2 ); ++j ) {

				currentPlayer = teams[i].getPlayer(j);
                gamePlayers[currentPlayer.getGameIndex()] = currentPlayer;

			}

		}

		return gamePlayers;
		
	}



	/* setScore( int team, int score )
	 * 
	 * Set the team no's score
	 * 
	 */ 
	public void setScore( int no, int score ) {

		if ( no >= 0 && no < 2 )		
			this.scores[no] = score;
		
	}



	/* getScore( int team, int score )
	 * 
	 * Get the team no's score
	 * 
	 */ 
	public int getScore( int no) {
		
		if ( no >= 0 && no < 2 )		
			return this.scores[no];
		else 
			return -1; // Index no error
		
	}



	/* incScore( int no, int incVal )
	 * 
	 * Increment by inval the score of team no
	 * 
	 */ 
	public void incScore( int no, int incVal ) {
		
		if ( no >= 0 && no < 2 )		
			this.scores[no] += incVal;
		
	}



	/* decScore( int no, int incVal )
	 * 
	 * Decrement by inval the score of team no
	 * 
	 */ 
	public void decScore( int no, int incVal ) {
		
		if ( no >= 0 && no < 2 )		
			this.scores[no] = Mathf.Min( 0, this.scores[no] - incVal );
		
	}



    /* resetScores()
     * 
     * Set all teams scores to 0
     * 
     */ 
    public void resetScores() {
        
        this.scores[0] = 0;       
        this.scores[1] = 0;
        
    }



	/* giveBall( Player player )
	 * 
	 * player has the ball
	 * 
	 */ 
	public void giveBall( Player player, Ball ball ) {

		giveBall( player.getGameIndex(), ball );

	}



	/* giveBall( int playerno )
	 * 
	 * player at index playerno has the ball
	 * 
	 */ 
	public void giveBall( int playerno, Ball ball ) {

		if ( ball != null )
			getPlayers()[playerno].giveBall( ball );
		
	}

    

	/* freeBall()
	 * 
	 * No players has the ball
	 * 
	 */ 
	public void freeBall() {
		
		Player currentPlayer;
		
		for ( int i = 0; i < 2; ++i ) {
			
			for ( int j = 0; j < ( this.playersnum / 2 ); ++j ) {
				
				currentPlayer = teams[i].getPlayer(j);

    			currentPlayer.giveAwayBall();
				
			}
			
		}
		
	}



    /* isBallFree()
     * 
     * Returns true if no player has a specific ball
     * 
     */ 
    public bool isBallFree( Ball ball ) {
        
		if ( whoOwnsBall( ball ) == null )
            return true;  // The ball is free
        else
            return false; // The ball is not free

    }



	/* addBall( int type, Vector3 pos, float hitdamage, int speed )
     * 
     * Add a new ball to the Field
     * Returns the new ball, null if no mode balls space
     * 
     */ 
	public Ball addBall( int type, Vector3 pos, float hitdamage, int speed ) {

		for ( int i = 0; i < maxBallsNum; ++i ) {

			if ( balls[i] == null ) {

				balls[i] = new Ball( type, pos, hitdamage, speed );

				this.ballsNum++;

				return balls[i];

			}

		}

		return null;
		
	}



	/* removeBall( Ball ball )
     * 
     * Remove the ball from the field
     * Returns true if ball existe, false otherwise
     * 
     */ 
	public bool removeBall( Ball ball ) {

		for ( int i = 0; i < maxBallsNum; ++i ) {
			
			if ( balls[i] == ball ) {

				GameController.Destroy( balls[i].getBallObj() );

				balls[i] = null;

				this.ballsNum--;
				
				return true;

			}
			
		}
		
		return false;
		
	}



    /* whoOwnsBall()
     * 
     * Returns the Player objects of the players who owns a ball
     * null if none owns the ball
     * 
     */
    public Player[] whoOwnsBall() {
        
		Player[] players = new Player[this.ballsNum];

		for ( int i = 0; i < this.ballsNum; ++i ) {
                
			players[i] = balls[i].getGrabber();

        }                    
        
		return players; // List of players who owns a ball
        
    }



	/* whoOwnsBall( Ball ball ) 
     * 
     * Returns the Player object of the player who owns a specific ball
     * null if noone owns the ball
     * 
     */ 
	public Player whoOwnsBall( Ball ball ) {
		
		Player currentPlayer;
		
		for ( int i = 0; i < 2; ++i ) {
			
			for ( int j = 0; j < ( this.playersnum / 2 ); ++j ) {
				
				currentPlayer = teams[i].getPlayer(j);
				
				if ( currentPlayer.getBall() == ball )
					return currentPlayer; // A player owns the ball, return the Player object
				
			}
			
		}
		
		return null; // The ball is free
		
	}
	
	
	
	/* getBalls()
     * 
     * Return an array of the current game balls
     * 
     */ 
	public Ball[] getBalls() {
		
		// TODO only return active balls
		return this.balls;

    }



	/* getBallFromGameObject( GameObject obj )
     * 
     * Returns a Ball Object from a GameObject if it's a Ball
     * 
     */ 
	public Ball getBallFromGameObject( GameObject obj ) {
		
		for ( int i = 0; i < this.ballsNum; ++i ) {

			if ( balls[i].getBallObj() == obj )
				return balls[i];

		}

		return null;
		
	}



	/* setMainPlayer( Player player )
	 * 
	 * player is the main player of the game
	 * 
	 */ 
	public void setMainPlayer( Player player ) {
		
		Player[] players = getPlayers();
		
		for ( int i = 0; i < this.playersnum; ++i ) {

			players[i].setMainPlayer( false );
			
		}

		player.setMainPlayer( true );

		this.mainPlayer = player;
		
	}



	/* getMainPlayer()
	 * 
	 * Returns the main player of the game
	 * 
	 */ 
	public Player getMainPlayer() {
		
		return this.mainPlayer;
		
	}



    /* setPrisonField( int prisonno, bool active )
     * 
     * Activate or desactivate the magnetic field of prison prisonno
     * 
     */ 
    public void setPrisonField( int prisonno, bool active ) {

        if ( prisonno >= 0 && prisonno < 2 )
            this.prisonFieldActive[prisonno] = active;
       
    }



    /* setMagneticField( bool active )
     * 
     * Activate or desactivate the main magnetic field
     * 
     */ 
    public void setMagneticField( bool active ) {
        
        this.magneticFieldActive = active;
        
    }



    /* getPrisonField( int prisonno )
     * 
     * Returns the state of the magnetic field of prison prisonno
     * 
     */
    public bool getPrisonField( int prisonno ) {

        if ( prisonno >= 0 && prisonno < 2 )
            return this.prisonFieldActive[prisonno];

        return false;

    }
    
    

    /* getMagneticField( int prisonno )
     * 
     * Returns the state of the main magnetic field
     * 
     */
    public bool getMagneticField() {
        
        return this.magneticFieldActive;
        
    }



    /* getDifficulty()
     * 
     * set the difficulty setting of the game
     * 
     */
    public int getDifficulty() {

        return this.difficulty;

    }



    /* setDifficulty( short newdifficulty )
     * 
     * Returns the difficulty setting of the game
     * 
     */
    public void setDifficulty( int newdifficulty ) {
        
        this.difficulty = newdifficulty;
        
    }



	/* setMinBallHitSpeed( float val )
     * 
     * Set the minimum speed of the ball to count as a hit
     * 
     */
	public void setMinBallHitSpeed( float val ) {

		this.minBallHitSpeed = val;

	}



	/* getMinBallHitSpeed()
     * 
     * Get the minimum speed of the ball to count as a hit
     * 
     */
	public float getMinBallHitSpeed() {
		
		return this.minBallHitSpeed;

	}



	/* getFieldSize()
     * 
     * Get the size of the game field
     * 
     */
	public Rect3D getFieldSize() {

		return this.fieldSize;

	}



	/* getFieldSize()
     * 
     * Get the size of a prison cell
     * 
     */
	public Rect3D getPrisonSize( int no ) {

		return this.prisonSize[no];
		
	}



	/* getPrisonLimits()
     * 
     * Get the limits of a prison cell
     * 
     */
	public Rect3D getPrisonLimits( int no ) {
		
		return this.prisonLimits[no];
		
	}



	/* setFieldSize( float left, float right, float front, float back, float bottom, float top )
     * 
     * Set the size of the game field
     * 
     */
	public void setFieldSize( float left, float right, float front, float back, float bottom, float top ) {
		
		this.fieldSize.left   = left;
		this.fieldSize.right  = right;
		this.fieldSize.front  = front;
		this.fieldSize.back   = back;
		this.fieldSize.bottom = bottom;
		this.fieldSize.top    = top;

	}



	/* setPrisonSize( int no, float left, float right, float front, float back, float bottom, float top )
     * 
     * Set the size of a prison cell
     * 
     */
	public void setPrisonSize( int no, float left, float right, float front, float back, float bottom, float top ) {

		this.prisonSize[no].left   = left;
		this.prisonSize[no].right  = right;
		this.prisonSize[no].front  = front;
		this.prisonSize[no].back   = back;
		this.prisonSize[no].bottom = bottom;
		this.prisonSize[no].top    = top;
		
	}



	/* setPrisonLimits( int no, float left, float right, float front, float back, float bottom, float top )
     * 
     * Set the limits of the players positions in a prison cell
     * 
     */
	public void setPrisonLimits( int no, float left, float right, float front, float back, float bottom, float top ) {
		
		this.prisonLimits[no].left   = left;
		this.prisonLimits[no].right  = right;
		this.prisonLimits[no].front  = front;
		this.prisonLimits[no].back   = back;
		this.prisonLimits[no].bottom = bottom;
		this.prisonLimits[no].top    = top;
		
	}



	/* bool posInPrison( int no, Vector3 pos )
	 * 
	 * Return true if a position is in prison no
	 * 
     */
	public bool posInPrison( int no, Vector3 pos ) {
		
		Rect3D rect = getPrisonSize( no );

		if ( pos.x >= rect.xMin() && pos.x <= rect.xMax()
		  && pos.y >= rect.yMin() && pos.y <= rect.yMax()
		  && pos.z >= rect.zMin() && pos.z <= rect.zMax() ) {

            return true;

		}

		return false;
		
	}



	/* fitByInPrison( int no, Vector3 pos, float offset )
	 * 
	 * Returns a position in the prison that fits in it by an offset
	 * 
     */
	public Vector3 fitByInPrison( int no, Vector3 pos, float offset ) {
		
		Rect3D  rect   = getPrisonLimits( no );
		Vector4 newpos = new Vector3 ( pos.x, pos.y, pos.z );

		if ( pos.x <= rect.xMin() )
			newpos.x = rect.xMin() + offset;

		if ( pos.x >= rect.xMax() )
			newpos.x = rect.xMax() - offset;

		if ( pos.y <= rect.yMin() )
			newpos.y = rect.yMin() + offset;
		
		if ( pos.y >= rect.yMax() )
			newpos.y = rect.yMax() - offset;

		if ( pos.z <= rect.zMin() )
			newpos.z = rect.zMin() + offset;
		
		if ( pos.z >= rect.zMax() )
			newpos.z = rect.zMax() - offset;

		return newpos;
		
	}



    /* generatePosInTeamSide(int noTeam)
	 * 
	 * Return a random Vector 3 in an area Team
	 * 
     */
    public Vector3 generatePosInTeamSide(int noTeam)
    {
        float x = 0;
        float y = 0;
        float z = 0;

        Rect3D gameArea = this.getFieldSize();

        if (noTeam == 0) {

            do {

                x = Random.Range(gameArea.xMin() + 2.0f, gameArea.xMax() - 2.0f);
                y = gameArea.centerY() + 1.0f;
                z = Random.Range(gameArea.zMin() + 5.0f, gameArea.centerZ() - 10.0f);

            } while(posInPrison(0, new Vector3(x, y, z)));


        } else if(noTeam == 1) {
            
			do {

                x = Random.Range(gameArea.xMin() + 2.0f, gameArea.xMax() - 2.0f);
                y = gameArea.centerY() + 1.0f;
                z = Random.Range(gameArea.centerZ() + 5.0f, gameArea.zMax() - 10.0f);

            } while (posInPrison(1, new Vector3(x, y, z)));

        }

        //Debug.LogError("Position"+ x + ",  " +  y + ", " + z);
        return new Vector3(x, y, z);
    }



	/* getPositionFieldSide( Vector3 pos )
	 * 
	 * Return the number of the team's side a position is at
	 * 
     */
	public int getPositionFieldSide( Vector3 pos ) {

		if ( pos.z > getFieldSize().centerZ() ) {

			return 1;

		}

		return 0;

	}



	/* getDistance( Vector3 pos1, Vector3 pos2 )
	 * 
	 * Returns the distance between 2 positions
	 * 
     */
	public float getDistance( Vector3 pos1, Vector3 pos2 ) {

		return ( pos2 - pos1 ).magnitude;

	}



    /* generateRespawnPos()
     * 
     * Genere des positions de depart pour chaque vaisseau de chaque team 
     */
    public void generateRespawnPos() {

        for ( int i = 0; i < teams.Length; ++i ) {

            teams[i].generateRespawn();

        }

    }



    /* reinitAll()
	 * 
	 * reinit all the game after each round
	 * 
     */
    public void reinitAll() {
		GameObject lpu = GameObject.FindGameObjectsWithTag ("laserPowerUp")[0];
		LaserPowerUp its = lpu.gameObject.GetComponent<LaserPowerUp>();
		its.resetPowerUp ();


        for ( int i = 0; i < teams.Length; ++i ) {

            teams[i].reinitPlayers();

        }

		for ( int i = 0; i < ballsNum; ++i ) {

            if (balls[i] != null)
                balls[i].reinitBall();

		}

    }



	public void setZeroGravityMode( bool state ) {

		if ( zeroGravityMode != state ) {

			GameObject camera = GameObject.FindGameObjectsWithTag("MainCamera")[0];

			if ( state == false && zeroGravityMode ) {

				Physics.gravity = new Vector3( 0.0f, -9.81f, 0.0f );

				// Play the zero gravity sounds
				playSound( AudioController.AUDIO_ZERO_GRAVITY_OFF );
				stopPlaying( AudioController.AUDIO_ZERO_GRAVITY_SOUND );

				if ( camera != null ) {

					camera.GetComponent<Bloom>().enabled = false;
					controller.zeroGravityText.SetActive ( false );

					if ( controller.getBlocs() != null )
						controller.getBlocs().normalGravity();

				}

				// Switch to a harder mood if in random segments mode
				if ( controller.getRandomSegmentsMode() )
					audio().playSegmentsSoundtrack( 3 );

			} else if ( state == true && !zeroGravityMode ) { // Zero gravity mode

				Physics.gravity = new Vector3( 0.0f, 0.0f, 0.0f );

				// Play the zero gravity sounds
				playSound( AudioController.AUDIO_ZERO_GRAVITY_ON );
				playSound( AudioController.AUDIO_ZERO_GRAVITY_SOUND, 1.0f, true );

				if ( camera != null ) {

					camera.GetComponent<Bloom>().enabled = true;

					if ( controller.getBlocs() != null )
						controller.getBlocs().zeroGravity( 2.0f, 8.0f );

					controller.zeroGravityText.SetActive ( true );

				}

				// Switch to a smoother mood if in random segments mode
				if ( controller.getRandomSegmentsMode() )
					audio().playSegmentsSoundtrack( 0 );

			}

			zeroGravityMode = state;

		}

	}



	public bool getZeroGravityMode() {

		return zeroGravityMode;

	}



	public void setFirstPersonView( bool state ) {

		if ( state ) {

			GameObject mire = GameObject.FindWithTag ( "FirstPersonMire" );

			mire.GetComponent<SpriteRenderer>().enabled = true;

		} else {

			GameObject mire = GameObject.FindWithTag ( "FirstPersonMire" );

			mire.GetComponent<SpriteRenderer>().enabled = false;

		}

		firtPersonView = state;
		
	}



	public bool getFirstPersonView() {
		
		return firtPersonView;
		
	}



	public void setBezierPaths( bool val ) {

		this.showBezierPath = val;

		Player[] players = getPlayers();

		foreach ( Player currentPlayer in players ) {

			if ( !currentPlayer.isMainPlayer() )
				currentPlayer.getObject().GetComponent<SmoothRandomMouvement>().showPath = val;
		
		}

	}



	public bool getBezierPaths() {

		return showBezierPath;

	}



	/* playSound( int sound )
	 * playSound( int sound, float level )
	 * playSound( int sound, float level, bool loop )
	 * 
	 * Play a sound
	 * 
	 */
	public void playSound( int sound ) {
		
		audio().playSound( sound, null, 1.0f, false );
		
	}
	
	public void playSound( int sound, float level ) {
		
		audio().playSound( sound, null, level, false );
		
	}
	
	public void playSound( int sound, float level, bool loop ) {
		
		audio().playSound( sound, null, level, loop );
		
	}
	
	
	
	/* isPlaying()
	 * isPlaying( int sound )
	 * 
	 * Is playing a sound?
	 * 
	 */	
	public bool isPlaying() {
		
		return audio().isPlaying( null );
		
	}
	
	public bool isPlaying( int sound ) {
		
		return audio().isPlaying( sound, null );
		
	}
	
	
	
	/* stopPlaying( int sound )
	 * 
	 * Stop a sound
	 * 
	 */	
	public bool stopPlaying( int sound ) {
		
		return audio().stopSound( sound, null );
		
	}
	
	
	
	/* setSoundVolume( int sound, float level )
	 * 
	 * Set the level of a sound
	 * 
	 */	
	public void setSoundVolume( int sound, float level ) {
		
		audio().setVolume( sound, null, level );
		
	}
	
	
	
	/* setSoundPitch( int sound, float pitch ) 
	 * 
	 * Set the pitch of a sound
	 * 
	 */	
	public void setSoundPitch( int sound, float pitch ) {
		
		audio().setPitch( sound, null, pitch );
		
	}

}
