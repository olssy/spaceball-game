﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Configs : MonoBehaviour {

	// Game quality configurations
	public bool         showAnimatedLights  = true;
	public bool         showAnimatedShadows = true;
	public bool         highQualityShadows  = true;
	public bool         trailRenderer       = true;
	public bool         animFrontier        = true;
	public bool         invisibility        = false;
	public QualityLevel qualityLevel        = QualityLevel.Beautiful; // Quality from 0 to 5 (or 1 to 6?)

	[Range(2, 20)]    public int   playersByTeam         = 2;
	[Range(1, 40)]    public int   ballsNum              = 6;
	[Range(1, 5)]     public int   difficulty            = 3;         // Difficulty from 0 to 3

	[Range(0f, 2f)]   public float brightness            = 1;
	[Range(0f, 2f)]   public float contrast              = 1;
    [Range(10f,300f)] public float roundTime             = 180.0f;
	[Range(1,10)]     public int   roundNum              = 5;

	public bool       arrowsMouseMode                    = true;
	[Range(0.1f, 5f)] public float mouseSensibility      = 3.0f;
	[Range(1, 1000)]  public int   bezierCurvesPrecision = 100;

	public bool       showFPS           = false;
	public bool       qualityAdjustFPS  = true;
	public bool       magnetBalls       = true;
	public float      rayCastRadius     = 1.0f;
	public float      magnetBallsRadius = 1.0f;

	private int       currentQuality    = 0;


	void Start () {

		configQualityLevel();
		configAnimatedLights();
		configAnimatedShadows();
		configHighQualityShadows();
		configBrightness();
		configContrast();
		configTrailRenderer();
		configAnimFrontier();

		// Show the FPS counter
		showFPSCounter( this.showFPS );

		if ( this.invisibility ) {

			Physics.IgnoreLayerCollision( 10, 20 );

		}

	}



	public void setMouseSensibility( float sens ) {
		
		this.mouseSensibility = sens;
		
	}



	public void setMouseSensibility( Slider slider ) {
		
		this.mouseSensibility = slider.value;
		
	}



	public float getMouseSensibility() {
		
		return this.mouseSensibility;
		
	}

	

	public void setArrowsMouseMode( bool mode ) {
		
		this.arrowsMouseMode = mode;
		
	}



	public bool getArrowsMouseMode() {
		
		return this.arrowsMouseMode;
		
	}



	public void configQualityLevel() {
		
		QualitySettings.SetQualityLevel( (int) this.qualityLevel );
		
	}



	public void setQualityLevel( int level ) {
		
		this.qualityLevel = (QualityLevel) level;
		
		configQualityLevel();
		
	}



    public void setQualityLevel(Slider slider)
    {

		this.currentQuality = (int) slider.value;

		switch ( (int) slider.value ) {

			case 1  :

				setQualityLevel( 0 );
				showAnimatedLights  = false;
				showAnimatedShadows = false;
				highQualityShadows  = false;
				trailRenderer       = false;
				animFrontier        = false;

			break;

		case 2  :
		
				setQualityLevel( 3 );
				showAnimatedLights  = false;
				showAnimatedShadows = false;
				highQualityShadows  = false;
				trailRenderer       = true;
				animFrontier        = true;
		
			break;

		case 3  :
	
				setQualityLevel( 4 );
				showAnimatedLights  = true;
				showAnimatedShadows = false;
				highQualityShadows  = false;
				trailRenderer       = true;
				animFrontier        = true;
	
			break;

		}

    }



    public int getQualityLevel() {
		
		return (int) this.qualityLevel;
		
	}



	public void setCurrentQualityLevel( int level ) {

		this.currentQuality = level;
		
		switch ( level ) {
			
		case 1  :
			
			setQualityLevel( 0 );
			showAnimatedLights  = false;
			showAnimatedShadows = false;
			highQualityShadows  = false;
			trailRenderer       = false;
			animFrontier        = false;
			
			break;
			
		case 2  :
			
			setQualityLevel( 3 );
			showAnimatedLights  = false;
			showAnimatedShadows = false;
			highQualityShadows  = false;
			trailRenderer       = true;
			animFrontier        = true;
			
			break;
			
		case 3  :
			
			setQualityLevel( 4 );
			showAnimatedLights  = true;
			showAnimatedShadows = false;
			highQualityShadows  = false;
			trailRenderer       = true;
			animFrontier        = true;
			
			break;
			
		}
				
	}



	public int getCurrentQualityLevel() {
		
		return this.currentQuality;
		
	}

	

	public void configBrightness() {

		GameObject camera = GameObject.FindWithTag ("MainCamera");

		if ( camera != null ) {

			BrightnessEffect effect = camera.GetComponent<BrightnessEffect>();

	        if ( effect == null )
				return;

			if ( this.brightness == 1 && this.contrast == 1 ) {

	          	effect.enabled = false;

			} else {

				effect.enabled = true;
				effect._Brightness = this.brightness;

			}

		}
		
	}
	
	
	
	public void setBrightness( float level ) {
		
		this.brightness = level;
		
		configBrightness();
		
	}



    public void setBrightness( Slider slider ) {

        this.brightness = slider.value;

        configBrightness();

    }



    public float getBrightness() {
		
		return this.brightness;
		
	}



	public void configContrast() {
		
		GameObject camera = GameObject.FindWithTag ("MainCamera");
		
		if ( camera != null ) {
			
			BrightnessEffect effect = camera.GetComponent<BrightnessEffect>();
			
			if ( effect == null )
				return;
			
			if ( this.brightness == 1 && this.contrast == 1 ) {
				
				effect.enabled = false;
				
			} else {
				
				effect.enabled   = true;
				effect._Contrast = this.contrast;
				
			}
			
		}
		
	}
	
	
	
	public void setContrast( float level ) {
		
		this.contrast = level;
		
		configContrast();
		
	}



    public void setContrast( Slider slider ) {

        this.contrast = slider.value;

        configContrast();

    }



    public float getContrast() {
		
		return this.contrast;
		
	}



	public void showFPSCounter( bool show ) {

		GameObject     gameControllerObject = GameObject.FindWithTag ("GameController");
		GameController gameController       = null;

		if ( gameControllerObject != null ) {
			
			gameController = gameControllerObject.GetComponent <GameController>();
			
		}
		
		if ( gameController == null ) {
			
			Debug.Log ("Cannot find 'GameController' script");
			
		}

		if ( gameController.FPScounter != null ) {

			gameController.FPScounter.GetComponentsInChildren<UnityStandardAssets.Utility.FPSCounter>()[0].show( show );
			//gameController.FPScounter.SetActive ( show );
			this.showFPS = show;

		}

	}



	public float getCurrentFPS() {
		
		GameObject     gameControllerObject = GameObject.FindWithTag ("GameController");
		GameController gameController       = null;
		
		if ( gameControllerObject != null ) {
			
			gameController = gameControllerObject.GetComponent <GameController>();
			
		}
		
		if ( gameController == null ) {
			
			Debug.Log ("Cannot find 'GameController' script");
			
		}
		
		if ( gameController.FPScounter != null ) {
			
			return gameController.FPScounter.GetComponentsInChildren<UnityStandardAssets.Utility.FPSCounter>()[0].getFPS();
			
		}

		return Mathf.Infinity;

	}

	

	public bool getFPSCounter() {

		return this.showFPS;
		
	}



	public void setQualityAdjustFPS( bool val ) {
		
		this.qualityAdjustFPS = val;
		
	}



	public bool getQualityAdjustFPS() {
		
		return this.qualityAdjustFPS;
		
	}

	
	
    public void configAnimatedLights() {

		if ( this.showAnimatedLights ) {

			// Do something

		} else {

			// Do something
			
		}

	}



	public bool getAnimatedLights() {
		
		return this.showAnimatedLights;
		
	}



	public void setAnimatedLights( bool val ) {
		
		this.showAnimatedLights = val;
		
	}



	public void configAnimatedShadows() {
		
		if ( this.showAnimatedShadows ) {

			// Do something
			
		} else {

			// Do something
			
		}
		
	}



	public bool getAnimatedShadows() {
		
		return this.showAnimatedShadows;
		
	}



	public void setAnimatedShadows( bool val ) {
		
		this.showAnimatedShadows = val;
		
	}



	public void configHighQualityShadows() {
		
		if ( this.highQualityShadows ) {

			setHighQualityShadows( true );
			
		} else {

			setHighQualityShadows( false );
			
		}
		
	}



	public bool getHighQualityShadows() {
		
		return this.highQualityShadows;
		
	}



	public void setHighQualityShadows( bool val ) {
		
		this.highQualityShadows = val;
		
	}



	public void setPlayersByTeam( int val ) {
		
		this.playersByTeam = val;
		
	}



    public void setPlayersByTeam( Slider slider ) {

        this.playersByTeam = (int) slider.value;

    }



    public int getPlayersByTeam() {
		
		return this.playersByTeam;
		
	}



	public void setBallsNum( int val ) {
		
		this.ballsNum = val;
		
	}



    public void setBallsNum( Slider slider ) {

        this.ballsNum = (int) slider.value;

    }



    public int getBallsNum() {
		
		return this.ballsNum;
		
	}



	public void setDifficulty( int val ) {
		
		this.difficulty = val;
		
	}



    public void setDifficulty( Slider slider ) {

        this.difficulty = (int) slider.value;

    }



    public int getDifficulty() {
		
		return this.difficulty;
		
	}
	


	public void configTrailRenderer() {


	}



	public bool getTrailRenderer() {

		return this.trailRenderer;

	}



	public void setTrailRenderer( bool val ) {
		
		this.trailRenderer = val;
		
	}



	public void configAnimFrontier() {
		
		
	}


	
	public bool getAnimFrontier() {
		
		return this.animFrontier;
		
	}
	


	public void setAnimFrontier( bool val ) {
		
		this.animFrontier = val;
		
	}



	public bool getInvinsibility() {
		
		return this.invisibility;
		
	}
	
	
	public void setInvinsibility( bool val ) {
		
		this.invisibility = val;
		
	}



    public float getRoundTime() {

        return this.roundTime;

    }



    public void setRoundTime( float time ) {

        this.roundTime = time;

    }



	public void setRoundTime( Slider slider ) {
		
		this.roundNum = (int) slider.value;
		
	}



	public int getRoundNum() {
		
		return this.roundNum;
		
	}

	
	
	public void setRoundNum( int num ) {
		
		this.roundNum = num;
		
	}



	public void setRoundNum( Slider slider ) {
		
		this.roundNum = (int) slider.value;
		
	}



	public float getRayCastRadius() {
		
		return this.rayCastRadius;
		
	}


	
	public void getRayCastRadius( float radius ) {
		
		this.rayCastRadius = radius;
		
	}



	public bool getMagnetBalls() {
		
		return this.magnetBalls;
		
	}


		
	public void setMagnetBalls( bool val ) {
		
		this.magnetBalls = val;
		
	}



	public float getMagnetBallsRadius() {
		
		return this.magnetBallsRadius;
		
	}
	
	
	
	public void setMagnetBallsRadius( float val ) {
		
		this.magnetBallsRadius = val;
		
	}

}
